package com.infinityapp.android.factory.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.infinityapp.infinity.MainApplication
import com.infinityapp.infinity.R
import com.infinityapp.infinity.activity.IntroActivity
import com.infinityapp.infinity.activity.SignInOrUpActivity
import com.twitter.sdk.android.core.Twitter


abstract class InfinityLaunchActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null
    private var openMLA = false
    private var isRelationship = false
    private var id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
    }

    override fun onResume() {
        super.onResume()
        if (!isGooglePlayServicesAvailable(this)) GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)
        setupFirebase()
        checkIsMLA()
        startApplication()
    }


    override fun onBackPressed() {}

    private fun isGooglePlayServicesAvailable(activity: Activity): Boolean {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val status = googleApiAvailability.isGooglePlayServicesAvailable(activity)
        if (status != ConnectionResult.SUCCESS) {
            val dialog = googleApiAvailability.getErrorDialog(activity, status, 2404)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setOnCancelListener { System.exit(1) }
            dialog.setOnDismissListener { System.exit(1) }
            dialog.show()
            return if (googleApiAvailability.isUserResolvableError(status)) {
                false
            } else {
                false
            }

        }
        return true
    }

    private fun setupFirebase() {
        FirebaseFirestore.getInstance().firestoreSettings = FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .setSslEnabled(true)
                .build()
        mAuth = FirebaseAuth.getInstance()
    }

    private fun checkIsMLA() {
        if (intent.action == "openMLA") {
            if (intent.getStringExtra("openMLA") != "" || intent.getStringExtra("openMLA") != null) {
                openMLA = true
                val isRelationship = intent.getStringExtra("openMLA").split("/")[0] == "relationships"
                val relOrGroupId = intent.getStringExtra("openMLA").split("/")[1]
                this.isRelationship = isRelationship
                this.id = relOrGroupId
            }

        }
    }

    private fun requestAndLaunch() {
        if (!isGooglePlayServicesAvailable(this)) {
            GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this).addOnCompleteListener { result ->
                if (result.isSuccessful) {
                    requestAndLaunch()
                } else {
                    finish()
                }
            }
            return
        }
        FirebaseRemoteConfig.getInstance().fetch(0).addOnCompleteListener {
            Twitter.initialize(this)
            this.launchMainActivity()
        }
    }

    private fun startApplication() {
        Thread(Runnable { if (!MainApplication.instance.getInitializationFunction().invoke()) System.exit(1) else this.runOnUiThread({ requestAndLaunch() }) }).start()
    }

    private fun launchMainActivity() {
        val intent = Intent(this, IntroActivity::class.java)
        val intent2 = Intent(this, SignInOrUpActivity::class.java)
        if (mAuth!!.currentUser != null) {
            if (openMLA) intent2.action = "openMLA"
            if (openMLA && isRelationship) {
                intent2.putExtra("openMLAR", id)
                startActivity(intent2)
            } else if (openMLA && !isRelationship) {
                intent2.putExtra("openMLAG", id)
                startActivity(intent2)
            }
            //Signed In
            startActivity(intent2)
        } else {// Sign Up
            startActivity(intent)
        }
    }

}
