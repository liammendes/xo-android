package com.infinityapp.infinity

import android.view.View

/**
 * Created by liammendes on 03/02/2018.
 */

interface FSAAInterface {
    fun active(): ArrayList<View.OnClickListener>
}
