package com.infinityapp.infinity.activity

import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.github.javiersantos.bottomdialogs.BottomDialog
import com.google.android.gms.ads.MobileAds
import com.google.firebase.auth.FirebaseAuth
import com.infinityapp.infinity.R
import com.infinityapp.infinity.fragment.MainFragment
import com.infinityapp.infinity.fragment.VerificationFragment
import com.infinityapp.infinity.views.CurrentProfileLoadingView
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by liammendes on 11/02/2018.
 */

class MainActivity :
/**SparklesCaptureInterface()**/
        AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MobileAds.initialize(this, "ca-app-pub-6435691243087915~8783663494")
        setLightStatusBar(window.decorView, this)
    }

    override fun onBackPressed() = finish()

    @TargetApi(Build.VERSION_CODES.M)
    fun setLightStatusBar(view: View, activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            var flags = view.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            view.systemUiVisibility = flags
            activity.window.statusBarColor = Color.WHITE
        }
    }

    fun signOut() {
        var bd = BottomDialog.Builder(this)
                .setTitle("Signing Out")
                .setContent("Hold on Tight, This is going to be quite a ride")
                .setCustomView(CurrentProfileLoadingView(this))
                .autoDismiss(true)
                .setCancelable(false)
                .show()
        super.finish()
        bd.dismiss()
        FirebaseAuth.getInstance().signOut()
    }

    override fun onStart() {
        super.onStart()
        main_content.adapter = MainPager(supportFragmentManager)
        main_tabs.menu.clear()
        if (!FirebaseAuth.getInstance().currentUser!!.isEmailVerified) {
            main_tabs.inflateMenu(R.menu.setup_menu)
        } else main_tabs.inflateMenu(R.menu.home_menu)
        main_tabs.setOnNavigationItemSelectedListener { item: MenuItem ->
            when (item.itemId) {
                R.id.navigation_home -> main_content.currentItem = 0
                R.id.navigation_verification -> main_content.currentItem = 0
            //wR.id.navigation_music -> main_content.currentItem = 1
            }
            true
        }
        main_content.currentItem = 0
    }

    override fun finish() {
        super.finish()
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(startMain)
    }

    class MainPager(fm: FragmentManager): FragmentPagerAdapter(fm) {

        val NUM_ITEMS = 1

        override fun getItem(position: Int): Fragment {
            if (!FirebaseAuth.getInstance().currentUser!!.isEmailVerified)
                when (position) {
                //0 -> return InitialSetupFragment()
                    0 -> return VerificationFragment()
                }
            else
                when (position) {
                    0 -> return MainFragment()
                //1 -> return MusicFragment()
                }
            return Fragment()
        }

        override fun getCount(): Int {
            if (FirebaseAuth.getInstance().currentUser == null) {
                /**signOut();**/
                return 1
            }
            if (!FirebaseAuth.getInstance().currentUser!!.isEmailVerified) return 1
            return NUM_ITEMS
        }

        override fun getPageTitle(position: Int): CharSequence? {
            if (!FirebaseAuth.getInstance().currentUser!!.isEmailVerified) {
                when (position) {
                    0 -> return "Verification"
                }
            } else
                when (position) {
                    0 -> return "Home"
                //1 -> return "Music"
                }
            return super.getPageTitle(position)
        }

    }

}