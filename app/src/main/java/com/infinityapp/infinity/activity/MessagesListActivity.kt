package com.infinityapp.infinity.activity

import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.transition.AutoTransition
import android.transition.TransitionManager
import android.util.AttributeSet
import android.util.Log
import android.util.Log.e
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.davidecirillo.multichoicerecyclerview.MultiChoiceAdapter
import com.davidecirillo.multichoicerecyclerview.MultiChoiceToolbar
import com.firebase.ui.common.ChangeEventType
import com.firebase.ui.firestore.ChangeEventListener
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.storage.FirebaseStorage
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.MainApplication
import com.infinityapp.infinity.R
import com.infinityapp.infinity.adapter.ChatRecyclerAdapter
import com.infinityapp.infinity.adapter.ChatRecyclerAdapterImpl
import com.infinityapp.infinity.adapter.RelationshipChatRecyclerAdapter
import com.infinityapp.infinity.dataclasses.Chat
import com.infinityapp.infinity.dataclasses.RelationshipChat
import com.infinityapp.infinity.fragment.TYPE
import com.infinityapp.infinity.utils.DisplayUtils
import com.infinityapp.infinity.virtualapi.VirtualAPI
import kotlinx.android.synthetic.main.activity_messages_list.*
import kotlinx.android.synthetic.main.activity_messages_list.view.*
import kotlinx.android.synthetic.main.more_common_layout.*
import kotlinx.android.synthetic.main.predictions_bar_layout.*
import java.util.*

class MessageListActivity : AppCompatActivity() {

    private var cra: Any? = null
    private var t: TYPE? = null
    object MLA {
        init {
        }
        var mlaInstance: MessageListActivity? = null
    }
    private var chosen: Boolean = false
    private var selectedImage: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (allChecksPassed(requestCode, resultCode, data))
            if (t == TYPE.GROUP) {
                chosen = true
                selectedImage = data!!.data
                FirebaseStorage.getInstance().reference.child("images/${UUID.randomUUID()}.jpg").putFile(selectedImage!!).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        VirtualAPI.sendMessageUID(InfinityAPI.Group.retrieveContext()!!.groupId, "", InfinityAPI.signedInUser!!.UserID, "P", task.result.downloadUrl.toString(), data.data.toString())
                    } else {
                        Toast.makeText(this@MessageListActivity, "Failed to send photo. Please try again!", Toast.LENGTH_LONG).show()
                    }
                }
            } else if (t == TYPE.FRIENDS) {
                chosen = true
                selectedImage = data!!.data
                FirebaseStorage.getInstance().reference.child("images/${UUID.randomUUID()}.jpg").putFile(selectedImage!!).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        VirtualAPI.RsendMessageUID(InfinityAPI.Relationship.retrieveContext()!!.relID, "", InfinityAPI.signedInUser!!.UserID, "P", task.result.downloadUrl.toString(), data.data.toString())
                    } else {
                        Toast.makeText(this@MessageListActivity, "Failed to send photo. Please try again!", Toast.LENGTH_LONG).show()
                    }
                }
            }
    }

    fun allChecksPassed(requestCode: Int, resultCode: Int, data: Intent?): Boolean = if (requestCode == 42) {
        if (resultCode == Activity.RESULT_OK) {
            data != null
        } else false
    } else false

    @TargetApi(Build.VERSION_CODES.M)
    fun setLightStatusBar(view: View, activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            var flags = view.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            view.systemUiVisibility = flags
            activity.window.statusBarColor = Color.WHITE

        }
    }

    fun openCameraDialog(view: View) {
        var READ_REQUEST_CODE = 42
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        var int = Intent(Intent.ACTION_OPEN_DOCUMENT)
        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        int.addCategory(Intent.CATEGORY_OPENABLE)
        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        int.type = "image/*"
        startActivityForResult(int, READ_REQUEST_CODE)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_messages_list)
        setLightStatusBar(window.decorView, this)

        setupButtons()

        //setupPredictions()

        val constraintSet1 = ConstraintSet()
        constraintSet1.clone(mlcl)
        val constraintSet2 = ConstraintSet()
        constraintSet2.clone(this, R.layout.activity_messages_list_more_expanded)

        MLA.mlaInstance = this@MessageListActivity

        cib.setOnCheckedChangeListener { button, isChecked ->
            if (isChecked) {
                val transition = AutoTransition()
                transition.duration = 350
                e(MainApplication.instance.createGlobalTag("MessagesListActivity"), "isChecked")
                TransitionManager.beginDelayedTransition(mlcl, transition)
                //recyclerview_message_list.smoothScrollToPosition(recyclerview_message_list.adapter.itemCount)

                var animator = ValueAnimator.ofInt(recyclerview_message_list.paddingBottom, DisplayUtils.convertDpToPixel(64))
                animator.addUpdateListener { va ->
                    recyclerview_message_list.setPadding(0, 0, 0, ((va.animatedValue as Int) * 2) + 32)
                    /**recyclerview_message_list.smoothScrollToPosition(recyclerview_message_list.adapter.itemCount)**/
                }
                animator.duration = transition.duration
                animator.addListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(p0: Animator?) {
                        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onAnimationCancel(p0: Animator?) {
                        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onAnimationStart(p0: Animator?) {
                        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onAnimationEnd(p0: Animator?) {
                        recyclerview_message_list.smoothScrollToPosition(recyclerview_message_list.adapter!!.itemCount - 1)
                    }

                })
                animator.start()
                constraintSet2.applyTo(mlcl)
            } else {
                val transition = AutoTransition()
                transition.duration = 250
                e(MainApplication.instance.createGlobalTag("MessagesListActivity"), "!isChecked")
                TransitionManager.beginDelayedTransition(mlcl, transition)
                //recyclerview_message_list.smoothScrollToPosition(recyclerview_message_list.adapter.itemCount)

                var animator = ValueAnimator.ofInt(recyclerview_message_list.paddingBottom, DisplayUtils.convertDpToPixel(0))
                animator.addUpdateListener { va ->
                    recyclerview_message_list.setPadding(0, 0, 0, 0)
                    recyclerview_message_list.smoothScrollBy(0, -(va.animatedValue as Int))
                    /**recyclerview_message_list.smoothScrollToPosition(recyclerview_message_list.adapter.itemCount)**/
                }
                animator.duration = transition.duration
                animator.start()
                constraintSet1.applyTo(mlcl)
            }
        }

        var type: TYPE
        if (intent.extras != null) {
            type = intent.extras.get("TYPE") as TYPE
            InfinityAPI.Messaging.initializeContext(type)
        } else {
            type = InfinityAPI.Messaging.retrieveContext()
        }
        if (type == TYPE.GROUP) {
            if (InfinityAPI.Group.retrieveContext()!!.getGroupObject().groupOfficial) official_group_aml.visibility = View.VISIBLE
            t = TYPE.GROUP
            setSupportActionBar(findViewById(R.id.tbml))
            tbml.setNavigationOnClickListener { onBackPressed() }
            Glide.with(this).load(InfinityAPI.Group.retrieveContext()!!.getGroupObject().groupProfilePicture).into(ml_group_pp)
            supportActionBar!!.title = InfinityAPI.Group.retrieveContext()!!.getGroupObject().groupName

            var multiChoiceToolbar =
                    MultiChoiceToolbar.Builder(this@MessageListActivity, tbml)
                            .setTitles(InfinityAPI.Group.retrieveContext()!!.getGroupObject().groupName, "items selected")
                            .setMultiChoiceColours(R.color.md_green_500, R.color.md_green_700)
                            .setDefaultIcon(R.drawable.ic_arrow_back_green_24dp, { view ->
                                onBackPressed()
                            })
                            .setDefaultColours(R.color.md_white_1000, R.color.md_white_1000)
                            .build()

            var chats = InfinityAPI.Group.retrieveContext()!!.groupReference.collection("chats").orderBy("timestamp").orderBy("messageNumber")
            val options = FirestoreRecyclerOptions.Builder<Chat>()
                    .setQuery(chats, Chat::class.java)
                    .build()
            cra = ChatRecyclerAdapterImpl(options)
            recyclerview_message_list.adapter = cra as ChatRecyclerAdapterImpl

            (cra as ChatRecyclerAdapterImpl).setMultiChoiceToolbar(multiChoiceToolbar)
            (cra as ChatRecyclerAdapterImpl).setMultiChoiceSelectionListener(object : MultiChoiceAdapter.Listener {
                override fun OnDeselectAll(itemSelectedCount: Int, allItemCount: Int) {
                    setLightStatusBar(window.decorView, this@MessageListActivity)
                    tbml.setBackgroundColor(resources.getColor(R.color.md_white_1000))
                    if(InfinityAPI.Group.retrieveContext()!!.getGroupObject().groupOfficial)
                        official_group_aml.visibility = View.VISIBLE
                    ml_group_pp.visibility = View.VISIBLE
                    tbml.setTitleTextColor(resources.getColor(R.color.shamrock_green))
                }

                override fun OnSelectAll(itemSelectedCount: Int, allItemCount: Int) {

                }

                override fun OnItemSelected(selectedPosition: Int, itemSelectedCount: Int, allItemCount: Int) {
                    official_group_aml.visibility = View.GONE
                    ml_group_pp.visibility = View.GONE
                    tbml.setTitleTextColor(resources.getColor(R.color.md_white_1000))
                }

                override fun OnItemDeselected(deselectedPosition: Int, itemSelectedCount: Int, allItemCount: Int) {
                    if(itemSelectedCount == 0){
                        setLightStatusBar(window.decorView, this@MessageListActivity)
                        tbml.setBackgroundColor(resources.getColor(R.color.md_white_1000))
                        if(InfinityAPI.Group.retrieveContext()!!.getGroupObject().groupOfficial)
                            official_group_aml.visibility = View.VISIBLE
                        ml_group_pp.visibility = View.VISIBLE
                        tbml.setTitleTextColor(resources.getColor(R.color.shamrock_green))
                    }
                }

            })

            (recyclerview_message_list.adapter as ChatRecyclerAdapterImpl).registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    recyclerview_message_list.layoutManager!!.smoothScrollToPosition(recyclerview_message_list, null, (cra as ChatRecyclerAdapterImpl).itemCount)
                }
            })
            recyclerview_message_list.layoutManager = LinearLayoutManager(this@MessageListActivity)
            (recyclerview_message_list.layoutManager as LinearLayoutManager).reverseLayout = false
            (cra as ChatRecyclerAdapterImpl).snapshots.addChangeEventListener(object : ChangeEventListener {

                override fun onDataChanged() {
                    //recyclerview_message_list.smoothScrollToPosition(recyclerview_message_list.adapter.itemCount-1)
                }

                override fun onChildChanged(type: ChangeEventType, snapshot: DocumentSnapshot, newIndex: Int, oldIndex: Int) {
                    when (type) {
                        ChangeEventType.ADDED -> {
                            recyclerview_message_list.adapter!!.notifyItemInserted(newIndex); stol(); stal()
                        }
                        ChangeEventType.CHANGED -> {
                            recyclerview_message_list.adapter!!.notifyItemChanged(newIndex); stol(); stal()
                        }
                        ChangeEventType.MOVED -> {
                            recyclerview_message_list.adapter!!.notifyItemMoved(oldIndex, newIndex); stol(); stal()
                        }
                        ChangeEventType.REMOVED -> {
                            recyclerview_message_list.adapter!!.notifyItemRemoved(newIndex); stol(); stal()
                        }
                    }
                }

                override fun onError(e: FirebaseFirestoreException) {
                    //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

            })

            send_iv.setOnClickListener {
                Thread({
                    if (edittext_chatbox.text!!.isEmpty()) return@Thread
                    VirtualAPI.sendMessageUID(InfinityAPI.Group.retrieveContext()!!.groupId, edittext_chatbox.text.toString(), InfinityAPI.signedInUser!!.UserID, "M", "", "")
                    runOnUiThread { edittext_chatbox.setText("") }
                    onMessageSent()
                }).start()
            }
            send.setOnClickListener {
                Thread({
                    if (edittext_chatbox.text!!.isEmpty()) return@Thread
                    VirtualAPI.sendMessageUID(InfinityAPI.Group.retrieveContext()!!.groupId, edittext_chatbox.text.toString(), InfinityAPI.signedInUser!!.UserID, "M", "", "")
                    runOnUiThread { edittext_chatbox.setText("") }
                    onMessageSent()
                }).start()
            }
        }
        if (type == TYPE.FRIENDS) {
            t = TYPE.FRIENDS
            var isHost = InfinityAPI.signedInUser!!.UserID == InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().host
            var oppositeUID = if (isHost) InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().guest else InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().host
            setSupportActionBar(findViewById<androidx.appcompat.widget.Toolbar>(R.id.tbml))
            tbml.setNavigationOnClickListener { onBackPressed() }
            Glide.with(this@MessageListActivity).load(if (isHost) InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().guestpp else InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().hostpp).into(ml_group_pp)
            supportActionBar!!.title = if (isHost) InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().guestname else InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().hostname
            var chats = InfinityAPI.Relationship.retrieveContext()!!.relReference.collection("chats").orderBy("timestamp").orderBy("messageNumber")
            val options = FirestoreRecyclerOptions.Builder<RelationshipChat>()
                    .setQuery(chats, RelationshipChat::class.java)
                    .build()
            cra = RelationshipChatRecyclerAdapter(options)
            var multiChoiceToolbar =
                    MultiChoiceToolbar.Builder(this@MessageListActivity, tbml)
                            .setTitles(InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().getOtherName(InfinityAPI.signedInUser!!.Name), "items selected")
                            .setMultiChoiceColours(R.color.md_green_500, R.color.md_green_700)
                            .setDefaultIcon(R.drawable.ic_arrow_back_green_24dp, { view ->
                                onBackPressed()
                            })
                            .setDefaultColours(R.color.md_white_1000, R.color.md_white_1000)
                            .build()

            recyclerview_message_list.adapter = cra as RelationshipChatRecyclerAdapter

            (cra as RelationshipChatRecyclerAdapter).setMultiChoiceToolbar(multiChoiceToolbar)
            (cra as RelationshipChatRecyclerAdapter).setMultiChoiceSelectionListener(object : MultiChoiceAdapter.Listener {
                override fun OnDeselectAll(itemSelectedCount: Int, allItemCount: Int) {
                    setLightStatusBar(window.decorView, this@MessageListActivity)
                    tbml.setBackgroundColor(resources.getColor(R.color.md_white_1000))
                    ml_group_pp.visibility = View.VISIBLE
                    tbml.setTitleTextColor(resources.getColor(R.color.shamrock_green))
                }

                override fun OnSelectAll(itemSelectedCount: Int, allItemCount: Int) {

                }

                override fun OnItemSelected(selectedPosition: Int, itemSelectedCount: Int, allItemCount: Int) {
                    official_group_aml.visibility = View.GONE
                    ml_group_pp.visibility = View.GONE
                    tbml.setTitleTextColor(resources.getColor(R.color.md_white_1000))
                }

                override fun OnItemDeselected(deselectedPosition: Int, itemSelectedCount: Int, allItemCount: Int) {
                    if(itemSelectedCount == 0){
                        setLightStatusBar(window.decorView, this@MessageListActivity)
                        tbml.setBackgroundColor(resources.getColor(R.color.md_white_1000))
                        ml_group_pp.visibility = View.VISIBLE
                        tbml.setTitleTextColor(resources.getColor(R.color.shamrock_green))
                    }
                }

            })

            (recyclerview_message_list.adapter as RelationshipChatRecyclerAdapter).registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    recyclerview_message_list.layoutManager!!.smoothScrollToPosition(recyclerview_message_list, null, (cra as RelationshipChatRecyclerAdapter).itemCount)
                }
            })
            recyclerview_message_list.layoutManager = LinearLayoutManager(this@MessageListActivity)
            (recyclerview_message_list.layoutManager as LinearLayoutManager).reverseLayout = false
            (cra as RelationshipChatRecyclerAdapter).snapshots.addChangeEventListener(object : ChangeEventListener {
                override fun onDataChanged() {
                    //recyclerview_message_list.smoothScrollToPosition(recyclerview_message_list.adapter.itemCount-1)
                }

                override fun onChildChanged(type: ChangeEventType, snapshot: DocumentSnapshot, newIndex: Int, oldIndex: Int) {
                    when (type) {
                        ChangeEventType.ADDED -> {
                            recyclerview_message_list.adapter!!.notifyItemInserted(newIndex); stol(); stal()
                        }
                        ChangeEventType.CHANGED -> {
                            recyclerview_message_list.adapter!!.notifyItemChanged(newIndex); stol(); stal()
                        }
                        ChangeEventType.MOVED -> {
                            recyclerview_message_list.adapter!!.notifyItemMoved(oldIndex, newIndex); stol(); stal()
                        }
                        ChangeEventType.REMOVED -> {
                            recyclerview_message_list.adapter!!.notifyItemRemoved(newIndex); stol(); stal()
                        }
                    }
                }

                override fun onError(e: FirebaseFirestoreException) {
                    //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

            })

            send_iv.setOnClickListener {
                Thread({
                    if (edittext_chatbox.text!!.isEmpty()) return@Thread
                    VirtualAPI.RsendMessageUID(InfinityAPI.Relationship.retrieveContext()!!.relID, edittext_chatbox.text.toString(), InfinityAPI.signedInUser!!.UserID, "M", "", "")
                    runOnUiThread { edittext_chatbox.setText("") }
                    onMessageSent()
                }).start()
            }
            send.setOnClickListener {
                Thread({
                    if (edittext_chatbox.text!!.isEmpty()) return@Thread
                    VirtualAPI.RsendMessageUID(InfinityAPI.Relationship.retrieveContext()!!.relID, edittext_chatbox.text.toString(), InfinityAPI.signedInUser!!.UserID, "M", "", "")
                    runOnUiThread { edittext_chatbox.setText("") }
                    onMessageSent()
                }).start()
            }
        }
        //registerForContextMenu(recyclerview_message_list)
    }

    private fun onMessageSent() {
        clearPredictions()
    }

    private fun clearPredictions() {
        try {
            predictions_recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            var pa = PredictionsAdapter(this)
            predictions_recyclerView.adapter = pa
        } catch (e: Exception) {
            Log.e(MainApplication.instance.createGlobalTag("MLEngine"), "Failed to initialize and inflate predictions!")
            e.printStackTrace()
        }
    }

    private fun setupPredictions() {
        try {
            var rm = "Hi"
            predictions_recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            var pa = PredictionsAdapter(this)
            //pa.predictions.addAll(NativeManager.getPredictions(rm))
            predictions_recyclerView.adapter = pa
        } catch (e: Exception) {
            Log.e(MainApplication.instance.createGlobalTag("MLEngine"), "Failed to initialize and inflate predictions!")
            e.printStackTrace()
        }
    }

    private fun setupButtons() {
        //openCameraDialog.setOnClickListener { openCameraDialog(openCameraDialog) }
        add_ons.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        var aaoa = AddOnAdapter(this)
        aaoa.addons.add(LocalAddon("Image", R.drawable.ic_camera_black_24dp, View.OnClickListener { view -> openCameraDialog(view) }))
        add_ons.adapter = aaoa
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        //var info = item.menuInfo
        e(MainApplication.instance.createGlobalTag("MessageListActivity"), "Debug Info - itemId: " + item.itemId)
        when (item.itemId) {
        /**R.id.delete_chat_item -> {
                if (cra is ChatRecyclerAdapterImpl) {
                    // is group

                    deletelongClickedChat = true

                    if (deletelongClickedChat) {
                        deletelongClickedChat = false
                        deleteGroupChat((longClickedChat!! as Chat).getmGroupId() + "/" + (longClickedChat!! as Chat).getmessageNumber())
                    } else if (cra is RelationshipChatRecyclerAdapter) {
                        // is relationship

                        deletelongClickedChat = true

                        if (deletelongClickedChat) {
                            deletelongClickedChat = false
                            deleteRelationshipChat((longClickedChat!! as RelationshipChat).getmRelId() + "/" + (longClickedChat!! as RelationshipChat).getmessageNumber())
                        }
                    }

                }
        }**/

            else -> {
                return super.onContextItemSelected(item)
            }

        }
        return super.onContextItemSelected(item)
    }
    fun openProfileSettings(view: View) {
        if (t == TYPE.GROUP) {
            InfinityAPI.Group.initializeContext(InfinityAPI.Group.retrieveContext()!!)
            startActivity(Intent(this@MessageListActivity, EditGroupActivity::class.java))
        } else if (t == TYPE.FRIENDS) {
            InfinityAPI.Relationship.initializeContext(InfinityAPI.Relationship.retrieveContext()!!)
            startActivity(Intent(this@MessageListActivity, EditRelationshipActivity::class.java))
        }
    }
    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
    private fun deleteRelationshipChat(s: String) {
        val relId = s.split('/')[0]
        val messageNumber = s.split('/')[1]
        InfinityAPI.makeCustomCall("deleteRelationshipChat", Pair("relId", relId), Pair("messageNumber", messageNumber))
    }

    private fun deleteGroupChat(s: String) {
        val groupId = s.split('/')[0]
        val messageNumber = s.split('/')[1]
        InfinityAPI.makeCustomCall("deleteGroupChat", Pair("groupId", groupId), Pair("messageNumber", messageNumber))
    }

    fun stol() = (cra!! as ChatRecyclerAdapter<*, *>).stopListening()
    fun stal() = (cra!! as ChatRecyclerAdapter<*, *>).startListening()
    override fun onStart() {
        super.onStart()
        stal()
    }
    override fun onStop() {
        super.onStop()
        stol()
    }
}


class PPLFailedException(message: String) : Exception(message)
class RelativeChatboxLayout : RelativeLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (MessageListActivity.MLA.mlaInstance == null) return
        if (recyclerview_message_list == null) return
        //recyclerview_message_list.setPadding(0,0,0, h)
    }

}

class AddOnAdapter(context: Context) : RecyclerView.Adapter<AddOnAdapter.ViewHolder>() {

    var addons: ArrayList<Any> = arrayListOf()
    var mInflater: LayoutInflater? = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater!!.inflate(R.layout.add_on, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = addons.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (addons[position] is LocalAddon) {
            holder.bindLA(addons[position])
        } else holder.bind(addons[position])
    }

    class ViewHolder(iv: View) : RecyclerView.ViewHolder(iv) {

        fun bind(addon: Any) {
            itemView.findViewById<TextView>(R.id.add_on_text).text = (addon as Addon).name
            Glide.with(itemView).load(addon.image).into(itemView.findViewById(R.id.add_on_image))
            itemView.findViewById<ConstraintLayout>(R.id.add_on_root).setOnClickListener(addon.onClickListener)
        }

        fun bindLA(addon: Any) {
            itemView.findViewById<TextView>(R.id.add_on_text).text = (addon as LocalAddon).name
            Glide.with(itemView).load(addon.image).into(itemView.findViewById(R.id.add_on_image))
            var mdblue400: Int = 0xFF2979FF.toInt()
            (itemView.findViewById<ImageView>(R.id.add_on_image)).setColorFilter(mdblue400)
            itemView.findViewById<ConstraintLayout>(R.id.add_on_root).setOnClickListener(addon.onClickListener)
        }

    }

    init {
        this.mInflater = LayoutInflater.from(context)
    }

}

class PredictionsAdapter(context: Context) : RecyclerView.Adapter<PredictionsAdapter.ViewHolder>() {
    var predictions: ArrayList<String> = arrayListOf()
    var mInflater: LayoutInflater? = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater!!.inflate(R.layout.prediction, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = predictions.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(predictions[position])
    }

    class ViewHolder(iv: View) : RecyclerView.ViewHolder(iv) {
        fun bind(prediction: String) {
            itemView.findViewById<TextView>(R.id.predictions_textView).text = prediction
            itemView.setOnClickListener {
                if (InfinityAPI.Group.retrieveContext() != null) Thread({
                    VirtualAPI.sendMessageUID(InfinityAPI.Group.retrieveContext()!!.groupId, prediction, InfinityAPI.signedInUser!!.UserID, "M", "", "")
                }).start()
                else
                    if (InfinityAPI.Relationship.retrieveContext() != null) Thread({
                        VirtualAPI.RsendMessageUID(InfinityAPI.Relationship.retrieveContext()!!.relID, prediction, InfinityAPI.signedInUser!!.UserID, "M", "", "")
                    }).start()
            }
        }
    }

    init {
        this.mInflater = LayoutInflater.from(context)
    }
}

data class Addon(var name: String, var image: String, var onClickListener: View.OnClickListener)

data class LocalAddon(var name: String, var image: Int, var onClickListener: View.OnClickListener)