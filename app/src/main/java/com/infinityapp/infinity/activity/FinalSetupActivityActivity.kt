package com.infinityapp.infinity.activity

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.github.javiersantos.bottomdialogs.BottomDialog
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.storage.FirebaseStorage
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.FSAAInterface
import com.infinityapp.infinity.R
import com.infinityapp.infinity.callback.SetupCallback
import com.infinityapp.infinity.dataclasses.SignUpDataGoogle
import com.infinityapp.infinity.fragment.PrivacyPolicyFragment
import com.infinityapp.infinity.fragment.SetupInfoFragment
import com.infinityapp.infinity.views.CurrentProfileLoadingView
import kotlinx.android.synthetic.main.activity_final_setup_activity.*
import java.io.File


class FinalSetupActivityActivity : AppCompatActivity(), SetupInfoFragment.OnSignUpListener, PrivacyPolicyFragment.OnDecisionMadeListener, IFSAAInterface {

    override fun setBOCL(next: View.OnClickListener, cancel: View.OnClickListener) {
        fsaa_next.setOnClickListener(next);fsaa_cancel.setOnClickListener(cancel)
    }

    private var sid: SignUpDataGoogle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_final_setup_activity)
        InfinityAPI.deinitializeCurrentContexts()
        fragmentsview.adapter = RegisterPagerAdapter(supportFragmentManager)
        fragmentsview.currentItem = 0
    }

    override fun onStart() {
        super.onStart()
        val page = fragmentManager.findFragmentByTag("android:switcher:" + R.id.fragmentsview + ":" + fragmentsview.currentItem)
        if(page != null){
            fsaa_next.setOnClickListener(((page as Fragment) as FSAAInterface).active()[0])
            fsaa_cancel.setOnClickListener(((page as Fragment) as FSAAInterface).active()[1])
        }
    }

    private fun getMimeType(context: Context, uri: Uri): String {
        val extension: String

        //Check uri format to avoid null
        if (uri.scheme == ContentResolver.SCHEME_CONTENT) {
            //If scheme is a content
            val mime = MimeTypeMap.getSingleton()
            extension = mime.getExtensionFromMimeType(context.contentResolver.getType(uri))
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(File(uri.path)).toString())

        }

        return extension
    }

    private fun setup(sid: SignUpDataGoogle, callback: SetupCallback) {
        //New Setup Methodology
        /**run {
            var cui = CUInformation(sign_up_data = sid, status = Status.USER, profile_picture = "")
            if (!sid.gpp) {
        val storage = FirebaseRemoteConfig.getInstance()
                val storageRef = storage.reference
                val imagesRef = storageRef.child("profile_pictures").child(sid.userID!!).child("profilepicture." + getMimeType(this@FinalSetupActivityActivity, sid.profilePicture))
                imagesRef.putFile(sid.profilePicture!!).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        cui.setProfilePicture(task.result.downloadUrl?.toString()!!)
                    } else {
                        cui.setProfilePicture(FirebaseAuth.getInstance().currentUser!!.photoUrl!!.toString())
                    }
                }
            }
            APIInterface(this@FinalSetupActivityActivity).createUser(cui)
        }**/

        if (sid.useFirebaseAuthPP) {
            InfinityAPI.makeCustomCall("createUserUID", Pair("dob", (sid.dob.time.toString())), Pair("userId", FirebaseAuth.getInstance().currentUser!!.uid), Pair("email", sid.email), Pair("displayName", sid.displayName), Pair("username", sid.username), Pair("profileImage", sid.profilePicture.toString()))!!.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    InfinityAPI.makeCustomCall("checkUserExists", Pair("userId", FirebaseAuth.getInstance().currentUser!!.uid))!!.addOnCompleteListener { task2 ->
                        if (task2.isSuccessful) {
                            val userExists2 = (task2.result.data as HashMap<*, *>)["exists"] as Boolean
                            if (userExists2) {
                                Tasks.whenAll(FirebaseAuth.getInstance().currentUser!!.updateProfile(UserProfileChangeRequest.Builder().setDisplayName(sid.displayName).setPhotoUri(sid.profilePicture).build()), FirebaseAuth.getInstance().currentUser!!.updateEmail(sid.email)).addOnCompleteListener { task3 ->
                                    if (task3.isSuccessful) {
                                        callback.setupSuccessful()
                                    } else {
                                        Toast.makeText(this@FinalSetupActivityActivity, "Something wen't wrong. Continuing Anyways!", LENGTH_LONG).show()
                                        callback.setupSuccessful()
                                    }
                                }
                            }
                        } else {

                        }
                    }
                } else {
                    recheckUser()
                }
            }
                    } else {
                        var ppref = FirebaseStorage.getInstance().reference.child("profile_pictures").child(sid.userID + getMimeType(this@FinalSetupActivityActivity, sid.profilePicture))
            ppref.putFile(sid.profilePicture).addOnCompleteListener { t2 ->
                if (t2.isSuccessful) {
                    InfinityAPI.makeCustomCall("createUserUID", Pair("dob", (sid.dob.time.toString())), Pair("userId", FirebaseAuth.getInstance().currentUser!!.uid), Pair("email", sid.email), Pair("displayName", sid.displayName), Pair("username", sid.username), Pair("profileImage", t2.result.downloadUrl.toString()))!!.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            InfinityAPI.makeCustomCall("checkUserExists", Pair("userId", FirebaseAuth.getInstance().currentUser!!.uid))!!.addOnCompleteListener { task2 ->
                                if (task2.isSuccessful) {
                                    val userExists2 = (task2.result.data as HashMap<*, *>)["exists"] as Boolean
                                    if (userExists2) {
                                        Tasks.whenAll(FirebaseAuth.getInstance().currentUser!!.updateProfile(UserProfileChangeRequest.Builder().setDisplayName(sid.displayName).setPhotoUri(t2.result.downloadUrl).build()), FirebaseAuth.getInstance().currentUser!!.updateEmail(sid.email)).addOnCompleteListener { task3 ->
                                            if (task3.isSuccessful) {
                                                callback.setupSuccessful()
                                            } else {
                                                Toast.makeText(this@FinalSetupActivityActivity, "Something wen't wrong. Continuing Anyways!", LENGTH_LONG).show()
                                                callback.setupSuccessful()
                                            }
                                        }
                                    }
                                } else {

                                }
                            }
                        } else {
                            recheckUser()
                        }
                    }
                            } else {
                                callback.setupUnsuccessful()
                            }
                            return@addOnCompleteListener
                        }
                    }
    }

    private fun recheckUser() {
        startActivity(Intent(this, LaunchActivity::class.java))
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        //Go to previous Fragment
        if (supportFragmentManager.backStackEntryCount > 0) {
            super.onBackPressed()
        } else if (supportFragmentManager.backStackEntryCount == 0) {
            ((fragmentManager.findFragmentByTag("android:switcher:" + R.id.fragmentsview + ":" + fragmentsview.currentItem) as Fragment) as FSAAInterface).active()[1].onClick(fragmentsview)
        }
    }

    override fun onSignUp(sid: SignUpDataGoogle) {
        this.sid = sid
        if(sid.profilePicture == null){
            Toast.makeText(this, "You cannot continue without a profile picture.", Toast.LENGTH_LONG)
        }
        fragmentsview.currentItem = RegisterPages.PRIVACY_POLICY.pageNumber
        var next = View.OnClickListener{ onAgreed() }
        var cancel = View.OnClickListener{ onDisagreed() }
        setBOCL(next, cancel)
    }

    override fun onAgreed() {
        val signupdialog = BottomDialog.Builder(this@FinalSetupActivityActivity).setTitle("Signing Up").setContent("Get ready for awesomeness").setCancelable(false).setCustomView(CurrentProfileLoadingView(this@FinalSetupActivityActivity)).show()
        setup(sid!!, FSAASetupCallback(signupdialog, this@FinalSetupActivityActivity))
    }

    override fun onDisagreed() {
        FirebaseAuth.getInstance().currentUser!!.delete()
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this, AuthenticationActivity::class.java))
        finish()
    }

    class FSAASetupCallback(signupdialog: BottomDialog, finalsetupactivityactivity: FinalSetupActivityActivity) : SetupCallback {

        private val sud: BottomDialog = signupdialog
        private val fsaa: FinalSetupActivityActivity = finalsetupactivityactivity

        override fun setupSuccessful() {
            sud.dismiss()
            fsaa.startActivity(Intent(fsaa, SignInOrUpActivity::class.java))
            fsaa.finish()
        }

        override fun setupSuccessfulWithError() {
            sud.dismiss()
            Toast.makeText(fsaa, "Setup Successful but an error occurred. This may be due to trying to upload your Profile Image!", Toast.LENGTH_LONG).show()
            fsaa.startActivity(Intent(fsaa, SignInOrUpActivity::class.java))
            fsaa.finish()
        }

        override fun setupUnsuccessful() {
            sud.dismiss()
            BottomDialog.Builder(fsaa)
                    .setTitle("Error Occurred")
                    .setContent("There was a problem setting up your Socialee Account. This may be down to a network issue. If subsequent sign up attempt's fail" +
                            ", Please Contact Liam Mendes at the email: liammendes6@gmail.com\nThank You for using Socialee Alpha")
                    .setCancelable(false)
                    .setPositiveText("TRY AGAIN").setPositiveTextColor(R.color.md_white_1000).setPositiveBackgroundColor(R.color.md_blue_500)
                    .setPositiveText("CANCEL").setPositiveTextColor(R.color.md_white_1000).setPositiveBackgroundColor(R.color.md_red_500)
                    .onPositive { dialog ->
                        dialog.dismiss()
                        fsaa.onAgreed()
                    }
                    .onNegative { dialog ->
                        dialog.dismiss()
                        fsaa.onDisagreed()
                    }.build()
                    .show()
        }

    }

    class RegisterPagerAdapter(private var fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        private var NUM_ITEMS = RegisterPages.values().size
        override fun getCount(): Int {
            return NUM_ITEMS
        }

        override fun getItem(position: Int): Fragment? {
            when (position) {
                RegisterPages.SETUP_INFO.pageNumber -> {return com.infinityapp.infinity.fragment.SetupInfoFragment()}
                RegisterPages.PRIVACY_POLICY.pageNumber -> {return com.infinityapp.infinity.fragment.PrivacyPolicyFragment()}
            }

            return null
        }

    }

    enum class RegisterPages(val pageNumber: Int){
        SETUP_INFO(0),
        PRIVACY_POLICY(1)
    }
}

private fun InfinityAPI.makeCustomCall(method: String, invokationFinishedCallback: InfinityAPI.InvokeFinishedCallback, keys: ArrayList<String>, vararg datas: String) {
    var map = mutableMapOf<String, String>()
    for (i in keys.indices) {
        map[keys[i]] = datas[i]
    }
    InfinityAPI.makeCustomCall(method, map)!!.addOnCompleteListener { task ->
        if (task.isSuccessful) {
            invokationFinishedCallback.responseReceived(InfinityAPI.oldMethodBridge(task.result.data))
        } else {
            invokationFinishedCallback.responseReceived(null)
        }
    }
}

interface IFSAAInterface {

    fun setBOCL(vocl1: View.OnClickListener, vocl2: View.OnClickListener)

}
