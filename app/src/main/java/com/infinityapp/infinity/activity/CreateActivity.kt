package com.infinityapp.infinity.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.infinityapp.infinity.R
import kotlinx.android.synthetic.main.activity_create.*


class CreateActivity : AppCompatActivity() {

    private var mainActivity: Class<*> = MainActivity::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create)
        add_friend_button.setOnClickListener { addFriend(add_friend_button) }
        create_group_button.setOnClickListener { createGroup(create_group_button) }
        create_close_button.setOnClickListener { cancel(create_close_button) }
    }

    fun cancel(v: View){
        startActivity(Intent(this@CreateActivity, mainActivity))
    }

    fun createGroup(v: View){
        startActivity(Intent(this@CreateActivity, CreateGroupActivity::class.java))
    }

    fun addFriend(v: View){
        startActivity(Intent(this@CreateActivity, AddFriendActivity::class.java))
    }
}