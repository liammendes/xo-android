package com.infinityapp.infinity.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.functions.FirebaseFunctions
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.R
import kotlinx.android.synthetic.main.activity_add_friend.*
import org.json.JSONObject

class AddFriendActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_friend)
        search_username.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(p: CharSequence?, start: Int, before: Int, count: Int) {
                var s = ""
                if (p != null) s = p.toString()
                if (s == InfinityAPI.signedInUser!!.username) {
                    requested_friend_dn.text = getString(R.string.add_self_error)
                    requested_friend_email.text = ""
                    badge_click_to_add.visibility = View.GONE
                    badge_requested.visibility = View.GONE
                    future_friend.setOnClickListener {}
                    return
                }
                requested_friend_dn.text = getString(R.string.loading)
                requested_friend_email.text = ""
                badge_click_to_add.visibility = View.GONE
                badge_requested.visibility = View.GONE
                future_friend.setOnClickListener {}
                var exists: Boolean
                InfinityAPI.makeCustomCall("checkUsernameExists", Pair("username", s))!!.addOnCompleteListener { result ->
                    exists = if (result.isSuccessful) {
                        ((result.result.data as java.util.HashMap<*, *>)["exists"] as Boolean)
                    } else {
                        false
                    }
                    if (!exists) {
                        requested_friend_dn.text = getString(R.string.no_user_error)
                        requested_friend_email.text = ""
                        badge_click_to_add.visibility = View.GONE
                        badge_requested.visibility = View.GONE
                        return@addOnCompleteListener
                    } else {
                        requested_friend_dn.text = getString(R.string.loading)
                        requested_friend_email.text = ""
                        badge_click_to_add.visibility = View.GONE
                        badge_requested.visibility = View.GONE
                        val usernameToUserIddata = HashMap<String, Any>()
                        usernameToUserIddata["username"] = s
                        FirebaseFunctions.getInstance().getHttpsCallable("usernameToUserId").call(usernameToUserIddata).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                InfinityAPI.getUsersDisplayName((task.result.data as HashMap<*, *>)["userId"] as String, object : InfinityAPI.InvokeFinishedCallback {
                                    override fun responseReceived(response: String?) {
                                        if (response == null) {
                                            requested_friend_dn.text = getString(R.string.failed_username_check)
                                            requested_friend_email.text = getString(R.string.try_again)
                                            badge_click_to_add.visibility = View.GONE
                                            badge_requested.visibility = View.GONE
                                            future_friend.setOnClickListener {}
                                            return
                                        }
                                        val jo = JSONObject(response)
                                        requested_friend_dn.text = jo.get("name").toString()
                                        future_friend.setOnClickListener {}
                                        InfinityAPI.getProfilePicture((task.result.data as HashMap<*, *>)["userId"] as String, object : InfinityAPI.InvokeFinishedCallback {
                                            override fun responseReceived(response: String?) {
                                                if (response == null) {
                                                    requested_friend_dn.text = getString(R.string.failed_username_check)
                                                    requested_friend_email.text = getString(R.string.try_again)
                                                    badge_click_to_add.visibility = View.GONE
                                                    badge_requested.visibility = View.GONE
                                                    future_friend.setOnClickListener {}
                                                    return
                                                }
                                                val jo2 = JSONObject(response)
                                                Glide.with(this@AddFriendActivity).load(jo2["profileImage"]).into(requested_friend_img)
                                                val rfet = getString(R.string.username_prefix) + s
                                                requested_friend_email.text = rfet
                                                badge_click_to_add.visibility = View.VISIBLE
                                                future_friend.setOnClickListener { iv ->
                                                    badge_requested.visibility = View.VISIBLE
                                                    badge_click_to_add.visibility = View.GONE
                                                    InfinityAPI.makeCustomCall("addFriend", Pair("userId", InfinityAPI.signedInUser!!.UserID), Pair("userToAdd", (task.result.data as HashMap<*, *>)["userId"] as String))!!.addOnCompleteListener { task ->
                                                        if (!task.isSuccessful) {
                                                            badge_requested.visibility = View.GONE
                                                            badge_click_to_add.visibility = View.VISIBLE
                                                            Snackbar.make(iv, "Failed to send friend request!", Snackbar.LENGTH_SHORT).show()
                                                        } else {
                                                            badge_requested.visibility = View.VISIBLE
                                                            badge_click_to_add.visibility = View.GONE
                                                            future_friend.setOnClickListener {}
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                    }
                                })
                            } else {
                                requested_friend_dn.text = getString(R.string.failed_username_check)
                                requested_friend_email.text = getString(R.string.try_again)
                                badge_click_to_add.visibility = View.GONE
                                badge_requested.visibility = View.GONE
                                future_friend.setOnClickListener {}
                            }
                        }
                    }
                }
                InfinityAPI.makeCustomCall("usernameToUserId", Pair("username", s))!!.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                    }
                }
            }
        })
    }

    fun cancelaf(view: View) {
        view.bringToFront()
        onBackPressed()
    }
}
