package com.infinityapp.infinity.activity

import android.annotation.TargetApi
import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.functions.HttpsCallableResult
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.MainApplication
import com.infinityapp.infinity.R
import kotlinx.android.synthetic.main.activity_requests.*
import org.json.JSONObject

class RequestsActivity : AppCompatActivity() {

    @TargetApi(Build.VERSION_CODES.M)
    fun setLightStatusBar(view: View, activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            var flags = view.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            view.systemUiVisibility = flags
            activity.window.statusBarColor = Color.WHITE

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_requests)
        setLightStatusBar(window.decorView, this)
        requests_bar.setNavigationOnClickListener {
            NavUtils.navigateUpFromSameTask(this)
        }
        requests_bar.title = "Requests"
        val query = FirebaseFirestore.getInstance()
                .collection("users")
                .document(InfinityAPI.signedInUser!!.UserID)
                .collection("requests")

        val options = FirestoreRecyclerOptions.Builder<Request>()
                .setQuery(query, Request::class.java)
                .build()

        var adapter = object : FirestoreRecyclerAdapter<Request, RequestHolder>(options) {

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestHolder {
                var iv = LayoutInflater.from(parent.context).inflate(R.layout.request, parent, false)
                return RequestHolder(iv)
            }

            override fun onBindViewHolder(holder: RequestHolder, position: Int, model: Request) {
                holder.setFrom(model.from)
            }

        }

        requests_root.layoutManager = LinearLayoutManager(this)
        requests_root.adapter = adapter

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //TYPE, USERID
    data class Request(var type: String, var from: String)

    class RequestHolder(iv: View) : RecyclerView.ViewHolder(iv) {
        fun setFrom(from: String) {
            itemView.findViewById<TextView>(R.id.badge_requested).visibility = View.VISIBLE
            InfinityAPI.getProfilePicture(from, object : InfinityAPI.InvokeFinishedCallback {
                override fun responseReceived(response: String?) {
                    Glide.with(MainApplication.instance).load(JSONObject(response).getString("profileImage")).into(itemView.findViewById(R.id.requested_friend_img))
                }

            })
            InfinityAPI.getUsersDisplayName(from, object : InfinityAPI.InvokeFinishedCallback {
                override fun responseReceived(response: String?) {
                    itemView.findViewById<TextView>(R.id.requested_friend_dn).text = JSONObject(response).getString("name")
                }
            })
            InfinityAPI.getUsersEmail(from, object : InfinityAPI.InvokeFinishedCallback {
                override fun responseReceived(response: String?) {
                    itemView.findViewById<TextView>(R.id.requested_friend_email).text = JSONObject(response).getString("email")
                }
            })
            itemView.findViewById<RelativeLayout>(R.id.request_root).setOnClickListener {
                InfinityAPI.makeCustomCall("acceptFriendRequest", Pair("userId", InfinityAPI.signedInUser!!.UserID), Pair("friendUserId", from))!!.addOnCompleteListener { task: Task<HttpsCallableResult> ->
                    if (task.isSuccessful) {
                        Snackbar.make(itemView, "Successfully accepted friend request. You can now chat with this person!", Snackbar.LENGTH_SHORT).show()
                    } else {
                        Snackbar.make(itemView, "Failed to accept friend request. Please try again!", Snackbar.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}
