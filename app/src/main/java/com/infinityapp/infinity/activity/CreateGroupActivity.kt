package com.infinityapp.infinity.activity

//import com.google.firebase.storage.FirebaseStorage
import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.R
import kotlinx.android.synthetic.main.activity_create_group.*
import java.io.File
import java.util.*


class CreateGroupActivity : AppCompatActivity() {

    private var mainActivity: Class<*> = MainActivity::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_group)
    }

    private var selectedImage: Uri? = null
    set(value) {
        if (value == null) return else Glide.with(this).load(value).into(group_image)
        field = value
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 42) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    selectedImage = data.data
                }
            }
        }
    }

    fun cancel(v: View){
        onBackPressed()
    }

    fun getMimeType(context: Context, uri: Uri): String {
        val extension: String

        //Check uri format to avoid null
        if (uri.scheme == ContentResolver.SCHEME_CONTENT) {
            //If scheme is a content
            val mime = MimeTypeMap.getSingleton()
            extension = mime.getExtensionFromMimeType(context.contentResolver.getType(uri))
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(File(uri.path)).toString())

        }

        return extension
    }

    fun next(v: View){
        var possibleGroupId = UUID.randomUUID().toString()

        if (selectedImage == null) {
            Toast.makeText(this@CreateGroupActivity, "You need to choose a Group Image to Continue!", Toast.LENGTH_LONG).show()
            return
        }
        if (create_group_name.text.isBlank()) {
            Toast.makeText(this@CreateGroupActivity, "You need to have a initial Group Name to Continue!", Toast.LENGTH_LONG).show()
            return
        }
        if (create_group_description.text.isBlank()) {
            Toast.makeText(this@CreateGroupActivity, "You need to have a initial Group Description to Continue!", Toast.LENGTH_LONG).show()
            return
        }
        var dialog = findViewById<ProgressBar>(R.id.loader)
        dialog.visibility = View.VISIBLE
        activity_create_group.alpha = 0.5f
        FirebaseStorage.getInstance().getReference("groups_profile_pictures/" + possibleGroupId + "." + getMimeType(this@CreateGroupActivity, selectedImage!!)).putFile(selectedImage!!).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Thread({
                    InfinityAPI.makeCustomCall("createGroup", Pair("description", create_group_description.text.toString()), Pair("groupId", possibleGroupId), Pair("userId", InfinityAPI.signedInUser!!.UserID), Pair("name", create_group_name.text.toString()), Pair("profilePicture", task.result.downloadUrl.toString()))!!.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            dialog.visibility = View.GONE
                            activity_create_group.alpha = 1.0f
                            startActivity(Intent(this@CreateGroupActivity, MainActivity::class.java))
                        } else {
                            dialog.visibility = View.GONE
                            activity_create_group.alpha = 1.0f
                            Toast.makeText(this@CreateGroupActivity, "An Error has occured", Toast.LENGTH_LONG).show()
                        }
                    }
                }).start()
            } else {
                dialog.visibility = View.GONE
                activity_create_group.alpha = 1.0f
                Toast.makeText(this@CreateGroupActivity, "An Error has occured", Toast.LENGTH_LONG).show()
            }
        }
    }

    /***fun next(v: View){
    var possibleGroupId = UUID.randomUUID().toString()

    if(selectedImage == null){
    Toast.makeText(this@CreateGroupActivity, "You need to choose a Group Image to Continue!", Toast.LENGTH_LONG).show()
    return
    }
    if (create_group_name.text.isBlank()) {
    Toast.makeText(this@CreateGroupActivity, "You need to have a initial Group Name to Continue!", Toast.LENGTH_LONG).show()
    return
    }
    if (create_group_description.text.isBlank()) {
    Toast.makeText(this@CreateGroupActivity, "You need to have a initial Group Description to Continue!", Toast.LENGTH_LONG).show()
    return
    }
    var dialog = MaterialDialog.Builder(this@CreateGroupActivity).progressIndeterminateStyle(true).title("Creating Group").content("Uploading Profile Picture and Creating Group").show()
    -***FirebaseRemoteConfig.getInstance().getReference("groups_profile_pictures/" + possibleGroupId + "." + getMimeType(this@CreateGroupActivity, selectedImage!!)).putFile(selectedImage!!).addOnCompleteListener { task ->
    if(task.isSuccessful){
    Thread({
    InfinityAPI.makeCustomCall("createGroup", Pair("description", create_group_description.editText!!.text.toString()), Pair("groupId", possibleGroupId), Pair("userId", InfinityAPI.signedInUser!!.UserID), Pair("name", create_group_name.editText!!.text.toString()), Pair("profilePicture", task.result.downloadUrl.toString()))!!.addOnCompleteListener { task ->
    if (task.isSuccessful) {
    dialog.dismiss()
    startActivity(Intent(this@CreateGroupActivity, mainActivity))
    } else {
    dialog.dismiss()
    Toast.makeText(this@CreateGroupActivity, "An Error has occured", Toast.LENGTH_LONG).show()
    }
    }
    }).start()
    } else {
    dialog.dismiss()
    Toast.makeText(this@CreateGroupActivity, "An Error has occured", Toast.LENGTH_LONG).show()
    }
    }***-
    }***/

    fun changeGroupImage(v: View){
        var readrequestcode = 42
        var int = Intent(Intent.ACTION_OPEN_DOCUMENT)
        int.addCategory(Intent.CATEGORY_OPENABLE)
        int.type = "image/*"
        startActivityForResult(int, readrequestcode)
    }
}
