package com.infinityapp.infinity.activity

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log.d
import android.util.Log.e
import android.view.View
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.*
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.BuildConfig
import com.infinityapp.infinity.MainApplication
import com.infinityapp.infinity.R
import com.infinityapp.infinity.fragment.*
import com.twitter.sdk.android.core.*
import kotlinx.android.synthetic.main.activity_authentication.*
import kotlinx.android.synthetic.main.activity_other.*


class AuthenticationActivity : FragmentActivity(), GoogleApiClient.OnConnectionFailedListener, MoreOptionsFragment.MOListener, AuthEmailPassFragment.OFIListener, ForgotPasswordStep1Fragment.FPS1Listener, ForgotPasswordStep2Fragment.FPS2Listener, NewEmailAccountFragment.NEAListener, GoogleApiClient.ConnectionCallbacks {

    override fun onConnected(p0: Bundle?) {
        //TODO("not implemented")
    }

    override fun onConnectionSuspended(p0: Int) {
        d(LOCAL_TAG, "Connection Failed with no Error Message.")
        Snackbar.make(auth_activity, "No Connection", Snackbar.LENGTH_LONG).setAction("Retry", { signIn(Provider.Google) }).setActionTextColor(resources.getColor(R.color.shamrock_green)).show()
        dialog!!.visibility = View.GONE
        auth_vp.alpha = 1.0f
    }

    private val LOCAL_TAG = MainApplication.instance.createGlobalTag("AuthenticationActivity")
    private val RC_SIGN_IN = 9001
    private var mConnectionCallback: GoogleApiClient.ConnectionCallbacks? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var dialog: ProgressBar? = null

    override fun new_email_account_create_account() {
        finish(); startActivity(Intent(this@AuthenticationActivity, FinalSetupActivityActivity::class.java))
    }
    override fun new_email_account_cancel() {
        auth_vp.currentItem = SetupPages.MORE_OPTIONS.pageNumber
    }
    override fun new_email_account_onCreate() = Unit
    override fun fps2_done() {
        auth_vp.currentItem = SetupPages.MAIN_SETUP.pageNumber
    }

    override fun fps2_onCreate() = Unit
    override fun fps1_next(signInData: ForgotPasswordStep1Fragment.SignInData) {
        FirebaseAuth.getInstance().sendPasswordResetEmail(signInData.email).addOnCompleteListener { task ->
            if(task.isSuccessful){
                auth_vp.currentItem = SetupPages.FORGOT_PASSWORD_STEP_2.pageNumber
            } else {
                AlertDialog.Builder(this).setMessage("There was an error while trying to send a Forget Password Email. Would you like to retry?").setTitle("Error").setNegativeButton("Cancel", { dialogInterface: DialogInterface, i: Int -> dialogInterface.dismiss(); fps1_cancel() }).setPositiveButton("Retry", { dialogInterface: DialogInterface, i: Int -> dialogInterface.dismiss(); fps1_next(signInData) }).show()
            }
        }
    }
    override fun fps1_cancel() {
        auth_vp.currentItem = SetupPages.MAIN_SETUP.pageNumber
    }

    override fun fps1_onCreate() = Unit
    override fun more_options_onCreate() = Unit
    override fun more_options_google() {
        signIn(Provider.Google)
    }
    override fun more_options_twitter() {
        initializeButtons()
        signIn(Provider.Twitter)
    }
    override fun more_options_sign_up() {
        auth_vp.currentItem = SetupPages.NEW_ACCOUNT_PREPARATION.pageNumber
    }
    override fun more_options_back() {
        auth_vp.currentItem = SetupPages.MAIN_SETUP.pageNumber
    }
    override fun auth_email_pass_onCreate() = Unit
    override fun auth_email_pass_next(signInData: AuthEmailPassFragment.SignInData) {
        val page = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.auth_vp + ":" + auth_vp.currentItem)
        if (auth_vp.currentItem == SetupPages.MAIN_SETUP.pageNumber && page != null) {
          (page as AuthEmailPassFragment).setLoading(true)
            try {
                FirebaseAuth.getInstance().signInWithEmailAndPassword(signInData.email, signInData.password!!).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        startActivity(Intent(this@AuthenticationActivity, SignInOrUpActivity::class.java))
                    } else {
                        page.setLoading(false)
                        (page).error("Failed to sign in")
                    }
                }
            } catch (e: Exception){
                (page).setLoading(false)
                (page).error(e.message)
            }
        }
    }
    override fun auth_email_pass_cancel() {
        startActivity(Intent(this@AuthenticationActivity, IntroActivity::class.java))
    }
    override fun auth_email_pass_more_options() {
        auth_vp.currentItem = (SetupPages.MORE_OPTIONS.pageNumber)
    }
    override fun auth_email_pass_forgot_password(signInData: AuthEmailPassFragment.SignInData) {
        val page = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.auth_vp + ":" + auth_vp.currentItem)
        auth_vp.currentItem = (SetupPages.FORGOT_PASSWORD_STEP_1.pageNumber)
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        d(LOCAL_TAG, "Connection Failed with Error Message: ${p0.errorMessage}")
        Snackbar.make(auth_activity, "No Connection", Snackbar.LENGTH_LONG).setAction("Retry", { signIn(Provider.Google) }).setActionTextColor(resources.getColor(R.color.shamrock_green)).show()
        dialog!!.visibility = View.GONE
        auth_vp.alpha = 1.0f
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == RC_SIGN_IN) handleSignInResult(Auth.GoogleSignInApi.getSignInResultFromIntent(data)) else {
                if (data == null) {
                    dialog!!.visibility = View.GONE
                    auth_vp.alpha = 1.0f
                    return
                }
                twitter_button.onActivityResult(requestCode, resultCode, data)
            }
        } catch (e: Exception) {
            Toast.makeText(this, "An Error has occurred!", Toast.LENGTH_LONG).show()
            dialog!!.visibility = View.GONE
            auth_vp.alpha = 1.0f
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)

        initializeSignInProviders()
        initialize()
    }

    fun setupContent(view: TextView) {
        view.movementMethod = LinkMovementMethod.getInstance()

        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()
        FirebaseRemoteConfig.getInstance().setConfigSettings(configSettings)
        FirebaseRemoteConfig.getInstance().setDefaults(R.xml.remote_config_defaults)
        FirebaseRemoteConfig.getInstance().activateFetched()
        view.text = Html.fromHtml(Html.fromHtml((FirebaseRemoteConfig.getInstance().getString("authactivity_legal")).replace("{privacypolicy_link}", FirebaseRemoteConfig.getInstance().getString("privacypolicy_link")).replace("{tos_link}", FirebaseRemoteConfig.getInstance().getString("tos_link")) + "").toString())
    }

    private fun initialize() {
        auth_vp.adapter = SetupPagerAdapter(supportFragmentManager)
    }
    private fun initializeButtons() {
        twitter_button.getInstance(this)
        setupTwitterButton()
    }
    private fun initializeSignInProviders() {
        // Configure Google Sign In
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.default_web_client_id))
                        .requestEmail()
                        .requestProfile()
                        .build())
                .build()

    }
    private fun setupTwitterButton() {
        twitter_button.setCallback(object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>?) {
                handleSignInResult(result)
            }

            override fun failure(exception: TwitterException?) {
                e(LOCAL_TAG, exception!!.message)
                Snackbar.make(auth_activity, "Twitter Authentication Failed", Snackbar.LENGTH_LONG).setAction("Try Again", { signIn(Provider.Twitter) }).setActionTextColor(resources.getColor(R.color.colorPrimary)).show()
                dialog!!.visibility = View.GONE
                auth_vp.alpha = 0.5f
            }
        })
        twitter_button.isTwitterButton = true
    }

    private fun signIn(p: Provider) {
        when(p){
            Provider.Twitter -> signInTwitter()
            Provider.Google -> signInGoogle()
        }
        dialog = findViewById(R.id.loader)
        dialog!!.visibility = View.VISIBLE
        auth_vp.alpha = 0.5f
    }
    private fun signInTwitter() {
        twitter_button.invokelcl()
    }
    private fun signInGoogle() {
        startActivityForResult(Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient), RC_SIGN_IN)
    }
    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            if (MainApplication.instance.getSharedPreferences("MainApplication", Context.MODE_PRIVATE).getBoolean("gso_on_fso", true)) {
                addAuthSignOutListener()
            }
            finishSignIn(GoogleAuthProvider.getCredential(result.signInAccount!!.idToken, null))
        } else {
            signInFailed(result)
            dialog!!.visibility = View.GONE
            auth_vp.alpha = 1.0f
        }
    }
    private fun handleSignInResult(twresult: Result<TwitterSession>?) {
        if (twresult != null) {
            finishSignIn(twresult.data)
        } else {
            //Sign in to XO with Twitter
            signInFailed(twresult)
            dialog!!.visibility = View.GONE
            auth_vp.alpha = 1.0f
        }
    }

    private fun finishSignIn(crroot: TwitterSession?) {
        var credential = TwitterAuthProvider.getCredential(
                crroot!!.authToken.token,
                crroot.authToken.secret)

        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this, { task ->
                    if (task.isSuccessful) {
                        signIn()
                    } else {
                        signInFailed(task)
                    }
                })
    }
    private fun finishSignIn(credential: AuthCredential) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this, { task ->
                    if (task.isSuccessful) {
                        InfinityAPI.firebaseUidToSignIn(FirebaseAuth.getInstance().currentUser!!.uid, object : InfinityAPI.SignedInCallback {
                            override fun signUp() {
                                signIn()
                            }

                            override fun signedIn() {
                                signIn()
                            }

                        })
                    } else {
                        signInFailed(task)
                    }
                })
    }

    private fun signIn() {
        val rNotific = FirebaseAuth.AuthStateListener { value ->
            if (value.currentUser == null) {
                //Signing Out
                InfinityAPI.deinitializeCurrentContexts()
                InfinityAPI.NotificationsManager.unSubscribeFromAll()
            }
        }
        FirebaseAuth.getInstance().addAuthStateListener(rNotific)
        FirebaseAuth.getInstance().addAuthStateListener { FirebaseAuth.getInstance().removeAuthStateListener(rNotific) }
        // Sign in success, update UI with the signed-in user's information
        startActivity(Intent(this, SignInOrUpActivity::class.java))
        finish()
    }
    private fun signInFailed(task: Task<AuthResult>) {
        // If sign in fails, display a message to the user.
        e(LOCAL_TAG, "Sign In Failed w/Error Message (task): ${task.exception!!.message}")
        e(LOCAL_TAG, "Sign In Failed w/Error Localized Message: ${task.exception!!.localizedMessage}")
        if(task.exception!!.message == null){
            task.exception!!.fillInStackTrace()
        }
        dialog!!.visibility = View.GONE
        auth_vp.alpha = 1.0f
        var sb = Snackbar.make(auth_activity, "Failed to Authenticate with Firebase. Please Try Again", Snackbar.LENGTH_LONG)

        sb.setAction("Dismiss", {
            sb.dismiss()
        })
                .setActionTextColor(resources.getColor(R.color.colorPrimary)).show()
    }
    private fun signInFailed(result: GoogleSignInResult) {
        // If sign in fails, display a message to the user.
        e(LOCAL_TAG, "Sign In Failed w/Error Message (result): ${result.status.statusMessage}")
        e(LOCAL_TAG, "Sign In Failed w/Error Status: ${result.status}")
        e(LOCAL_TAG, "Sign In Failed w/ Resolution: ${result.status.resolution}")
        e(LOCAL_TAG, "Status Code: ${result.status.statusCode}")
        if (result.status.statusMessage == null) {
            e(LOCAL_TAG, "Detected Sign In Failed with statusMessage = null. Outputting Debug Text")

        }
        Snackbar.make(auth_activity, "Google Authentication Failed", Snackbar.LENGTH_LONG).setAction("Try Again", { signIn(Provider.Google) }).setActionTextColor(resources.getColor(R.color.colorPrimary)).show()
        dialog!!.visibility = View.GONE
        auth_vp.alpha = 1.0f
    }
    private fun signInFailed(result: Result<TwitterSession>?) {
        e(LOCAL_TAG, result.toString())
        Snackbar.make(auth_activity, "Twitter Authentication Failed", Snackbar.LENGTH_LONG).setAction("Try Again", { signIn(Provider.Twitter) }).setActionTextColor(resources.getColor(R.color.colorPrimary)).show()
        dialog!!.visibility = View.GONE
        auth_vp.alpha = 1.0f
    }


    private fun logoutTwitter() {
        var twitterSession = TwitterCore.getInstance().sessionManager.activeSession
        if (twitterSession != null) {
            ClearCookies(applicationContext)
            TwitterCore.getInstance().sessionManager.clearActiveSession()
        }
    }

    private fun ClearCookies(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null)
            CookieManager.getInstance().flush()
        } else {
            var cookieSyncMngr = CookieSyncManager.createInstance(context)
            cookieSyncMngr.startSync()
            var cookieManager = CookieManager.getInstance()
            cookieManager.removeAllCookie()
            cookieManager.removeSessionCookie()
            cookieSyncMngr.stopSync()
            cookieSyncMngr.sync()
        }
    }

    private fun addAuthSignOutListener() {
        val asl: FirebaseAuth.AuthStateListener = getSignOutAuthStateListener()
        FirebaseAuth.getInstance().addAuthStateListener(asl)
        FirebaseAuth.getInstance().addAuthStateListener { FirebaseAuth.getInstance().removeAuthStateListener(asl) }
    }
    private fun getSignOutAuthStateListener(): FirebaseAuth.AuthStateListener {
        return FirebaseAuth.AuthStateListener { auth ->
            when {
                (auth.currentUser == null) -> {
                    mConnectionCallback = object : GoogleApiClient.ConnectionCallbacks {
                        override fun onConnected(p0: Bundle?) {
                            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback { mGoogleApiClient!!.unregisterConnectionCallbacks(mConnectionCallback!!) }
                            logoutTwitter()
                        }

                        override fun onConnectionSuspended(p0: Int) {
                            //We can No Longer access google api
                            //startActivity(Intent(applicationContext, AuthenticationActivity::class.java))
                            mGoogleApiClient!!.unregisterConnectionCallbacks(mConnectionCallback!!)
                        }

                    }
                    mGoogleApiClient!!.registerConnectionCallbacks(mConnectionCallback as GoogleApiClient.ConnectionCallbacks)
                }
            }
        }
    }

    override fun onBackPressed() {
        auth_email_pass_cancel()
    }

    enum class Provider{
        Twitter, Google
    }
    class SetupPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

	    private var NUM_ITEMS = SetupPages.values().size
        override fun getCount(): Int {
            return NUM_ITEMS
        }

        override fun getItem(position: Int): Fragment? {
            when (position) {
                SetupPages.MAIN_SETUP.pageNumber -> {return com.infinityapp.infinity.fragment.AuthEmailPassFragment()}
                SetupPages.MORE_OPTIONS.pageNumber -> {return com.infinityapp.infinity.fragment.MoreOptionsFragment()}
                SetupPages.FORGOT_PASSWORD_STEP_1.pageNumber -> {return com.infinityapp.infinity.fragment.ForgotPasswordStep1Fragment()}
                SetupPages.FORGOT_PASSWORD_STEP_2.pageNumber -> {return com.infinityapp.infinity.fragment.ForgotPasswordStep2Fragment()}
                SetupPages.NEW_ACCOUNT_PREPARATION.pageNumber -> {return com.infinityapp.infinity.fragment.NewEmailAccountFragment()}
            }

            return null
        }

    }
    enum class SetupPages(val pageNumber: Int){
        MAIN_SETUP(0),
        MORE_OPTIONS(1),
        FORGOT_PASSWORD_STEP_1(2),
        FORGOT_PASSWORD_STEP_2(3),
        NEW_ACCOUNT_PREPARATION(4)
    }

}
