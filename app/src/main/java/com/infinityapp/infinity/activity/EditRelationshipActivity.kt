package com.infinityapp.infinity.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import com.bumptech.glide.Glide
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.R
import com.infinityapp.infinity.fragment.TYPE
import kotlinx.android.synthetic.main.delete_section_friend.*
import kotlinx.android.synthetic.main.notification_section.*
import kotlinx.android.synthetic.main.settings_section.*

class EditRelationshipActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_relationship)
        supportActionBar!!.title = "Edit Chat"

        var isHost = (InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().host == InfinityAPI.signedInUser!!.UserID)

        delete_friend.setOnClickListener {

        }

        if (InfinityAPI.Messaging.retrieveContext() == TYPE.FRIENDS) {
            if (isHost) {
                Glide.with(this).load(InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().guestpp).into(profile_icon)
                profile_title!!.text = InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().guestname
                profile_subtitle!!.text = (InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().hostname + " with " + InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().guestname)
                profile_root.setOnClickListener { nye() }
            } else {
                Glide.with(this).load(InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().hostpp).into(profile_icon)
                profile_title!!.text = InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().hostname
                profile_subtitle!!.text = (InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().guestname + " with " + InfinityAPI.Relationship.retrieveContext()!!.getRelationshipObject().hostname)
                profile_root.setOnClickListener { nye() }
            }
        } else if (InfinityAPI.Messaging.retrieveContext() == TYPE.GROUP) {
            startActivity(Intent(this@EditRelationshipActivity, EditGroupActivity::class.java))
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        notif_root.setOnClickListener {
            if (!notif_checkBox.isChecked) {
                InfinityAPI.NotificationsManager.unSubscribeFromRelationship(InfinityAPI.ChatManager.retrieveContext(), isHost)
            } else {
                InfinityAPI.NotificationsManager.subscribeToRelationship(InfinityAPI.ChatManager.retrieveContext(), isHost)
            }
        }

        notif_checkBox.setOnClickListener {
            if (!notif_checkBox.isChecked) {
                InfinityAPI.NotificationsManager.unSubscribeFromRelationship(InfinityAPI.ChatManager.retrieveContext(), isHost)
            } else {
                InfinityAPI.NotificationsManager.subscribeToRelationship(InfinityAPI.ChatManager.retrieveContext(), isHost)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun nye() {
        Toast.makeText(this, "Not Yet Implemented!", Toast.LENGTH_LONG).show()
    }
}
