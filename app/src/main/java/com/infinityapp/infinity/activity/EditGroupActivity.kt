package com.infinityapp.infinity.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.QuerySnapshot
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.R
import com.infinityapp.infinity.fragment.TYPE
import kotlinx.android.synthetic.main.activity_edit_group.*
import kotlinx.android.synthetic.main.colorscheme_section.*
import kotlinx.android.synthetic.main.notification_section.*
import kotlinx.android.synthetic.main.settings_section.*


class EditGroupActivity : AppCompatActivity() {

    fun goToChrome(view: View) {

        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://us-central1-lzm-projxo.cloudfunctions.net/addUserToGroup?userId=(INSERTUSERIDHERE)&groupId=${InfinityAPI.Group.retrieveContext()!!.groupId}&currentUserId=${InfinityAPI.signedInUser!!.UserID}"))
        startActivity(browserIntent)

    }

    fun done(view: View) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_group)
        supportActionBar!!.title = "Edit Group"

        // use a linear layout manager
        //var llm = LinearLayoutManager(this)
        //eg_members_recyclerview.layoutManager = llm

        members.setHasFixedSize(true)
        var llm = LinearLayoutManager(this)
        members.layoutManager = llm

        InfinityAPI.Group.retrieveContext()!!.groupReference.collection("members").get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                members.adapter = MembersAdapter(task)
            } else {

            }
        }

        if (InfinityAPI.Messaging.retrieveContext() == TYPE.GROUP) {
            Glide.with(this).load(InfinityAPI.Group.retrieveContext()!!.getGroupObject().groupProfilePicture).into(profile_icon)
            profile_title!!.text = InfinityAPI.Group.retrieveContext()!!.getGroupObject().groupName
            profile_subtitle!!.text = InfinityAPI.Group.retrieveContext()!!.getGroupObject().groupDescription
            profile_root.setOnClickListener { }
            if (InfinityAPI.signedInUser!!.UserID != InfinityAPI.Group.retrieveContext()!!.getGroupObject().createdBy) csroot.visibility = View.GONE
        } else if (InfinityAPI.Messaging.retrieveContext() == TYPE.FRIENDS) {
            startActivity(Intent(this@EditGroupActivity, EditRelationshipActivity::class.java))
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        notif_root.setOnClickListener {
            if (!notif_checkBox.isChecked) {
                InfinityAPI.NotificationsManager.unSubscribeFromGroup(InfinityAPI.ChatManager.retrieveContext())
            } else {
                InfinityAPI.NotificationsManager.subscribeToGroup(InfinityAPI.ChatManager.retrieveContext())
            }
        }

        notif_checkBox.setOnClickListener {
            if (!notif_checkBox.isChecked) {
                InfinityAPI.NotificationsManager.unSubscribeFromGroup(InfinityAPI.ChatManager.retrieveContext())
            } else {
                InfinityAPI.NotificationsManager.subscribeToGroup(InfinityAPI.ChatManager.retrieveContext())
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    class MembersAdapter(var tsk: Task<QuerySnapshot>) : RecyclerView.Adapter<MembersAdapter.MAViewHolder>() {

        override fun onBindViewHolder(holder: MAViewHolder, position: Int) {
            var d = tsk.result.documents[position].data
            holder.data = d
            holder.update()
        }

        override fun getItemCount(): Int = tsk.result.size()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MAViewHolder {
            var v = LayoutInflater.from(parent.context).inflate(R.layout.member, parent, false)
            return MAViewHolder(v)
        }

        class MAViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var data: MutableMap<String, Any>? = null

            fun update() {
                (data!!["userReference"] as DocumentReference).get().addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        itemView.findViewById<TextView>(R.id.member_name).text = task.result.getString("Name")
                        itemView.findViewById<TextView>(R.id.member_email).text = task.result.getString("EH")
                        Glide.with(itemView).load(task.result.getString("profileImage")).into(itemView.findViewById(R.id.member_pp))
                    } else {

                    }
                }
            }

        }

    }

}
