package com.infinityapp.infinity.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.github.javiersantos.bottomdialogs.BottomDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.perf.FirebasePerformance
import com.google.firebase.perf.metrics.Trace
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.MainApplication
import com.infinityapp.infinity.R
import com.infinityapp.infinity.dataclasses.CommonRefer
import com.infinityapp.infinity.fragment.TYPE

//SIOUA
class SignInOrUpActivity : AppCompatActivity() {

    private var dialog: BottomDialog? = null
    private var userExists: Boolean = false
    private var mainActivity: Class<*> = MainActivity::class.java
    private var attemptSignIn: Thread = Thread({
        //            Looper.prepare()
        InfinityAPI.makeCustomCall("checkUserExists", Pair("userId", FirebaseAuth.getInstance().currentUser!!.uid))!!.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val data = task.result.data as HashMap<*, *>
                userExists = data["exists"] as Boolean
                if (userExists) {
                    runOnUiThread { signIn() }
                } else if (!InfinityAPI.isConencted()) {
                    runOnUiThread { noInternet() }
                } else if (!userExists && InfinityAPI.isConencted()) {
                    runOnUiThread { createAccount() }
                }
            } else {
                throw task.exception!!
            }
        }
    })
    private var activityTrace: Trace? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in_or_up)

        if(FirebaseAuth.getInstance().currentUser == null){
            MainApplication.instance.startActivity(Intent(this@SignInOrUpActivity, AuthenticationActivity::class.java))
        }

        activityTrace = FirebasePerformance.startTrace("SignInOrUpActivityTimeTrace")
        activityTrace!!.start()
        attemptSignIn.start()
    }
    private fun noInternet() {
        //Implementation Needed
        BottomDialog.Builder(this)
                .setTitle("No Internet")
                .setContent("We couldn't connect to the Infinity Servers. Would you like to try again?")
                .setPositiveText("TRY AGAIN")
                .setNegativeText("CANCEL")
                .setPositiveTextColor(R.color.md_blue_500)
                .setNegativeTextColor(R.color.md_red_500)
                .onPositive { onCreateNB() }
                .onNegative { FirebaseAuth.getInstance().signOut();startActivity(Intent(this@SignInOrUpActivity, mainActivity)); this@SignInOrUpActivity.finish() }
                .autoDismiss(true)
                .setCancelable(false)
                .build().show()
    }
    private fun onCreateNB() {
        attemptSignIn.interrupt()
        attemptSignIn = Thread({
            //            Looper.prepare()
            InfinityAPI.makeCustomCall("checkUserExists", Pair("userId", FirebaseAuth.getInstance().currentUser!!.uid))!!.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val data = task.result.data as HashMap<*, *>
                    userExists = data["exists"] as Boolean
                    if (userExists) {
                        runOnUiThread { signIn() }
                    } else if (!InfinityAPI.isConencted()) {
                        runOnUiThread { noInternet() }
                    } else if (!userExists && InfinityAPI.isConencted()) {
                        runOnUiThread { createAccount() }
                    }
                } else {
                    throw task.exception!!
                }
            }
        })

        attemptSignIn.start()
    }
    override fun onBackPressed() {
        //Do Nothing
    }
    override fun onStop() {
        super.onStop()
        activityTrace!!.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (dialog != null) dialog!!.dismiss()
    }
    private fun createAccount() {
        attemptSignIn.interrupt()
        startActivity(Intent(this, FinalSetupActivityActivity::class.java))
        finish()
    }
    private fun signIn() {
        val signOutMainHandler: (FirebaseAuth) -> Unit = { auth ->
            if (auth.currentUser == null) {
                //Signing Out
                startActivity(Intent(this, AuthenticationActivity::class.java))
            }
        }
        FirebaseAuth.getInstance().addAuthStateListener(signOutMainHandler)
        FirebaseAuth.getInstance().addAuthStateListener { FirebaseAuth.getInstance().removeAuthStateListener(signOutMainHandler) }
        attemptSignIn.interrupt()
        if (intent.action == "openMLA") {

            if (intent.getStringExtra("openMLAR") != "" || intent.getStringExtra("openMLAR") != null) {
                FirebaseFirestore.getInstance().collection("relationships").document(intent.getStringExtra("openMLAR") + "").get().addOnCompleteListener { task ->
                    if (!task.result.exists()) {
                        Log.e(MainApplication.instance.createGlobalTag("SignInOrUpActivity"), "A Fatal error has occured, Continuing")
                        return@addOnCompleteListener
                    }
                    if (task.isSuccessful) {
                        //Start Chat
                        val c = InfinityAPI.RelationshipRefer(intent.getStringExtra("openMLAR"), task.result.reference)
                        c.initialize(object : InfinityAPI.InitializedCallback {
                            override fun initialized() {
                                InfinityAPI.Relationship.initializeContext(c)
                                val i = Intent(this@SignInOrUpActivity, MessageListActivity::class.java)
                                i.putExtra("TYPE", TYPE.FRIENDS)
                                InfinityAPI.ChatManager.initializeContext(CommonRefer(null, null, null, intent.getStringExtra("openMLAR"), task.result.reference, "F"))
                                startActivity(i)
                            }

                        })
                    }
                }
            } else if (intent.getStringExtra("openMLAG") != "" || intent.getStringExtra("openMLAG") != null) {
                FirebaseFirestore.getInstance().collection("groups").document(intent.getStringExtra("openMLAG")).get().addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        //Start Chat
                        val c = InfinityAPI.GroupRefer(task.result.getString("groupId")!!, task.result.getString("groupName")!!, task.result.reference)
                        c.initialize(object : InfinityAPI.InitializedCallback {
                            override fun initialized() {
                                InfinityAPI.Group.initializeContext(c)
                                val i = Intent(this@SignInOrUpActivity, MessageListActivity::class.java)
                                i.putExtra("TYPE", TYPE.GROUP)
                                InfinityAPI.ChatManager.initializeContext(CommonRefer(task.result.getString("groupId"), task.result.getString("groupName"), task.result.reference, null, null, "G"))
                                startActivity(i)
                            }

                        })
                    }
                }
            }

        }

        Thread {
            if (InfinityAPI.signedInUser == null)
                InfinityAPI.firebaseUidToSignIn(FirebaseAuth.getInstance().currentUser!!.uid, object : InfinityAPI.SignedInCallback {
                    override fun signUp() {
                        createAccount()
                    }

                    override fun signedIn() {
                        runOnUiThread {
                            this@SignInOrUpActivity.startActivity(android.content.Intent(this@SignInOrUpActivity, mainActivity))
                            this@SignInOrUpActivity.finish()
                        }
                    }

                }) else {
                runOnUiThread {
                    this@SignInOrUpActivity.startActivity(android.content.Intent(this@SignInOrUpActivity, mainActivity))
                    this@SignInOrUpActivity.finish()
                }
            }
        }.start()
    }
}
