package com.infinityapp.infinity.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.firebase.auth.FirebaseAuth
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.R
import kotlinx.android.synthetic.main.activity_my_account.*

class MyAccountActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        setupCTL()
        loadProfile()

    }

    fun aboutButton(v: View) {
        //startActivity(Intent(this@MyAccountActivity, AboutActivity::class.java))
    }

    fun requestsButton(v: View) {
        startActivity(Intent(this@MyAccountActivity, RequestsActivity::class.java))
    }

    fun profileButton(v: View) {
        //startActivity(Intent(this@MyAccountActivity, ProfileActivity::class.java))
    }

    fun signOut(v: View){
        InfinityAPI.deinitializeCurrentContexts()
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this, LaunchActivity::class.java))
        finish()
    }

    private fun loadProfile() {
        if (InfinityAPI.signedInUser == null) startActivity(Intent(this, SignInOrUpActivity::class.java))
        Glide.with(this).load(InfinityAPI.signedInUser!!.profileImage).into(ma_pi)
        ma_name.text = InfinityAPI.signedInUser!!.Name
        ma_email.text = InfinityAPI.signedInUser!!.EH
    }

    private fun setupCTL() {
        val collapsingToolbarLayout = findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar)
        val appBarLayout = findViewById<AppBarLayout>(R.id.appbar)
        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = false
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = "Settings"
                    isShow = true
                } else if (isShow) {
                    collapsingToolbarLayout.title = " "//carefull there should a space between double quote otherwise it wont work
                    isShow = false
                }

                rel.alpha = (Math.abs(appBarLayout.totalScrollRange.toFloat() / appBarLayout.totalScrollRange.toFloat()) - Math.abs(verticalOffset / appBarLayout.totalScrollRange.toFloat()))
            }
        })
        var isShow = true
        var scrollRange = -1

        if (scrollRange == -1) {
            scrollRange = appBarLayout.totalScrollRange
        }
        if (scrollRange + 1 == 0) {
            collapsingToolbarLayout.title = "Settings"
            isShow = true
        } else if (isShow) {
            collapsingToolbarLayout.title = " "//carefull there should a space between double quote otherwise it wont work
            isShow = false
        }

    }
}