package com.infinityapp.infinity.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.firebase.perf.metrics.AddTrace
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.infinityapp.infinity.R
import kotlinx.android.synthetic.main.activity_intro.*

class IntroActivity : AppCompatActivity() {

    @AddTrace(name = "intro_start_trace", enabled = true)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
        FirebaseRemoteConfig.getInstance().setDefaults(R.xml.remote_config_defaults)
        FirebaseRemoteConfig.getInstance().fetch().addOnCompleteListener {
            FirebaseRemoteConfig.getInstance().activateFetched()
            Glide.with(this).load(FirebaseRemoteConfig.getInstance().getString("intro_image")).into(backdrop)
            gs.setOnClickListener { finish();this.startActivity(Intent(this@IntroActivity, AuthenticationActivity::class.java)) }
            gs.text = FirebaseRemoteConfig.getInstance().getString("intro_gs_button")
            intro_top_label.text = FirebaseRemoteConfig.getInstance().getString("intro_top_label")
            intro_bottom_label.text = FirebaseRemoteConfig.getInstance().getString("intro_bottom_label")
            if (FirebaseRemoteConfig.getInstance().getString("intro_logo") == "{InfinityOfficial}") return@addOnCompleteListener else {
                Glide.with(this).load(FirebaseRemoteConfig.getInstance().getString("intro_logo")).into(intro_infinity_logo)
            }
        }
    }
}
