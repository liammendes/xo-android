package com.infinityapp.infinity.views

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.infinityapp.infinity.MainApplication
import com.infinityapp.infinity.R
import com.infinityapp.infinity.activity.AuthenticationActivity
import com.infinityapp.infinity.utils.DisplayUtils
import com.twitter.sdk.android.core.Callback
import com.twitter.sdk.android.core.Twitter
import com.twitter.sdk.android.core.TwitterCore
import com.twitter.sdk.android.core.TwitterSession
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.twitter.sdk.android.core.internal.CommonUtils
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_other.view.*
import java.lang.ref.WeakReference

/**
 * TODO: document your custom view class.
 */
class ListOptionView : LinearLayout {

    var hView: View? = null
    var title: String? = null
    var subtext: String? = null
    private var seperator: String? = null
    var icon: Drawable? = null
    var enabled: Int = 0
    var isSetupWiz: Boolean = false
    var isTwitterButton: Boolean = false

    internal var activityRef: WeakReference<Activity>? = null
    @Volatile internal var authClient: TwitterAuthClient? = null
    internal var callback: Callback<TwitterSession>? = null
    internal val ERROR_MSG_NO_ACTIVITY = "TwitterLoginButton requires an activity." + " Override getActivity to provide the activity for this button."

    private var activity: AuthenticationActivity? = null

    var lic: CircleImageView? = null
    var titletv: TextView? = null
    var subtexttv: TextView? = null
    var officialBadge: TextView? = null
    private var seperatorlayout: RelativeLayout? = null
    private var seperatortext: TextView? = null

    constructor(context: Context) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs, defStyle)
    }

    fun getInstance(mainActivity: AuthenticationActivity) {
        activity = mainActivity
        this.activityRef = WeakReference(mainActivity)
        this.authClient = TwitterAuthClient()

        checkTwitterCoreAndEnable()
    }

    /**
     * Sets the [com.twitter.sdk.android.core.Callback] to invoke when login completes.
     *
     * @param callback The callback interface to invoke when login completes.
     * @throws java.lang.IllegalArgumentException if callback is null.
     */
    fun setCallback(callback: Callback<TwitterSession>?) {
        if (callback == null) {
            throw IllegalArgumentException("Callback cannot be null")
        }
        this.callback = callback
    }

    /**
     * @return the current [com.twitter.sdk.android.core.Callback]
     */
    fun getCallback(): Callback<TwitterSession> {
        return this.callback!!
    }

    /**
     * Call this method when [android.app.Activity.onActivityResult]
     * is called to complete the authorization flow.
     *
     * @param requestCode the request code used for SSO
     * @param resultCode the result code returned by the SSO activity
     * @param data the result data returned by the SSO activity
     */
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == getTwitterAuthClient().requestCode) {
            getTwitterAuthClient().onActivityResult(requestCode, resultCode, data)
        }
    }

    /**
     * Gets the activity. Override this method if this button was created with a non-Activity
     * context.
     */
    protected fun getActivity(): Activity? {
        return if (isInEditMode) {
            null
        } else {
            activity
        }
    }

    internal fun getTwitterAuthClient(): TwitterAuthClient {
        if (authClient == null) {
            synchronized(RelativeLayout::class.java) {
                if (authClient == null) {
                    authClient = TwitterAuthClient()
                }
            }
        }
        return authClient!!
    }

    private fun checkTwitterCoreAndEnable() {
        //Default (Enabled) in edit mode
        if (isInEditMode) {
            return
        }

        try {
            TwitterCore.getInstance()
        } catch (ex: IllegalStateException) {
            //Disable if TwitterCore hasn't started
            Twitter.getLogger().e(MainApplication.instance.createGlobalTag("TwitterLoginButton - SignInOptionView"), ex.message)
            isEnabled = false
        }

    }

    fun invokelcl() {
        LoginClickListener().onClick(twitter_button)
    }

    private inner class LoginClickListener : View.OnClickListener {

        override fun onClick(view: View) {
            checkCallback(callback)
            checkActivity(activityRef!!.get())

            getTwitterAuthClient().authorize(activityRef!!.get()!!, callback)
        }

        private fun checkCallback(callback: Callback<*>?) {
            if (callback == null) {
                CommonUtils.logOrThrowIllegalStateException(TwitterCore.TAG,
                        "Callback must not be null, did you call setCallback?")
            }
        }

        private fun checkActivity(activity: Activity?) {
            if (activity == null || activity.isFinishing) {
                CommonUtils.logOrThrowIllegalStateException(TwitterCore.TAG,
                        ERROR_MSG_NO_ACTIVITY)
            }
        }
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        var hView = inflate(context, R.layout.list_option_view, this)

        val attri = context.obtainStyledAttributes(attrs, R.styleable.ListOptionView)

        title = attri.getString(R.styleable.ListOptionView_lov_title)
        subtext = attri.getString(R.styleable.ListOptionView_lov_subtext)
        icon = attri.getDrawable(R.styleable.ListOptionView_lov_icon)
        enabled = attri.getInt(R.styleable.ListOptionView_lov_seperatore, 0)
        seperator = attri.getString(R.styleable.ListOptionView_lov_seperator_name)
        isSetupWiz = attri.getBoolean(R.styleable.ListOptionView_isSetupWiz, false)
        isTwitterButton = attri.getBoolean(R.styleable.ListOptionView_isTwitterButton, false)

        this.hView = hView

        if(isSetupWiz){
            hView.findViewById<View>(R.id.lovvi_seperatorz).left = DisplayUtils.convertDpToPixel(90)
            val imagePadding = DisplayUtils.convertDpToPixel(4)
            hView.findViewById<CircleImageView>(R.id.lovvi_icon).setPadding(imagePadding, imagePadding, imagePadding, imagePadding)
            hView.findViewById<CircleImageView>(R.id.lovvi_icon).scaleType = ImageView.ScaleType.CENTER_CROP
        }

        if (title != null)
            hView.findViewById<TextView>(R.id.lovvi_title).st(title)

        if (subtext != null)
            hView.findViewById<TextView>(R.id.lovvi_subtext).st(subtext)

        if (icon != null)
            hView.findViewById<CircleImageView>(R.id.lovvi_icon).si(icon)

        lic = hView.findViewById(R.id.lovvi_icon)

        if (enabled == 1) {
            hView.findViewById<RelativeLayout>(R.id.lovvi_seperatorl).visibility = View.VISIBLE
            hView.findViewById<TextView>(R.id.lovvi_seperatortext).st(seperator)
        } else if (enabled == 0 || seperator == null) {
            hView.findViewById<RelativeLayout>(R.id.lovvi_seperatorl).visibility = View.GONE
        }
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int, view: View) {
        // Load attributes
        var hView = view

        this.hView = hView

        titletv = hView.findViewById(R.id.lovvi_title)
        subtexttv = hView.findViewById(R.id.lovvi_subtext)
        officialBadge = hView.findViewById(R.id.official_group_lov)
        lic = hView.findViewById(R.id.lovvi_icon)
        seperatorlayout = hView.findViewById(R.id.lovvi_seperatorl)
        seperatortext = hView.findViewById(R.id.lovvi_seperatortext)
    }

    constructor(context: Context?, view: View) : super(context) {
        init(context!!, null, 0, view)
    }

}

private fun CircleImageView.si(icon: Drawable?) {
    setImageDrawable(icon)
}

private fun TextView.st(subtext: String?) {
    text = subtext
}
