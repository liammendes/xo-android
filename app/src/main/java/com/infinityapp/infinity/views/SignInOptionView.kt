package com.infinityapp.infinity.views

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.infinityapp.infinity.MainApplication
import com.infinityapp.infinity.R
import com.infinityapp.infinity.activity.AuthenticationActivity
import com.twitter.sdk.android.core.Callback
import com.twitter.sdk.android.core.Twitter
import com.twitter.sdk.android.core.TwitterCore
import com.twitter.sdk.android.core.TwitterSession
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.twitter.sdk.android.core.internal.CommonUtils
import kotlinx.android.synthetic.main.sign_in_option.view.*
import java.lang.ref.WeakReference


/**
 * TODO: document your custom view class.
 */
class SignInOptionView : RelativeLayout {
    private var mtitle: String? = null // TODO: use a default from R.string...
    private var micon: Drawable? = null // TODO: use a default from R.dimen...
    private var mtint: Int? = null // TODO: use a default from R.dimen...
    private var mtintlv: Int? = null // TODO: use a default from R.dimen...
    var _main: AuthenticationActivity? = null
    internal val ERROR_MSG_NO_ACTIVITY = "TwitterLoginButton requires an activity." + " Override getActivity to provide the activity for this button."
    /**
     * Gets the example drawable attribute value.
     *
     * @return The example drawable attribute value.
     */
    /**
     * Sets the view's example drawable attribute value. In the example view, this drawable is
     * drawn above the text.
     *
     * @param exampleDrawable The example drawable attribute value to use.
     */

    /**
     * Gets the example string attribute value.
     *
     * @return The example string attribute value.
     */
    /**
     * Sets the view's example string attribute value. In the example view, this string
     * is the text to draw.
     *
     * @param exampleString The example string attribute value to use.
     */
    var title: String?
        get() = mtitle
        set(stringTitle) {
            mtitle = stringTitle
        }

    /**
     * Gets the example color attribute value.
     *
     * @return The example color attribute value.
     */
    /**
     * Sets the view's example color attribute value. In the example view, this color
     * is the font color.
     *
     * @param exampleColor The example color attribute value to use.
     */

    /**
     * Gets the example dimension attribute value.
     *
     * @return The example dimension attribute value.
     */
    /**
     * Sets the view's example dimension attribute value. In the example view, this dimension
     * is the font size.
     *
     * @param exampleDimension The example dimension attribute value to use.
     */
    var icon: Drawable?
        get() = micon
        set(icona) {
            micon = icona
        }

    var tint: Int?
        get() = mtint
        set(tintv) {
            mtint = tintv
        }

    var logotint: Int?
        get() = mtintlv
        set(tintlv) {
            mtintlv = tintlv
        }

    var isTwitterButton: Boolean = false

    internal var activityRef: WeakReference<Activity>? = null
    @Volatile internal var authClient: TwitterAuthClient? = null
    internal var callback: Callback<TwitterSession>? = null

    private var activity: AuthenticationActivity? = null

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)

    }

    fun getInstance(mainActivity: AuthenticationActivity) {
        _main = mainActivity
        this.activityRef = WeakReference(mainActivity)
        this.authClient = TwitterAuthClient()

        checkTwitterCoreAndEnable()
    }

    /**
     * Sets the [com.twitter.sdk.android.core.Callback] to invoke when login completes.
     *
     * @param callback The callback interface to invoke when login completes.
     * @throws java.lang.IllegalArgumentException if callback is null.
     */
    fun setCallback(callback: Callback<TwitterSession>?) {
        if (callback == null) {
            throw IllegalArgumentException("Callback cannot be null")
        }
        this.callback = callback
    }

    /**
     * @return the current [com.twitter.sdk.android.core.Callback]
     */
    fun getCallback(): Callback<TwitterSession> {
        return this.callback!!
    }

    /**
     * Call this method when [android.app.Activity.onActivityResult]
     * is called to complete the authorization flow.
     *
     * @param requestCode the request code used for SSO
     * @param resultCode the result code returned by the SSO activity
     * @param data the result data returned by the SSO activity
     */
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == getTwitterAuthClient().requestCode) {
            getTwitterAuthClient().onActivityResult(requestCode, resultCode, data)
        }
    }

    /**
     * Gets the activity. Override this method if this button was created with a non-Activity
     * context.
     */
    protected fun getActivity(): Activity? {
        return if (isInEditMode) {
            null
        } else {
            _main
        }
    }

    fun changeTint(a: Int){
        content.findViewById<ImageView>(R.id.icon).imageTintList = ColorStateList.valueOf(logotint!!)
        content.findViewById<RelativeLayout>(R.id.pb2).backgroundTintList = ColorStateList.valueOf(tint!!)
        content.findViewById<TextView>(R.id.title).setTextColor(tint!!)
        content.findViewById<RelativeLayout>(R.id.content).backgroundTintList = ColorStateList.valueOf(tint!!)
    }

    internal fun getTwitterAuthClient(): TwitterAuthClient {
        if (authClient == null) {
            synchronized(RelativeLayout::class.java) {
                if (authClient == null) {
                    authClient = TwitterAuthClient()
                }
            }
        }
        return authClient!!
    }

    private fun checkTwitterCoreAndEnable() {
        //Default (Enabled) in edit mode
        if (isInEditMode) {
            return
        }

        try {
            TwitterCore.getInstance()
        } catch (ex: IllegalStateException) {
            //Disable if TwitterCore hasn't started
            Twitter.getLogger().e(MainApplication.instance.createGlobalTag("TwitterLoginButton - SignInOptionView"), ex.message)
            isEnabled = false
        }

    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.SignInOptionView, defStyle, 0)

        title = a.getString(
                R.styleable.SignInOptionView_OptTitle)
        // Use getDimensionPixelSize or getDimensionPixelOffset when dealing with
        // values that should fall on pixel boundaries.
        icon = a.getDrawable(
                R.styleable.SignInOptionView_OptIcon)

        tint = a.getColor(R.styleable.SignInOptionView_OptTint, resources.getColor(R.color.md_deep_purple_500))

        logotint = a.getColor(R.styleable.SignInOptionView_OptLogoTint, resources.getColor(R.color.md_white_1000))

        var hView = inflate(context, R.layout.sign_in_option, this)
        var content = hView.findViewById<RelativeLayout>(R.id.content)
        content.findViewById<TextView>(R.id.title).text = title
        content.findViewById<ImageView>(R.id.icon).setImageDrawable(icon)
        content.findViewById<ImageView>(R.id.icon).imageTintList = ColorStateList.valueOf(logotint!!)
        content.findViewById<RelativeLayout>(R.id.pb2).backgroundTintList = ColorStateList.valueOf(tint!!)
        content.findViewById<TextView>(R.id.title).setTextColor(tint!!)
        content.findViewById<RelativeLayout>(R.id.content).backgroundTintList = ColorStateList.valueOf(tint!!)

        a.recycle()
    }

    fun invokelcl() {
        //LoginClickListener().onClick(twitter_button)
    }

    private inner class LoginClickListener : View.OnClickListener {

        override fun onClick(view: View) {
            checkCallback(callback)
            checkActivity(activityRef!!.get())

            getTwitterAuthClient().authorize(activityRef!!.get()!!, callback)
        }

        private fun checkCallback(callback: Callback<*>?) {
            if (callback == null) {
                CommonUtils.logOrThrowIllegalStateException(TwitterCore.TAG,
                        "Callback must not be null, did you call setCallback?")
            }
        }

        private fun checkActivity(activity: Activity?) {
            if (activity == null || activity.isFinishing) {
                CommonUtils.logOrThrowIllegalStateException(TwitterCore.TAG,
                        ERROR_MSG_NO_ACTIVITY)
            }
        }
    }

}
