package com.infinityapp.infinity.views

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.R
import kotlinx.android.synthetic.main.user_loading_view.view.*

class CurrentProfileLoadingView : RelativeLayout {
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.CurrentProfileLoadingView, defStyle, 0)

        var hView = RelativeLayout.inflate(context, R.layout.user_loading_view, this)
        if (InfinityAPI.signedInUser == null) {
            //One of these values being null can cause a crash
            user_profile_displayName_1.text = FirebaseAuth.getInstance().currentUser!!.displayName
            user_profile_email_1.text = FirebaseAuth.getInstance().currentUser!!.email
            Glide.with(this).load(FirebaseAuth.getInstance().currentUser!!.photoUrl).into(user_profile_picture_1)
        } else {
            user_profile_displayName_1.text = InfinityAPI.signedInUser!!.Name
            user_profile_email_1.text = InfinityAPI.signedInUser!!.EH
            Glide.with(this).load(InfinityAPI.signedInUser!!.profileImage).into(user_profile_picture_1)
        }
        a.recycle()

    }
}
