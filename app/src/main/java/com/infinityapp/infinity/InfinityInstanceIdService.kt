package com.infinityapp.infinity

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.infinityapp.InfinityAPI

/**
 * Created by liammendes on 06/01/2018.
 */

class InfinityInstanceIdService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        // Get updated InstanceID token.
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d(MainApplication.instance.createGlobalTag("InfinityInstanceIdService"), "Refreshed token: " + refreshedToken!!)

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        InfinityAPI.NotificationsManager.sendRegistrationToServer(refreshedToken)
    }


}