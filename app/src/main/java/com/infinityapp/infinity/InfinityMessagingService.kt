package com.infinityapp.infinity

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.Icon
import android.os.Build
import android.os.Build.VERSION_CODES.O
import androidx.annotation.RequiresApi
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.activity.LaunchActivity
import com.infinityapp.infinity.fragment.TYPE
import com.infinityapp.infinity.receiver.NotificationBroadcastReceiver
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

/**
 * Created by liammendes on 06/01/2018.
 */

class InfinityMessagingService : FirebaseMessagingService() {

    private val NMR: String = "newMessageRelationships"
    private val NMG: String = "newMessageGroups"

    data class NotificationData(val t: TYPE, val m: String, val id: String)

    object NotificationMessagingManager {

        var currentId = 0
        var currentNotifications: MutableMap<Int, Notification> = mutableMapOf()
        var relationships: MutableMap<String, Int> = mutableMapOf()
        var groups: MutableMap<String, Int> = mutableMapOf()
        var conversationData: MutableMap<Int, NotificationData> = mutableMapOf()

        fun getNotifId(): Int = currentId
        fun getNotifId(id: String, t: TYPE): Int {
            return if (t == TYPE.FRIENDS) relationships[id]!! else groups[id]!!
        }

        fun changenotifId() {
            currentId++
        }

        fun nullContexts() {
            relationships = mutableMapOf()
            groups = mutableMapOf()
            currentNotifications.forEach { notifications ->
                var notificationManager = MainApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.cancel(notifications.key)
            }
            currentNotifications = mutableMapOf()
            conversationData = mutableMapOf()
        }

        fun getConversationData(i: Int): NotificationData {
            return conversationData[i]!!
        }

        fun getMessageReplyIntentRelationships(notifId: Int, relId: String, ims: InfinityMessagingService): PendingIntent {

            val intent: Intent
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // start a
                // (i)  broadcast receiver which runs on the UI thread or
                // (ii) service for a background task to b executed , but for the purpose of
                // this codelab, will be doing a broadcast receiver
                intent = Intent(MainApplication.instance.applicationContext, NotificationBroadcastReceiver::class.java)
                intent.action = "REPLY_ACTION"
                intent.putExtra("isNotificationReplyRelationships", true)
                intent.putExtra("notifId", notifId)
                return PendingIntent.getBroadcast(MainApplication.instance.applicationContext, 100, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT)
            } else {
                // start your activity for Android M and below
                intent = Intent(MainApplication.instance.applicationContext, LaunchActivity::class.java)
                intent.action = MainApplication.Actions.MLA
                intent.putExtra(MainApplication.Actions.MLA, "relationships/$relId")
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                return PendingIntent.getActivity(ims.applicationContext, 100, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT)
            }

        }

        fun getMessageReplyIntentGroups(notifId: Int, groupId: String, ims: InfinityMessagingService): PendingIntent {

            val intent: Intent
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // start a
                // (i)  broadcast receiver which runs on the UI thread or
                // (ii) service for a background task to b executed , but for the purpose of
                // this codelab, will be doing a broadcast receiver
                intent = Intent(MainApplication.instance.applicationContext, NotificationBroadcastReceiver::class.java)
                intent.action = "REPLY_ACTION"
                intent.putExtra("isNotificationReplyGroups", true)
                intent.putExtra("notifId", notifId)
                return PendingIntent.getBroadcast(MainApplication.instance.applicationContext, 100, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT)
            } else {
                // start your activity for Android M and below
                intent = Intent(MainApplication.instance.applicationContext, LaunchActivity::class.java)
                intent.action = "openMLA"
                intent.putExtra("openMLA", "groups/" + groupId)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                return PendingIntent.getActivity(ims, 100, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT)
            }

        }

        fun getBitmapFromURL(src: String): Bitmap? {
            return try {
                val url = URL(src)
                val connection = url.openConnection() as HttpURLConnection
                connection.doInput = true
                connection.connect()
                val input = connection.inputStream
                BitmapFactory.decodeStream(input)
            } catch (e: IOException) {
                e.printStackTrace()
                null
            }

        }

        @RequiresApi(O)
        fun OaddNotificationMessageRelationship(timestamp: String, senderDn: String, message: String, sender: String, relId: String, notifId: Int, ims: InfinityMessagingService) {
            InfinityAPI.getProfilePicture(sender, object : InfinityAPI.InvokeFinishedCallback {
                override fun responseReceived(response: String?) {
                    val roundDrawable = RoundedBitmapDrawableFactory.create(MainApplication.instance.resources, getBitmapFromURL(response!!))
                    roundDrawable.isCircular = true

                    // Key for the string that's delivered in the action's intent.
                    val KEY_TEXT_REPLY = "key_text_reply"
                    val replyLabel = MainApplication.instance.resources.getString(R.string.reply_label) + senderDn
                    val remoteInput = RemoteInput.Builder(KEY_TEXT_REPLY)
                            .setLabel(replyLabel)
                            .setAllowFreeFormInput(true)
                            .build()

                    // Build a PendingIntent for the reply action to trigger.
                    val replyPendingIntent = getMessageReplyIntentRelationships(notifId, relId, ims)

                    conversationData.put(notifId, NotificationData(TYPE.FRIENDS, message, relId))

                    val action = Notification.Action.Builder(Icon.createWithResource(MainApplication.instance, R.drawable.ic_reply_black_24dp),
                            "Reply", replyPendingIntent)
                            .addRemoteInput(remoteInput)
                            .setAllowGeneratedReplies(true)
                            .build()

                    var openRelationshipIntent = Intent(MainApplication.instance.applicationContext, LaunchActivity::class.java)
                    openRelationshipIntent.action = "openMLA"
                    openRelationshipIntent.putExtra("openMLA", "relationships/" + relId)
                    openRelationshipIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                    // Because clicking the notification opens a new ("special") activity, there's
                    // no need to create an artificial back stack.
                    var resultPendingIntent =
                            PendingIntent.getActivities(MainApplication.instance.applicationContext, 0, arrayOf(openRelationshipIntent), PendingIntent.FLAG_UPDATE_CURRENT)

                    var notif = Notification.Builder(MainApplication.instance.applicationContext, relId)
                            .setSmallIcon(R.drawable.ic_infinity)
                            .setColor(MainApplication.instance.resources.getColor(R.color.md_blue_A400, MainApplication.instance.theme))
                            .setWhen(timestamp.split('.')[0].toLong())
                            .setShowWhen(true)
                            .setContentTitle(senderDn)
                            .setContentText(message)
                            .setLargeIcon(roundDrawable.bitmap).addAction(action)
                            .setContentIntent(resultPendingIntent)

                    if (!relationships.contains(relId)) {
                        notif.addAction(action)
                        relationships.put(relId, notifId)
                        var builtNewNotif = notif.build()
                        currentNotifications.put(notifId, builtNewNotif)
                        (MainApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(notifId, builtNewNotif)
                    } else {
                        var builtNewNotif = notif.build()
                        currentNotifications[notifId] = builtNewNotif
                        (MainApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(notifId, builtNewNotif)
                    }
                }

            })
        }

        @RequiresApi(O)
        fun OaddNotificationMessageGroup(timestamp: String, senderDn: String, message: String, sender: String, groupId: String, groupName: String, notifId: Int, ims: InfinityMessagingService) {
            InfinityAPI.getProfilePicture(sender, object : InfinityAPI.InvokeFinishedCallback {
                override fun responseReceived(response: String?) {
                    val roundDrawable = RoundedBitmapDrawableFactory.create(MainApplication.instance.resources, getBitmapFromURL(response!!))
                    roundDrawable.isCircular = true

                    // Key for the string that's delivered in the action's intent.
                    val KEY_TEXT_REPLY = "key_text_reply"
                    val replyLabel = MainApplication.instance.resources.getString(R.string.reply_label) + senderDn
                    val remoteInput = RemoteInput.Builder(KEY_TEXT_REPLY)
                            .setLabel(replyLabel)
                            .setAllowFreeFormInput(true)
                            .build()

                    // Build a PendingIntent for the reply action to trigger.
                    val replyPendingIntent = getMessageReplyIntentGroups(notifId, groupId, ims)

                    conversationData.put(notifId, NotificationData(TYPE.GROUP, message, groupId))

                    val action = Notification.Action.Builder(R.drawable.ic_reply_black_24dp,
                            "Reply", replyPendingIntent)
                            .addRemoteInput(remoteInput)
                            .build()

                    var openRelationshipIntent = Intent(MainApplication.instance.applicationContext, LaunchActivity::class.java)
                    openRelationshipIntent.action = "openMLA"
                    openRelationshipIntent.putExtra("openMLA", "groups/" + groupId)
                    openRelationshipIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                    // Because clicking the notification opens a new ("special") activity, there's
                    // no need to create an artificial back stack.
                    var resultPendingIntent =
                            PendingIntent.getActivities(MainApplication.instance.applicationContext, 0, arrayOf(openRelationshipIntent), PendingIntent.FLAG_UPDATE_CURRENT)

                    var notif = Notification.Builder(MainApplication.instance.applicationContext, groupId)
                            .setSmallIcon(R.drawable.ic_infinity)
                            .setColor(MainApplication.instance.resources.getColor(R.color.md_blue_A400, MainApplication.instance.theme))
                            .setWhen(timestamp.split('.')[0].toLong())
                            .setShowWhen(true)
                            .setContentTitle(senderDn + " ― " + groupName)
                            .setContentText(message)
                            .setLargeIcon(roundDrawable.bitmap)
                            .setContentIntent(resultPendingIntent)
                            .addAction(action)

                    if (!groups.contains(groupId)) {
                        groups.put(groupId, notifId)
                        var builtNewNotif = notif.build()
                        currentNotifications.put(notifId, builtNewNotif)
                        (MainApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(notifId, builtNewNotif)
                    } else {
                        var builtNewNotif = notif.build()
                        currentNotifications[notifId] = builtNewNotif
                        (MainApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(notifId, builtNewNotif)
                    }
                }

            })
        }

        fun addNotificationMessageRelationship(timestamp: String, senderDn: String, message: String, sender: String, relId: String, notifId: Int, ims: InfinityMessagingService) {
            InfinityAPI.getProfilePicture(sender, object : InfinityAPI.InvokeFinishedCallback {
                override fun responseReceived(response: String?) {
                    val roundDrawable = RoundedBitmapDrawableFactory.create(MainApplication.instance.resources, getBitmapFromURL(response!!))
                    roundDrawable.isCircular = true

                    // Key for the string that's delivered in the action's intent.
                    val KEY_TEXT_REPLY = "key_text_reply"
                    val replyLabel = MainApplication.instance.resources.getString(R.string.reply_label) + senderDn
                    val remoteInput = RemoteInput.Builder(KEY_TEXT_REPLY)
                            .setLabel(replyLabel)
                            .setAllowFreeFormInput(true)
                            .build()

                    // Build a PendingIntent for the reply action to trigger.
                    val replyPendingIntent = getMessageReplyIntentRelationships(notifId, relId, ims)

                    conversationData.put(notifId, NotificationData(TYPE.FRIENDS, message, relId))

                    val action = Notification.Action.Builder(R.drawable.ic_reply_black_24dp,
                            "Reply", replyPendingIntent)
                            .addRemoteInput(remoteInput)
                            .build()

                    var openRelationshipIntent = Intent(MainApplication.instance.applicationContext, LaunchActivity::class.java)
                    openRelationshipIntent.action = "openMLA"
                    openRelationshipIntent.putExtra("openMLA", "relationships/" + relId)
                    openRelationshipIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                    // Because clicking the notification opens a new ("special") activity, there's
                    // no need to create an artificial back stack.
                    var resultPendingIntent =
                            PendingIntent.getActivities(MainApplication.instance.applicationContext, 0, arrayOf(openRelationshipIntent), PendingIntent.FLAG_UPDATE_CURRENT)

                    var notif = Notification.Builder(MainApplication.instance.applicationContext)
                            .setSmallIcon(R.drawable.ic_infinity)
                            .setColor(MainApplication.instance.resources.getColor(R.color.md_blue_A400))
                            .setWhen(timestamp.split('.')[0].toLong())
                            .setShowWhen(true)
                            .setContentTitle(senderDn)
                            .setContentText(message)
                            .setLargeIcon(roundDrawable.bitmap).addAction(action)
                            .setContentIntent(resultPendingIntent)
                            .addAction(action)

                    if (!relationships.contains(relId)) {
                        relationships.put(relId, notifId)
                        var builtNewNotif = notif.build()
                        currentNotifications.put(notifId, builtNewNotif)
                        (MainApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(notifId, builtNewNotif)
                    } else {
                        var builtNewNotif = notif.build()
                        currentNotifications[notifId] = builtNewNotif
                        (MainApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(notifId, builtNewNotif)
                    }
                }

            })
        }

        fun addNotificationMessageGroup(timestamp: String, senderDn: String, message: String, sender: String, groupId: String, groupName: String, notifId: Int, ims: InfinityMessagingService) {
            InfinityAPI.getProfilePicture(sender, object : InfinityAPI.InvokeFinishedCallback {
                override fun responseReceived(response: String?) {
                    val roundDrawable = RoundedBitmapDrawableFactory.create(MainApplication.instance.resources, getBitmapFromURL(response!!))
                    roundDrawable.isCircular = true

                    // Key for the string that's delivered in the action's intent.
                    val KEY_TEXT_REPLY = "key_text_reply"
                    val replyLabel = MainApplication.instance.resources.getString(R.string.reply_label) + senderDn
                    val remoteInput = RemoteInput.Builder(KEY_TEXT_REPLY)
                            .setLabel(replyLabel)
                            .setAllowFreeFormInput(true)
                            .build()

                    // Build a PendingIntent for the reply action to trigger.
                    val replyPendingIntent = getMessageReplyIntentGroups(notifId, groupId, ims)

                    conversationData.put(notifId, NotificationData(TYPE.GROUP, message, groupId))

                    val action = Notification.Action.Builder(R.drawable.ic_reply_black_24dp,
                            "Reply", replyPendingIntent)
                            .addRemoteInput(remoteInput)
                            .build()

                    var openRelationshipIntent = Intent(MainApplication.instance.applicationContext, LaunchActivity::class.java)
                    openRelationshipIntent.action = "openMLA"
                    openRelationshipIntent.putExtra("openMLA", "groups/" + groupId)
                    openRelationshipIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                    // Because clicking the notification opens a new ("special") activity, there's
                    // no need to create an artificial back stack.
                    var resultPendingIntent =
                            PendingIntent.getActivities(MainApplication.instance.applicationContext, 0, arrayOf(openRelationshipIntent), PendingIntent.FLAG_UPDATE_CURRENT)

                    var notif = Notification.Builder(MainApplication.instance.applicationContext)
                            .setSmallIcon(R.drawable.ic_infinity)
                            .setColor(MainApplication.instance.resources.getColor(R.color.md_blue_A400))
                            .setWhen(timestamp.split('.')[0].toLong())
                            .setShowWhen(true)
                            .setContentTitle(senderDn + " ― " + groupName)
                            .setContentText(message)
                            .setLargeIcon(roundDrawable.bitmap)
                            .setContentIntent(resultPendingIntent)
                            .addAction(action)

                    if (!groups.contains(groupId)) {
                        groups.put(groupId, notifId)
                        var builtNewNotif = notif.build()
                        currentNotifications.put(notifId, builtNewNotif)
                        (MainApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(notifId, builtNewNotif)
                    } else {
                        var builtNewNotif = notif.build()
                        currentNotifications[notifId] = builtNewNotif
                        (MainApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(notifId, builtNewNotif)
                    }
                }

            })
        }

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        if (remoteMessage == null) return
        if (remoteMessage.data["type"]!!.contains(NMR)) {
            if (remoteMessage.data["messageType"]!!.endsWith("M")) {
                var message: String = remoteMessage.data["message"]!!
                var messagePath: String = remoteMessage.data["messagePath"]!!
                var messageNumber: String = remoteMessage.data["messageNumber"]!!
                var relId: String = remoteMessage.data["relId"]!!
                var senderDn: String = remoteMessage.data["senderDn"]!!
                var sender: String = remoteMessage.data["sender"]!!
                var timestamp: String = remoteMessage.data["timestamp"]!!

                if (InfinityAPI.signedInUser == null) {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(relId); return
                }

                if (sender == InfinityAPI.signedInUser!!.UserID) {
                    return
                }

                InfinityAPI.NotificationsManager.setNewMessageRelationship(relId, messageNumber)
                if (!NotificationMessagingManager.relationships.containsKey(relId)) {
                    // The id of the channel.
                    val id = relId
                    // The user-visible name of the channel.
                    InfinityAPI.getOtherNameRelationship(relId, object : InfinityAPI.InvokeFinishedCallback {
                        override fun responseReceived(response: String?) {
                            val name = "Chat with " + response
                            // The user-visible description of the channel.
                            val description = "A MainApplication Chat with " + InfinityAPI.signedInUser!!.Name + "'s Friend, " + response + ". To Configure Notification's for " + name + ", Open the Chat in the MainApplication App."
                            val importance = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                NotificationManager.IMPORTANCE_HIGH
                            } else {
                                4
                            }
                            val mChannel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                NotificationChannel(id, name, importance)
                            } else {
                                null
                            }
                            // Configure the notification channel.
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                mChannel!!.description = description
                                mChannel.enableLights(true)
                                // Sets the notification light color for notifications posted to this
                                // channel, if the device supports this feature.
                                mChannel.lightColor = Color.RED
                                mChannel.enableVibration(true)
                                mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                                (MainApplication.instance.getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(mChannel)
                                NotificationMessagingManager.OaddNotificationMessageRelationship(timestamp, senderDn, message, sender, relId, NotificationMessagingManager.getNotifId(), this@InfinityMessagingService)
                            } else {
                                NotificationMessagingManager.addNotificationMessageRelationship(timestamp, senderDn, message, sender, relId, NotificationMessagingManager.getNotifId(), this@InfinityMessagingService)
                            }
                        }

                    })

                } else
                    if (Build.VERSION.SDK_INT >= O)
                        NotificationMessagingManager.OaddNotificationMessageRelationship(timestamp, senderDn, message, sender, relId, NotificationMessagingManager.getNotifId(relId, TYPE.FRIENDS), this)
                    else
                        NotificationMessagingManager.addNotificationMessageRelationship(timestamp, senderDn, message, sender, relId, NotificationMessagingManager.getNotifId(relId, TYPE.FRIENDS), this)

            } else if (remoteMessage.data["messageType"]!!.endsWith("P")) {
                var imageUrl: String = remoteMessage.data["message"]!!
                var messagePath: String = remoteMessage.data["messagePath"]!!
                var messageNumber: String = remoteMessage.data["messageNumber"]!!
                var relId: String = remoteMessage.data["relId"]!!
                var senderDn: String = remoteMessage.data["senderDn"]!!
                var sender: String = remoteMessage.data["sender"]!!
                var timestamp: String = remoteMessage.data["timestamp"]!!

                if (InfinityAPI.signedInUser == null) {
                    return; FirebaseMessaging.getInstance().unsubscribeFromTopic(relId)
                }

                if (sender == InfinityAPI.signedInUser!!.UserID) {
                    return
                }

                InfinityAPI.NotificationsManager.setNewMessageRelationship(relId, messageNumber)
                if (!NotificationMessagingManager.relationships.containsKey(relId)) {
                    // The id of the channel.
                    val id = relId
                    // The user-visible name of the channel.
                    InfinityAPI.getOtherNameRelationship(relId, object : InfinityAPI.InvokeFinishedCallback {
                        override fun responseReceived(response: String?) {
                            val name = "Chat with " + response
                            // The user-visible description of the channel.
                            val description = "A MainApplication Chat with " + InfinityAPI.signedInUser!!.Name + "'s Friend, " + response + ". To Configure Notification's for " + name + ", Open the Chat in the MainApplication App."
                            val importance = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                NotificationManager.IMPORTANCE_HIGH
                            } else {
                                4
                            }
                            val mChannel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                NotificationChannel(id, name, importance)
                            } else {
                                null
                            }
                            // Configure the notification channel.
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                mChannel!!.description = description
                                mChannel.enableLights(true)
                                // Sets the notification light color for notifications posted to this
                                // channel, if the device supports this feature.
                                mChannel.lightColor = Color.RED
                                mChannel.enableVibration(true)
                                mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                                (MainApplication.instance.getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(mChannel)
                                NotificationMessagingManager.OaddNotificationMessageRelationship(timestamp, senderDn, "Photo: 📸", sender, relId, NotificationMessagingManager.getNotifId(), this@InfinityMessagingService)
                            } else {
                                NotificationMessagingManager.addNotificationMessageRelationship(timestamp, senderDn, "Photo: 📸", sender, relId, NotificationMessagingManager.getNotifId(), this@InfinityMessagingService)
                            }
                        }

                    })

                } else
                    if (Build.VERSION.SDK_INT >= O)
                        NotificationMessagingManager.OaddNotificationMessageRelationship(timestamp, senderDn, "Photo: 📸", sender, relId, NotificationMessagingManager.getNotifId(relId, TYPE.FRIENDS), this)
                    else
                        NotificationMessagingManager.addNotificationMessageRelationship(timestamp, senderDn, "Photo: 📸", sender, relId, NotificationMessagingManager.getNotifId(relId, TYPE.FRIENDS), this)
            }
        } else if (remoteMessage.data["type"]!!.contains(NMG)) {
            if (remoteMessage.data["messageType"]!!.endsWith("M")) {
                var message: String = remoteMessage.data["message"]!!
                var messagePath: String = remoteMessage.data["messagePath"]!!
                var messageNumber: String = remoteMessage.data["messageNumber"]!!
                var groupId: String = remoteMessage.data["groupId"]!!
                var senderDn: String = remoteMessage.data["senderDn"]!!
                var groupName: String = remoteMessage.data["groupName"]!!
                var sender: String = remoteMessage.data["sender"]!!
                var timestamp: String = remoteMessage.data["timestamp"]!!

                if (InfinityAPI.signedInUser == null) {
                    return; FirebaseMessaging.getInstance().unsubscribeFromTopic(groupId)
                }

                if (sender == InfinityAPI.signedInUser!!.UserID) {
                    return
                }

                InfinityAPI.NotificationsManager.setNewMessageGroup(groupId, messageNumber)
                if (!NotificationMessagingManager.groups.containsKey(groupId)) {
                    // The id of the channel.
                    val id = groupId
                    // The user-visible name of the channel.
                    val name = groupName
                    // The user-visible description of the channel.
                    val description = "A Infinity Group called $groupName. To Configure Notification's for $groupName, Open the Group in the MainApplication App."
                    val importance = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        NotificationManager.IMPORTANCE_HIGH
                    } else {
                        4
                    }
                    val mChannel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationChannel(id, name, importance)
                    } else {
                        null
                    }
                    // Configure the notification channel.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        mChannel!!.description = description
                        mChannel.enableLights(true)
                        // Sets the notification light color for notifications posted to this
                        // channel, if the device supports this feature.
                        mChannel.lightColor = Color.RED
                        mChannel.enableVibration(true)
                        mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                        (MainApplication.instance.getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(mChannel)
                        NotificationMessagingManager.OaddNotificationMessageGroup(timestamp, senderDn, message, sender, groupId, groupName, NotificationMessagingManager.getNotifId(), this)
                    } else {
                        NotificationMessagingManager.addNotificationMessageGroup(timestamp, senderDn, message, sender, groupId, groupName, NotificationMessagingManager.getNotifId(), this)
                    }
                } else
                    if (Build.VERSION.SDK_INT >= O)
                        NotificationMessagingManager.OaddNotificationMessageGroup(timestamp, senderDn, message, sender, groupId, groupName, NotificationMessagingManager.getNotifId(groupId, TYPE.GROUP), this)
                    else
                        NotificationMessagingManager.addNotificationMessageGroup(timestamp, senderDn, message, sender, groupId, groupName, NotificationMessagingManager.getNotifId(groupId, TYPE.GROUP), this)

            } else if (remoteMessage.data["messageType"]!!.endsWith("P")) {
                var imageUrl: String = remoteMessage.data["message"]!!
                var messagePath: String = remoteMessage.data["messagePath"]!!
                var messageNumber: String = remoteMessage.data["messageNumber"]!!
                var groupId: String = remoteMessage.data["groupId"]!!
                var senderDn: String = remoteMessage.data["senderDn"]!!
                var groupName: String = remoteMessage.data["groupName"]!!
                var sender: String = remoteMessage.data["sender"]!!
                var timestamp: String = remoteMessage.data["timestamp"]!!

                if (InfinityAPI.signedInUser == null) {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(groupId); return
                }

                if (sender == InfinityAPI.signedInUser!!.UserID) {
                    return
                }

                InfinityAPI.NotificationsManager.setNewMessageGroup(groupId, messageNumber)
                InfinityAPI.NotificationsManager.setNewMessageGroup(groupId, messageNumber)
                if (!NotificationMessagingManager.groups.containsKey(groupId)) {
                    // The id of the channel.
                    val id = groupId
                    // The user-visible name of the channel.
                    val name = groupName
                    // The user-visible description of the channel.
                    val description = "A Infinity Group called $groupName. To Configure Notification's for $groupName, Open the Group in the MainApplication App."
                    val importance = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        NotificationManager.IMPORTANCE_HIGH
                    } else {
                        4
                    }
                    val mChannel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationChannel(id, name, importance)
                    } else {
                        null
                    }
                    // Configure the notification channel.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        mChannel!!.description = description
                        mChannel.enableLights(true)
                        // Sets the notification light color for notifications posted to this
                        // channel, if the device supports this feature.
                        mChannel.lightColor = Color.RED
                        mChannel.enableVibration(true)
                        mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                        (MainApplication.instance.getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(mChannel)
                        NotificationMessagingManager.OaddNotificationMessageGroup(timestamp, senderDn, "Photo: 📸", sender, groupId, groupName, NotificationMessagingManager.getNotifId(), this)
                    } else {
                        NotificationMessagingManager.addNotificationMessageGroup(timestamp, senderDn, "Photo: 📸", sender, groupId, groupName, NotificationMessagingManager.getNotifId(), this)
                    }
                } else
                    if (Build.VERSION.SDK_INT >= O)
                        NotificationMessagingManager.OaddNotificationMessageGroup(timestamp, senderDn, "Photo: 📸", sender, groupId, groupName, NotificationMessagingManager.getNotifId(groupId, TYPE.GROUP), this)
                    else
                        NotificationMessagingManager.addNotificationMessageGroup(timestamp, senderDn, "Photo: 📸", sender, groupId, groupName, NotificationMessagingManager.getNotifId(groupId, TYPE.GROUP), this)

            }
        }
        NotificationMessagingManager.changenotifId()
    }


}