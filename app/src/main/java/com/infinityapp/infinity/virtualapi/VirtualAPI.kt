package com.infinityapp.infinity.virtualapi

import android.content.Context
import android.webkit.ValueCallback
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.infinityapp.infinity.MainApplication


/**
 * Created by liammendes on 27/01/2018.
 */

object VirtualAPI {

    fun sendMessageUID(groupId: String, message: String, userId: String, messageType: String, imageUrl: String, offlineImageUrl: String) {
        if (messageType == "M") {
            FirebaseFirestore.getInstance().collection("groups").document(groupId).get().addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    if(!task.result.exists()) {
                        //Error
                        return@addOnCompleteListener
                    }
                            Utilities.getMessagesLengthFromGroupId(groupId, ValueCallback { len ->
                                val data = mapOf(Pair("mGroupId", groupId), Pair("mMessage", message), Pair("mSender", userId), Pair("messageNumber", len + 1), Pair("timestamp", FieldValue.serverTimestamp()), Pair("messageType", messageType))
                                task.result.reference.collection("chats").add(data).addOnCompleteListener { t2 ->
                                    if(t2.isSuccessful){

                                    } else {

                                    }
                                }
                            })
                } else {

                }
            }
        } else if(messageType == "P"){
            FirebaseFirestore.getInstance().collection("groups").document(groupId).get().addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    if(!task.result.exists()) {
                        //Error
                        return@addOnCompleteListener
                    }
                            Utilities.getMessagesLengthFromGroupId(groupId, ValueCallback { len ->
                                val data = mapOf(Pair("mGroupId", groupId), Pair("imageUrl", imageUrl), Pair("mSender", userId), Pair("messageNumber", len + 1), Pair("timestamp", FieldValue.serverTimestamp()), Pair("messageType", messageType))
                                val photo = mapOf(Pair("imageUrl", imageUrl), Pair("messageNumber", len + 1))
                                val pref = MainApplication.Companion.instance.getSharedPreferences("SentMessagePref", Context.MODE_PRIVATE)
                                pref.edit().putString(groupId + "/${len + 1}", offlineImageUrl).apply()
                                task.result.reference.collection("chats").add(data).addOnCompleteListener { t2 ->
                                    if(t2.isSuccessful){
                                        task.result.reference.collection("photos").add(photo)
                                    } else {

                                    }
                                }
                            })
                } else {

                }
            }
        }
    }
    fun RsendMessageUID(relId: String, message: String, userId: String, messageType: String, imageUrl: String, offlineImageUrl: String) {
        if (messageType == "M") {
            FirebaseFirestore.getInstance().collection("relationships").document(relId).get().addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    if(!task.result.exists()) {
                        //Error
                        return@addOnCompleteListener
                    }
                            Utilities.getMessagesLengthFromGroupId(relId, ValueCallback { len ->
                                val data = mapOf(Pair("mRelId", relId), Pair("mMessage", message), Pair("mSender", userId), Pair("messageNumber", len + 1), Pair("timestamp", FieldValue.serverTimestamp()), Pair("messageType", messageType))
                                task.result.reference.collection("chats").add(data).addOnCompleteListener { t2 ->
                                    if(t2.isSuccessful){

                                    } else {

                                    }
                                }
                            })
                } else {

                }
            }
        } else if(messageType == "P"){
            FirebaseFirestore.getInstance().collection("relationships").document(relId).get().addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    if(!task.result.exists()) {
                        //Error
                        return@addOnCompleteListener
                    }
                            Utilities.getMessagesLengthFromRelationshipId(relId, ValueCallback { len ->
                                val data = mapOf(Pair("mRelId", relId), Pair("imageUrl", imageUrl), Pair("mSender", userId), Pair("messageNumber", len + 1), Pair("timestamp", FieldValue.serverTimestamp()), Pair("messageType", messageType))
                                val photo = mapOf(Pair("imageUrl", imageUrl), Pair("messageNumber", len + 1))
                                val pref = MainApplication.Companion.instance.getSharedPreferences("SentMessagePref", Context.MODE_PRIVATE)
                                pref.edit().putString(relId + "/${len + 1}", offlineImageUrl).apply()
                                task.result.reference.collection("chats").add(data).addOnCompleteListener { t2 ->
                                    if(t2.isSuccessful){
                                        task.result.reference.collection("photos").add(photo)
                                    } else {

                                    }
                                }
                            })
                } else {

                }
            }
        }
    }

    object Utilities {

        fun getDisplayName(userId: String, callback: ValueCallback<String?>) {
            FirebaseFirestore.getInstance().collection("users").document(userId).get().addOnCompleteListener { task ->
                if(task.isSuccessful){
                    if(!task.result.exists()){
                        //Error
                        callback.onReceiveValue(null)
                        return@addOnCompleteListener
                    }

                    callback.onReceiveValue(task.result.getString("Name"))

                } else {

                }
            }
        }

        fun getUsersProfilePicture(userId: String, callback: ValueCallback<String?>) {
            FirebaseFirestore.getInstance().collection("users").document(userId).get().addOnCompleteListener { task ->
                if(task.isSuccessful){
                    if(!task.result.exists()){
                        //Error
                        callback.onReceiveValue(null)
                        return@addOnCompleteListener
                    }

                    callback.onReceiveValue(task.result.getString("profileImage"))

                } else {

                }
            }
        }

        fun getMessagesLengthFromGroupId(groupId: String, callback: ValueCallback<Int>) {
            FirebaseFirestore.getInstance().collection("groups").document(groupId).get().addOnCompleteListener { task ->
                if(task.isSuccessful){
                    if(!task.result.exists()){
                        //Error
                        callback.onReceiveValue(-1)
                        return@addOnCompleteListener
                    }

                    task.result.reference.collection("chats").get().addOnCompleteListener {
                        callback.onReceiveValue(it.result.size())
                    }

                } else {

                }
            }
        }

        fun getMessagesLengthFromRelationshipId(relId: String, callback: ValueCallback<Int>) {
            FirebaseFirestore.getInstance().collection("relationships").document(relId).get().addOnCompleteListener { task ->
                if(task.isSuccessful){
                    if(!task.result.exists()){
                        //Error
                        callback.onReceiveValue(-1)
                        return@addOnCompleteListener
                    }

                    task.result.reference.collection("chats").get().addOnCompleteListener {
                        callback.onReceiveValue(it.result.size())
                    }

                } else {

                }
            }
        }

    }

}