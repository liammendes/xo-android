package com.infinityapp.infinity.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.infinityapp.InfinityAPI;
import com.infinityapp.infinity.R;
import com.infinityapp.infinity.dataclasses.Chat;
import com.infinityapp.infinity.dataclasses.RelationshipChat;

import androidx.recyclerview.widget.RecyclerView;

public class SentMessageHolder extends RecyclerView.ViewHolder {

    TextView messageText, timeText;
    ImageView failed, success;
    private Object message;

    public SentMessageHolder(View itemView) {
        super(itemView);

        messageText = itemView.findViewById(R.id.text_message_body);
        timeText = itemView.findViewById(R.id.text_message_time);
    }

    public void setBackground(long colorscheme) {
        messageText.setBackgroundResource(InfinityAPI.Utils.colorSchemeToResource(colorscheme, true));
        if (colorscheme == 1)
            messageText.setTextColor(itemView.getResources().getColor(R.color.md_black_1000));
    }

    public void bind(Chat message) {
        messageText.setText(message.getmMessage());

        this.message = message;

        String minutes = message.getTimestamp().getMinutes() + "";
        if (minutes.length() == 1) {
            minutes = "0" + minutes;
        }

        String hours = message.getTimestamp().getHours() + "";
        if (hours.length() == 1) {
            hours = "0" + hours;
        }

        timeText.setText(hours + ":" + minutes);
    }

    public void bind(RelationshipChat message) {
        messageText.setText(message.getmMessage());

        this.message = message;

        String minutes = message.getTimestamp().getMinutes() + "";
        if (minutes.length() == 1) {
            minutes = "0" + minutes;
        }

        String hours = message.getTimestamp().getHours() + "";
        if (hours.length() == 1) {
            hours = "0" + hours;
        }

        timeText.setText(hours + ":" + minutes);
    }
}