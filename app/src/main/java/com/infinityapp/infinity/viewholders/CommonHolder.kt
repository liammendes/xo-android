package com.infinityapp.infinity.viewholders

import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.infinityapp.infinity.R
import com.infinityapp.infinity.views.ListOptionView
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by liammendes on 03/01/2018.
 */

class CommonHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

    var lov: ListOptionView = ListOptionView(itemView!!.context, itemView)
    fun setOnClickListener(function: View.OnClickListener) {
        lov.hView!!.findViewById<LinearLayout>(R.id.lovvi_root).setOnClickListener(function)
    }

    fun setNewMessage(b: Boolean) {
        if (b) lov.hView!!.findViewById<CircleImageView>(R.id.new_message).visibility = View.VISIBLE
    }

}