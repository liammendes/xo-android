package com.infinityapp.infinity.viewholders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.infinityapp.infinity.MainApplication;
import com.infinityapp.infinity.R;
import com.infinityapp.infinity.activity.PhotoListActivity;
import com.infinityapp.infinity.dataclasses.Chat;
import com.infinityapp.infinity.dataclasses.RelationshipChat;

import java.util.NoSuchElementException;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class SentPhotoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView timeText;
    ImageView image_content, failed, success;
    private Object message;

    public SentPhotoHolder(View itemView) {
        super(itemView);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) itemView.getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);

        image_content = itemView.findViewById(R.id.image_body);
        timeText = itemView.findViewById(R.id.text_message_time);

        //itemView.setOnLongClickListener(this);
        //image_content.setOnLongClickListener(this);
        //timeText.setOnLongClickListener(this);

        image_content.setOnClickListener(this);

        image_content.setMaxHeight(displayMetrics.heightPixels / 2);
        image_content.setMaxWidth(displayMetrics.widthPixels / 2);
    }

    private Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            if (((BitmapDrawable) drawable).getBitmap() != null) {
                return ((BitmapDrawable) drawable).getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public void scaleImage(ImageView view) throws NoSuchElementException {
        // Get bitmap from the the ImageView.
        Bitmap bitmap = null;

        try {
            Drawable drawing = view.getDrawable();
            bitmap = ((BitmapDrawable) drawing).getBitmap();
        } catch (NullPointerException e) {
            return;
        } catch (ClassCastException e) {
            // Check bitmap is Ion drawable
            bitmap = drawableToBitmap(view.getDrawable());
        } catch (NoSuchElementException e) {
            Log.e(MainApplication.instance.createGlobalTag("SentPhotoHolder"), "The ImageView " + view + "contains no drawable, continuing");
            return;
        }

        // Get current dimensions AND the desired bounding box
        int width = 0;

        try {
            width = bitmap.getWidth();
        } catch (NullPointerException e) {
            return;
        }

        int height = bitmap.getHeight();
        int bounding = dpToPx(250);
        Log.i("Test", "original width = " + Integer.toString(width));
        Log.i("Test", "original height = " + Integer.toString(height));
        Log.i("Test", "bounding = " + Integer.toString(bounding));

        // Determine how much to scale: the dimension requiring less scaling is
        // closer to the its side. This way the image always stays inside your
        // bounding box AND either x/y axis touches it.
        float xScale = ((float) bounding) / width;
        float yScale = ((float) bounding) / height;
        float scale = (xScale <= yScale) ? xScale : yScale;
        Log.i("Test", "xScale = " + Float.toString(xScale));
        Log.i("Test", "yScale = " + Float.toString(yScale));
        Log.i("Test", "scale = " + Float.toString(scale));

        // Create a matrix for the scaling and add the scaling data
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        // Create a new bitmap and convert it to a format understood by the ImageView
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        width = scaledBitmap.getWidth(); // re-use
        height = scaledBitmap.getHeight(); // re-use
        BitmapDrawable result = new BitmapDrawable(scaledBitmap);
        Log.i("Test", "scaled width = " + Integer.toString(width));
        Log.i("Test", "scaled height = " + Integer.toString(height));

        // Apply the scaled bitmap
        view.setImageDrawable(result);

        // Now change ImageView's dimensions to match the scaled image
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);

        Log.i("Test", "done");
    }

    private int dpToPx(int dp) {
        float density = this.itemView.getContext().getApplicationContext().getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    public void bind(Chat message) {
        if (((Activity) image_content.getContext()).isDestroyed()) return;
        Glide.with(itemView).load(message.getimageUrl()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                Glide.with(itemView).load(MainApplication.instance.getSharedPreferences("SentMessagePref", Context.MODE_PRIVATE)).into(image_content);
                scaleImage(image_content);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(image_content);
        scaleImage(image_content);

        this.message = message;

        String minutes = message.getTimestamp().getMinutes() + "";
        if (minutes.length() == 1) {
            minutes = "0" + minutes;
        }

        String hours = message.getTimestamp().getHours() + "";
        if (hours.length() == 1) {
            hours = "0" + hours;
        }

        timeText.setText(hours + ":" + minutes);
    }

    public void bind(RelationshipChat message) {
        Glide.with(itemView).load(message.getimageUrl()).into(image_content);
        scaleImage(image_content);

        this.message = message;

        String minutes = message.getTimestamp().getMinutes() + "";
        if (minutes.length() == 1) {
            minutes = "0" + minutes;
        }

        String hours = message.getTimestamp().getHours() + "";
        if (hours.length() == 1) {
            hours = "0" + hours;
        }

        timeText.setText(hours + ":" + minutes);
    }

    @Override
    public void onClick(View v) {
        Intent photoIntent = new Intent(MainApplication.instance, PhotoListActivity.class);
        photoIntent.putExtra("messageId", ((Chat)message).getmessageNumber());
        MainApplication.instance.startActivity(photoIntent);
    }
}