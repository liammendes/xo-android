package com.infinityapp.infinity.viewholders;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.infinityapp.infinity.MainApplication;
import com.infinityapp.infinity.R;
import com.infinityapp.infinity.activity.PhotoListActivity;
import com.infinityapp.infinity.dataclasses.Chat;
import com.infinityapp.infinity.dataclasses.RelationshipChat;
import com.infinityapp.infinity.virtualapi.VirtualAPI;

import java.util.NoSuchElementException;

import androidx.recyclerview.widget.RecyclerView;

public class ReceivedPhotoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView timeText, nameText;

    public ImageView profileImage, image_content;
    private Chat message;

    public ReceivedPhotoHolder(View itemView) {
        super(itemView);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) itemView.getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);

        image_content = itemView.findViewById(R.id.image_body);
        timeText = itemView.findViewById(R.id.text_message_time);
        nameText = itemView.findViewById(R.id.text_message_name);
        profileImage = itemView.findViewById(R.id.image_message_profile);

        image_content.setOnClickListener(this);

        image_content.setMaxHeight(displayMetrics.heightPixels / 2);
        image_content.setMaxWidth(displayMetrics.widthPixels / 2);

    }

    private Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            if (((BitmapDrawable) drawable).getBitmap() != null) {
                return ((BitmapDrawable) drawable).getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public void scaleImage(ImageView view) throws NoSuchElementException {
        // Get bitmap from the the ImageView.
        Bitmap bitmap = null;

        try {
            Drawable drawing = view.getDrawable();
            bitmap = ((BitmapDrawable) drawing).getBitmap();
        } catch (NullPointerException e) {
            throw new NoSuchElementException("No drawable on given view");
        } catch (ClassCastException e) {
            // Check bitmap is Ion drawable
            bitmap = drawableToBitmap(view.getDrawable());
        }

        // Get current dimensions AND the desired bounding box
        int width = 0;

        try {
            width = bitmap.getWidth();
        } catch (NullPointerException e) {
            throw new NoSuchElementException("Can't find bitmap on given view/drawable");
        }

        int height = bitmap.getHeight();
        int bounding = dpToPx(250);
        Log.i("Test", "original width = " + Integer.toString(width));
        Log.i("Test", "original height = " + Integer.toString(height));
        Log.i("Test", "bounding = " + Integer.toString(bounding));

        // Determine how much to scale: the dimension requiring less scaling is
        // closer to the its side. This way the image always stays inside your
        // bounding box AND either x/y axis touches it.
        float xScale = ((float) bounding) / width;
        float yScale = ((float) bounding) / height;
        float scale = (xScale <= yScale) ? xScale : yScale;
        Log.i("Test", "xScale = " + Float.toString(xScale));
        Log.i("Test", "yScale = " + Float.toString(yScale));
        Log.i("Test", "scale = " + Float.toString(scale));

        // Create a matrix for the scaling and add the scaling data
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        // Create a new bitmap and convert it to a format understood by the ImageView
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        width = scaledBitmap.getWidth(); // re-use
        height = scaledBitmap.getHeight(); // re-use
        BitmapDrawable result = new BitmapDrawable(scaledBitmap);
        Log.i("Test", "scaled width = " + Integer.toString(width));
        Log.i("Test", "scaled height = " + Integer.toString(height));

        // Apply the scaled bitmap
        view.setImageDrawable(result);

        // Now change ImageView's dimensions to match the scaled image
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);

        Log.i("Test", "done");
    }

    private int dpToPx(int dp) {
        float density = this.itemView.getContext().getApplicationContext().getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    public void bind(Chat message) {
        Glide.with(itemView).load(message.getimageUrl()).into(image_content);

        String minutes = message.getTimestamp().getMinutes() + "";
        if (minutes.length() == 1) {
            minutes = "0" + minutes;
        }

        String hours = message.getTimestamp().getHours() + "";
        if (hours.length() == 1) {
            hours = "0" + hours;
        }

        this.message = message;

// Format the stored timestamp into a readable String using method.
        timeText.setText(hours + ":" + minutes);

        VirtualAPI.Utilities.INSTANCE.getDisplayName(message.getmSender(), new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                nameText.setText(value);
            }
        });

        VirtualAPI.Utilities.INSTANCE.getUsersProfilePicture(message.getmSender(), new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                if (((Activity) profileImage.getContext()).isDestroyed()) return;
                Glide.with(itemView).load(value).into(profileImage);
            }
        });
    }

    public void bind(RelationshipChat message) {
        Glide.with(itemView).load(message.getimageUrl()).into(image_content);

        String minutes = message.getTimestamp().getMinutes() + "";
        if (minutes.length() == 1) {
            minutes = "0" + minutes;
        }

        String hours = message.getTimestamp().getHours() + "";
        if (hours.length() == 1) {
            hours = "0" + hours;
        }

        timeText.setText(hours + ":" + minutes);

        VirtualAPI.Utilities.INSTANCE.getDisplayName(message.getmSender(), new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                nameText.setText(value);
            }
        });

        VirtualAPI.Utilities.INSTANCE.getUsersProfilePicture(message.getmSender(), new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                if (((Activity) profileImage.getContext()).isDestroyed()) return;
                Glide.with(itemView).load(value).into(profileImage);
            }
        });
    }

    @Override
    public void onClick(View v) {
        Intent photoIntent = new Intent(MainApplication.instance, PhotoListActivity.class);
        photoIntent.putExtra("messageId", (message).getmessageNumber());
        MainApplication.instance.startActivity(photoIntent);
    }
}