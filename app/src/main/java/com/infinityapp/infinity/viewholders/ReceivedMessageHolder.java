package com.infinityapp.infinity.viewholders;

import android.app.Activity;
import android.view.View;
import android.webkit.ValueCallback;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.infinityapp.InfinityAPI;
import com.infinityapp.infinity.R;
import com.infinityapp.infinity.dataclasses.Chat;
import com.infinityapp.infinity.dataclasses.RelationshipChat;
import com.infinityapp.infinity.virtualapi.VirtualAPI;

import androidx.recyclerview.widget.RecyclerView;

public class ReceivedMessageHolder extends RecyclerView.ViewHolder {

    public TextView messageText, timeText, nameText;

    public ImageView profileImage;

    public ReceivedMessageHolder(View itemView) {
        super(itemView);

        messageText = itemView.findViewById(R.id.text_message_body);
        timeText = itemView.findViewById(R.id.text_message_time);
        nameText = itemView.findViewById(R.id.text_message_name);
        profileImage = itemView.findViewById(R.id.image_message_profile);
    }

    public void setBackground(long colorscheme) {
        messageText.setBackgroundResource(InfinityAPI.Utils.colorSchemeToResource(colorscheme, false));
        if (colorscheme == 1)
            messageText.setTextColor(itemView.getResources().getColor(R.color.md_black_1000));
    }

    public void bind(Chat message) {
        messageText.setText(message.getmMessage());

        String minutes = message.getTimestamp().getMinutes() + "";
        if (minutes.length() == 1) {
            minutes = "0" + minutes;
        }

        String hours = message.getTimestamp().getHours() + "";
        if (hours.length() == 1) {
            hours = "0" + hours;
        }

        // Format the stored timestamp into a readable String using method.
        timeText.setText(hours + ":" + minutes);

        VirtualAPI.Utilities.INSTANCE.getDisplayName(message.getmSender(), new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                nameText.setText(value);
            }
        });

        VirtualAPI.Utilities.INSTANCE.getUsersProfilePicture(message.getmSender(), new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                if (((Activity) profileImage.getContext()).isDestroyed()) return;
                Glide.with(itemView).load(value).into(profileImage);
            }
        });
    }

    public void bind(RelationshipChat message) {
        messageText.setText(message.getmMessage());

        String minutes = message.getTimestamp().getMinutes() + "";
        if (minutes.length() == 1) {
            minutes = "0" + minutes;
        }

        String hours = message.getTimestamp().getHours() + "";
        if (hours.length() == 1) {
            hours = "0" + hours;
        }

        // Format the stored timestamp into a readable String using method.
        timeText.setText(hours + ":" + minutes);

        VirtualAPI.Utilities.INSTANCE.getDisplayName(message.getmSender(), new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                nameText.setText(value);
            }
        });

        VirtualAPI.Utilities.INSTANCE.getUsersProfilePicture(message.getmSender(), new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                if (((Activity) profileImage.getContext()).isDestroyed()) return;
                Glide.with(itemView).load(value).into(profileImage);
            }
        });
    }
}