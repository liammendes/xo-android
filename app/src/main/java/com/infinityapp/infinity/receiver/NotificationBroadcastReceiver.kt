package com.infinityapp.infinity.receiver

import android.app.Notification
import android.app.NotificationManager
import android.app.RemoteInput
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.InfinityMessagingService
import com.infinityapp.infinity.R
import com.infinityapp.infinity.virtualapi.VirtualAPI

class NotificationBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        if (intent.getBooleanExtra("isNotificationReplyRelationships", false)) {

            var notifId = intent.getIntExtra("notifId", 0)

            VirtualAPI.RsendMessageUID(InfinityMessagingService.NotificationMessagingManager.conversationData[notifId]!!.id, getMessageText(intent).toString(), InfinityAPI.signedInUser!!.UserID, "M", "", "")

                    var repliedNotification = Notification.Builder(context)
                            .setSmallIcon(R.drawable.ic_infinity)
                            .setContentText("You ― " + getMessageText(intent))
                            .build()

                    // Issue the new notification.
                    var notificationManager =
                            (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)

                    notificationManager.notify(notifId, repliedNotification)

        } else if (intent.getBooleanExtra("isNotificationReplyGroups", false)) {

            var notifId = intent.getIntExtra("notifId", 0)

            VirtualAPI.RsendMessageUID(InfinityMessagingService.NotificationMessagingManager.conversationData[notifId]!!.id, getMessageText(intent).toString(), InfinityAPI.signedInUser!!.UserID, "M", "", "")

                    var repliedNotification = Notification.Builder(context)
                            .setSmallIcon(R.drawable.ic_infinity)
                            .setContentText("You ― " + getMessageText(intent))
                            .build()

                    // Issue the new notification.
                    var notificationManager =
                            (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)

                    notificationManager.notify(notifId, repliedNotification)

        }

    }

    private fun getMessageText(intent: Intent): CharSequence? {
        val remoteInput = RemoteInput.getResultsFromIntent(intent)
        return remoteInput?.getCharSequence("key_text_reply")
    }

}
