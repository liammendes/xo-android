package com.infinityapp.infinity.callback

/**
 * Created by liammendes on 02/11/2017.
 */

interface SetupCallback {
    fun setupSuccessful()
    fun setupUnsuccessful()
    fun setupSuccessfulWithError()
}