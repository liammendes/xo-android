package com.infinityapp.infinity.callback

/**
 * Created by liammendes on 03/11/2017.
 */

interface NewMessageListener {
    fun newMessage(messageNumber: String)
    fun readMessages(relId: String)

}