package com.infinityapp.infinity.utils;

import static com.infinityapp.infinity.utils.NativeManager.InfinityLibraries.INFINITY_NATIVE;

/**
 * Created by liammendes on 13/01/2018.
 */

public class NativeManager {

    static {
        System.loadLibrary(INFINITY_NATIVE);
    }

    static class InfinityLibraries {
        static String INFINITY_NATIVE = "infinity-lib";
    }

}
