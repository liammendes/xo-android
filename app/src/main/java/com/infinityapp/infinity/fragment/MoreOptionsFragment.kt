package com.infinityapp.infinity.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import com.infinityapp.infinity.MainApplication
import com.infinityapp.infinity.R
import com.infinityapp.infinity.views.ListOptionView

class MoreOptionsFragment : Fragment() {

    private var mListener: MOListener? = null
    private var rview: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rview = inflater.inflate(R.layout.activity_other, RelativeLayout(context))
        return rview
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (rview!!.findViewById<Button>(R.id.mo_cancel)).setOnClickListener { Log.e(MainApplication.instance.createGlobalTag("MoreOptionsFragment"), "mo_cancel was Clicked!");mListener!!.more_options_back() }
        (rview!!.findViewById<ListOptionView>(R.id.google_button)).setOnClickListener { Log.e(MainApplication.instance.createGlobalTag("MoreOptionsFragment"), "google_button was Clicked!");mListener!!.more_options_google() }
        (rview!!.findViewById<ListOptionView>(R.id.twitter_button)).setOnClickListener { Log.e(MainApplication.instance.createGlobalTag("MoreOptionsFragment"), "twitter_button was Clicked!");mListener!!.more_options_twitter() }
        (rview!!.findViewById<ListOptionView>(R.id.signup_button)).setOnClickListener { Log.e(MainApplication.instance.createGlobalTag("MoreOptionsFragment"), "signup_button was Clicked!");mListener!!.more_options_sign_up() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mListener!!.more_options_onCreate()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MOListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface MOListener {

        fun more_options_google()
        fun more_options_twitter()
        fun more_options_sign_up()
        fun more_options_back()
        fun more_options_onCreate()
    }

    data class SignInData(val email: String, val password: String?)
}
