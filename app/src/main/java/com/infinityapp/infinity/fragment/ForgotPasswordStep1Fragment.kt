package com.infinityapp.infinity.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.infinityapp.infinity.R

class ForgotPasswordStep1Fragment : Fragment() {

    private var mListener: FPS1Listener? = null
    private var signInData: SignInData? = SignInData("", null)

    private var v: View? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_forgot_password_step_1, null)
        this.v = view
        view.findViewById<Button>(R.id.fps1_cancel).setOnClickListener { mListener!!.fps1_cancel() }
        view.findViewById<Button>(R.id.fps1_next).setOnClickListener { signInData = SignInData(v!!.findViewById<EditText>(R.id.fps1_email).text.toString(), null);mListener!!.fps1_next(signInData!!) }
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FPS1Listener) {
            mListener = context
            mListener!!.fps1_onCreate()
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    fun setSignInData(signInData: SignInData){
        this.signInData = signInData
        v!!.findViewById<EditText>(R.id.fps1_email).text.clear()
        v!!.findViewById<EditText>(R.id.fps1_email).text.append(signInData.email)
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface FPS1Listener {
        fun fps1_next(signInData: SignInData)
        fun fps1_cancel()
        fun fps1_onCreate()
    }

    data class SignInData(val email: String, val password: String?)

    /**fun setLoading(b: Boolean) {
    v!!.findViewById<EditText>(R.id.fps1_root).isEnabled = !b
        if(b){
    v!!.findViewById<EditText>(R.id.fps1_root).alpha = 0.5f
    v!!.findViewById<EditText>(R.id.fps1_error_txt).visibility = View.GONE
        } else {
    v!!.findViewById<EditText>(R.id.fps1_root).alpha = 1f
        }
    }**/

    fun error(localizedMessage: String?) {
        /**v!!.findViewById<EditText>(R.id.fps1_error_txt).visibility = View.VISIBLE
        v!!.findViewById<EditText>(R.id.fps1_error_txt).text = SpannableStringBuilder(localizedMessage!!)**/
    }
}
