package com.infinityapp.infinity.fragment

//import com.google.firebase.storage.FirebaseStorage
import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.AnyRes
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.github.javiersantos.bottomdialogs.BottomDialog
import com.google.firebase.auth.FirebaseAuth
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.R
import com.infinityapp.infinity.activity.AuthenticationActivity
import com.infinityapp.infinity.activity.FinalSetupActivityActivity
import com.infinityapp.infinity.activity.IFSAAInterface
import com.infinityapp.infinity.dataclasses.SignUpDataGoogle
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [BlankFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [BlankFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SetupInfoFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    var chosen = false
    private var pubv: View? = null
    var selectedImage: Uri? = FirebaseAuth.getInstance().currentUser!!.photoUrl
    private var mListener: OnSignUpListener? = null

    var ppurl: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 42) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    chosen = true
                    selectedImage = data.data
                    loadPPiPPS(selectedImage)
                }
            }
        }
    }

    fun getUriToDrawable(@NonNull context: Context,
                         @AnyRes drawableId: Int): Uri {
        var imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + context.resources.getResourcePackageName(drawableId)
                + '/' + context.resources.getResourceTypeName(drawableId)
                + '/' + context.resources.getResourceEntryName(drawableId))
        return imageUri
    }

    fun isLeapYear(year: Int): Boolean {
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, year)
        return cal.getActualMaximum(Calendar.DAY_OF_YEAR) > 365
    }

    private fun loadPPiPPS() {
        //val storage = FirebaseRemoteConfig.getInstance()
        // Create a storage reference from our app
        //val storageRef = storage.reference

        if(selectedImage == null){
            selectedImage = Uri.parse("android.resource://com.infinityapp.infinity/mipmap/ic_launcher_round")
            chosen = true
        }
        else
            Glide.with(context!!).load(FirebaseAuth.getInstance().currentUser!!.photoUrl).into(v!!.findViewById(R.id.profile_picture_setup))
    }

    private var v: View? = null

    private fun loadPPiPPS(uri: Uri?) {
        if (uri == null) {
            loadPPiPPS()
        } else Glide.with(context!!).load(uri).into(v!!.findViewById(R.id.profile_picture_setup))
    }

    private var canSignUp: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var v = inflater.inflate(R.layout.fragment_setup_info, null)
        this.v = v

        val next = View.OnClickListener { signUpClicked() }
        val cancel = View.OnClickListener {
            FirebaseAuth.getInstance().currentUser!!.delete(); FirebaseAuth.getInstance().signOut(); startActivity(Intent(context, AuthenticationActivity::class.java)); activity!!.finish();(mListener as FinalSetupActivityActivity).onBackPressed()
        }
        val parent = activity as IFSAAInterface
        parent.setBOCL(next, cancel)

        selectedImage = FirebaseAuth.getInstance().currentUser!!.photoUrl
        loadPPiPPS(selectedImage)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
        (v.findViewById<ImageView>(R.id.profile_picture_setup)).setOnClickListener {
            var READ_REQUEST_CODE = 42

            // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
            // browser.
            var int = Intent(Intent.ACTION_OPEN_DOCUMENT)

            // Filter to only show results that can be "opened", such as a
            // file (as opposed to a list of contacts or timezones)
            int.addCategory(Intent.CATEGORY_OPENABLE)

            // Filter to show only images, using the image MIME data type.
            // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
            // To search for all documents available via installed storage providers,
            // it would be "*/*".
            int.type = "image/*"

            startActivityForResult(int, READ_REQUEST_CODE)
        }
        (v.findViewById<TextView>(R.id.user_profile_displayName_2)).text = FirebaseAuth.getInstance().currentUser?.displayName
        (v.findViewById<TextView>(R.id.user_profile_email_2)).text = FirebaseAuth.getInstance().currentUser?.email
        if (FirebaseAuth.getInstance().currentUser!!.email == null) (v.findViewById<TextView>(R.id.user_profile_email_2)).text = "<no email provided>"

        (v.findViewById<EditText>(R.id.fs_email)).setText(FirebaseAuth.getInstance().currentUser?.email)
        (v.findViewById<EditText>(R.id.fs_email)).addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                v.findViewById<TextView>(R.id.user_profile_email_2).text = p0

            }

        })

        (v.findViewById<EditText>(R.id.fs_displayname)).setText(FirebaseAuth.getInstance().currentUser?.displayName)
        (v.findViewById<EditText>(R.id.fs_displayname)).addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                (v.findViewById<TextView>(R.id.user_profile_displayName_2)).text = p0
            }

        })
        pubv = v
        return v
    }

    fun parseDate(date: String): Date? {
        try {
            return SimpleDateFormat("yyyy-MM-dd").parse(date)
        } catch (e: ParseException) {
            return null
        }

    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return if (target == null) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    fun signUpClicked() {
        if (!canSignUp) return
        var dn = (v!!.findViewById<EditText>(R.id.fs_displayname)).text.toString()
        var em = (v!!.findViewById<EditText>(R.id.fs_email)).text.toString()
        var un = (v!!.findViewById<EditText>(R.id.fs_username)).text.toString()
        var sid: SignUpDataGoogle? = null
        try {
            if (chosen) {
                sid = SignUpDataGoogle(dn, em, un, Date(), false, selectedImage, FirebaseAuth.getInstance().currentUser!!.uid)
                sid.useFirebaseAuthPP = false
            } else {
                sid = SignUpDataGoogle(dn, em, un, Date(), false, selectedImage, FirebaseAuth.getInstance().currentUser!!.uid)
                sid.useFirebaseAuthPP = true
            }
            mListener!!.onSignUp(sid)
        } catch (e: NullPointerException) {
            if (selectedImage == null) {
                BottomDialog.Builder(context!!).setTitle("Profile Picture Not Found").setContent("We could not find and prepare your selected profile image for preparation and uploading to the MainApplication Servers. Would you like to use your Google Account Profile Picture through out the app instead, You will be able to change your Profile Picture later.").setPositiveText("Use Google Picture").setNegativeText("Go Back").onPositive { sid = SignUpDataGoogle(dn, em, un, Date(), false, null, FirebaseAuth.getInstance().currentUser!!.uid); sid!!.useFirebaseAuthPP = true; mListener!!.onSignUp(sid!!) }.onNegative { bottomDialog -> bottomDialog.dismiss() }.build().show()
            }
        }
    }

    private fun getYearOffset(): Int {
        return when {
            Calendar.getInstance().get(Calendar.YEAR) == 2017 -> 0
            Calendar.getInstance().get(Calendar.YEAR) == 2018 -> 1
            Calendar.getInstance().get(Calendar.YEAR) == 2019 -> 2
            Calendar.getInstance().get(Calendar.YEAR) == 2020 -> 3
            else -> 0
        }
    }

    override fun onStart() {
        super.onStart()
        v!!.findViewById<EditText>(R.id.fs_email).addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var valid = isValidEmail(s)
                if (!valid) {
                    v!!.findViewById<EditText>(R.id.fs_email).error = "Email is not valid"
                    canSignUp = false
                } else {
                    v!!.findViewById<EditText>(R.id.fs_email).error = null
                    canSignUp = true
                }
            }
        })
        v!!.findViewById<EditText>(R.id.fs_displayname).addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.length <= 1) {
                    v!!.findViewById<EditText>(R.id.fs_displayname).error = "Display Name is not valid"
                    canSignUp = false
                } else {
                    v!!.findViewById<EditText>(R.id.fs_email).error = null
                    canSignUp = true
                }
            }
        })
        v!!.findViewById<EditText>(R.id.fs_username).addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                canSignUp = false
                v!!.findViewById<TextView>(R.id.user_status).text = "Checking if @$s exists"
                v!!.findViewById<TextView>(R.id.user_status).setTextColor(resources.getColor(R.color.md_blue_500))
                InfinityAPI.makeCustomCall("checkUsernameExists", Pair("username", s.toString()))!!.addOnCompleteListener { result ->
                    if (result.isSuccessful) {
                        if ((result.result.data as HashMap<*, *>)["exists"] as Boolean) {
                            //User exists
                            canSignUp = false
                            v!!.findViewById<TextView>(R.id.user_status).text = "@$s exists and is unavailable"
                            v!!.findViewById<TextView>(R.id.user_status).setTextColor(resources.getColor(R.color.md_red_500))
                        } else {
                            //User does not exist
                            canSignUp = true
                            v!!.findViewById<TextView>(R.id.user_status).text = "The Username is available"
                            v!!.findViewById<TextView>(R.id.user_status).setTextColor(resources.getColor(R.color.shamrock_green))
                        }
                    } else {
                        canSignUp = false
                        v!!.findViewById<TextView>(R.id.user_status).text = "An error occurred. Type another name and try again"
                        v!!.findViewById<TextView>(R.id.user_status).setTextColor(resources.getColor(R.color.md_red_500))
                    }
                }
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnSignUpListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnSignUpListener {
        // TODO: Update argument type and name
        fun onSignUp(sid: SignUpDataGoogle)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BlankFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String): SetupInfoFragment {
            return SetupInfoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
        }
    }
}