package com.infinityapp.infinity.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.infinityapp.infinity.R

class NewEmailAccountFragment : Fragment() {

    private var mListener: NEAListener? = null
    private var rview: View? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rview = inflater.inflate(R.layout.activity_new_email_account, RelativeLayout(context))
        rview!!.findViewById<Button>(R.id.cancel_action).setOnClickListener { mListener!!.new_email_account_cancel() }
        rview!!.findViewById<Button>(R.id.next_action).setOnClickListener {
            if (rview!!.findViewById<EditText>(R.id.til_email).text.toString().isEmpty() || rview!!.findViewById<EditText>(R.id.til_email).text.toString().isEmpty())
                Toast.makeText(mListener as Context, "All fields must be filled out to Sign Up to Infinity!", LENGTH_LONG).show()
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(rview!!.findViewById<EditText>(R.id.til_email).text.toString(), rview!!.findViewById<EditText>(R.id.til_email).text.toString()).addOnCompleteListener { task ->
                if(task.isSuccessful){
                    mListener!!.new_email_account_create_account()
                } else {
                    mListener!!.new_email_account_cancel()
                }
            }
        }
        return rview
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mListener!!.new_email_account_onCreate()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is NEAListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface NEAListener {

        fun new_email_account_create_account()
        fun new_email_account_cancel()
        fun new_email_account_onCreate()
    }

    data class SignInData(val email: String, val password: String?)
}
