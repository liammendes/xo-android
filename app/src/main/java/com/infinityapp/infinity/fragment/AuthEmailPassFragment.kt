package com.infinityapp.infinity.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.infinityapp.infinity.R
import com.infinityapp.infinity.activity.AuthenticationActivity
import com.infinityapp.infinity.activity.IntroActivity

class AuthEmailPassFragment : Fragment() {

    private var mListener: OFIListener? = null


    private var v: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_authsetup, null)
        this.v = view
        view.findViewById<Button>(R.id.cancel_action).setOnClickListener{mListener!!.auth_email_pass_cancel(); (mListener as AuthenticationActivity).finish(); startActivity(Intent(context, IntroActivity::class.java))}
        view.findViewById<Button>(R.id.next_action).setOnClickListener { mListener!!.auth_email_pass_next(SignInData(view.findViewById<EditText>(R.id.til_email).text.toString(), view.findViewById<EditText>(R.id.til_password).text.toString())) }
        view.findViewById<TextView>(R.id.auth_more_options).setOnClickListener{mListener!!.auth_email_pass_more_options()}
        view.findViewById<TextView>(R.id.auth_forgot_password).setOnClickListener { mListener!!.auth_email_pass_forgot_password(SignInData(view.findViewById<EditText>(R.id.til_email).text.toString(), null)) }
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OFIListener) {
            mListener = context
            mListener!!.auth_email_pass_onCreate()
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OFIListener {

        fun auth_email_pass_next(signInData: SignInData)
        fun auth_email_pass_cancel()
        fun auth_email_pass_more_options()
        fun auth_email_pass_forgot_password(signInData: SignInData)
        fun auth_email_pass_onCreate()
    }

    data class SignInData(val email: String, val password: String?)

    fun setLoading(b: Boolean) {
        v!!.findViewById<ImageView>(R.id.ic_auth).isEnabled = !b
        v!!.findViewById<TextView>(R.id.auth_headline).isEnabled = !b
        v!!.findViewById<ScrollView>(R.id.auth_sv).isEnabled = !b
        v!!.findViewById<Button>(R.id.cancel_action).isEnabled = !b
        v!!.findViewById<Button>(R.id.next_action).isEnabled = !b
        if(b){
            v!!.findViewById<ProgressBar>(R.id.signing_in_status).visibility = View.VISIBLE

            v!!.findViewById<ImageView>(R.id.ic_auth).alpha = 0.5f
            v!!.findViewById<TextView>(R.id.auth_headline).alpha = 0.5f
            v!!.findViewById<ScrollView>(R.id.auth_sv).alpha = 0.5f
            v!!.findViewById<Button>(R.id.cancel_action).alpha = 0.5f
            v!!.findViewById<Button>(R.id.next_action).alpha = 0.5f
            v!!.findViewById<TextView>(R.id.error_txt).visibility = View.GONE
        } else {
            v!!.findViewById<ProgressBar>(R.id.signing_in_status).visibility = View.GONE
            v!!.findViewById<ImageView>(R.id.ic_auth).alpha = 1f
            v!!.findViewById<TextView>(R.id.auth_headline).alpha = 1f
            v!!.findViewById<ScrollView>(R.id.auth_sv).alpha = 1f
            v!!.findViewById<Button>(R.id.cancel_action).alpha = 1f
            v!!.findViewById<Button>(R.id.next_action).alpha = 1f
        }
    }

    fun error(localizedMessage: String?) {
        v!!.findViewById<TextView>(R.id.error_txt).visibility = View.VISIBLE
        v!!.findViewById<TextView>(R.id.error_txt).text = localizedMessage!!
    }
}
