package com.infinityapp.infinity.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.infinityapp.infinity.R

class ForgotPasswordStep2Fragment : Fragment() {

    private var mListener: FPS2Listener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_forgot_password_step_2, null)
        view.findViewById<Button>(R.id.fps2_done).setOnClickListener { mListener!!.fps2_done() }
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FPS2Listener) {
            mListener = context
            mListener!!.fps2_onCreate()
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface FPS2Listener {
        fun fps2_done()
        fun fps2_onCreate()
    }
}
