package com.infinityapp.infinity.fragment

//TODO ADD OFFLINE AND ONLINE SEAMLESS SWITCHING
/**
 * There is no network activity after switching the network off and on again.
 * This means that if we have chat, we need it to call goOnline automatically
 * in the background or through a service in our app.
 */

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.github.javiersantos.bottomdialogs.BottomDialog
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.infinityapp.InfinityAPI
import com.infinityapp.infinity.BuildConfig
import com.infinityapp.infinity.MainApplication
import com.infinityapp.infinity.R
import com.infinityapp.infinity.activity.*
import com.infinityapp.infinity.dataclasses.CommonRefer
import com.infinityapp.infinity.dataclasses.Group
import com.infinityapp.infinity.dataclasses.Relationship
import com.infinityapp.infinity.viewholders.CommonHolder
import com.infinityapp.infinity.views.CurrentProfileLoadingView
import de.hdodenhof.circleimageview.CircleImageView
import java.io.Serializable

class MainFragment : Fragment() {

    private var mListener: MainActivity? = null
    private var inflatedView: View? = null
    private var commonAdapter: FirestoreRecyclerAdapter<CommonRefer, CommonHolder>? = null
    lateinit var mAdView: AdView

    @SuppressLint("InflateParams")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_main, null)
        //onCreateOptionsMenu()
        this.inflatedView = v
        Glide.with(MainApplication.instance).load(R.drawable.nge2).into(v.findViewById(R.id.no_group_e_ic))
        v.findViewById<LinearLayout>(R.id.ngell).visibility = View.GONE
        loadChats(MainApplication.instance)
        initialize(MainApplication.instance)
        loadProfile(MainApplication.instance)
        if (!BuildConfig.DEBUG) {
            if (!InfinityAPI.signedInUser!!.Verified) {
                mAdView = v.findViewById(R.id.adView)
                val adRequest = AdRequest.Builder().build()
                mAdView.loadAd(adRequest)
            } else {
                mAdView = v.findViewById(R.id.adView)
            }
        }
        return v
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mListener = context as MainActivity?
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
        commonAdapter!!.stopListening()
    }

    private fun initialize(v: Context?) {
        inflatedView!!.findViewById<CircleImageView>(R.id.wrapper_ring).setOnClickListener {
            startActivity(Intent(v, MyAccountActivity::class.java))
        }

        inflatedView!!.findViewById<FloatingActionButton>(R.id.fab).setOnClickListener {
            startActivity(Intent(v, CreateActivity::class.java))
        }
        FirebaseMessaging.getInstance().subscribeToTopic(InfinityAPI.signedInUser!!.UserID)
    }

    private fun loadChats(v: Context?) {
        if (InfinityAPI.signedInUser == null) signOut()
        var query = FirebaseFirestore.getInstance().collection("users").document(InfinityAPI.signedInUser!!.UserID).collection("chats")
        commonAdapter = object:FirestoreRecyclerAdapter<CommonRefer, CommonHolder>(FirestoreRecyclerOptions.Builder<CommonRefer>().setQuery(query, CommonRefer::class.java).build()){

            override fun onBindViewHolder(holder: CommonHolder, position: Int, model: CommonRefer) {
                when {
                    model.chatType == "F" -> {
                        loadFriends(holder, model, MainApplication.instance)
                    }
                    model.chatType == "G" -> {
                        loadGroups(holder, model, MainApplication.instance)
                    }
                    else -> {
                        //Skip and don't bind.
                    }
                }
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommonHolder {
                val v = LayoutInflater.from(parent.context).inflate(R.layout.list_option_view, parent, false)
                return CommonHolder(v)
            }


        }
        inflatedView!!.findViewById<RecyclerView>(R.id.common_list).adapter = commonAdapter
        inflatedView!!.findViewById<RecyclerView>(R.id.common_list).layoutManager = LinearLayoutManager(context)
        commonAdapter!!.startListening()
        query.addSnapshotListener { querySnapshot, _ ->
            if (querySnapshot!!.isEmpty) {
                inflatedView!!.findViewById<LinearLayout>(R.id.ngell).visibility = View.VISIBLE
            } else inflatedView!!.findViewById<LinearLayout>(R.id.ngell).visibility = View.GONE
        }
    }

    private fun loadProfile(v: Context?) {
        Glide.with(v!!).load(InfinityAPI.signedInUser!!.profileImage).into(inflatedView!!.findViewById(R.id.wrapper_ring))
        inflatedView!!.findViewById<TextView>(R.id.inf_title).text = (FirebaseRemoteConfig.getInstance().getString("welcome_prefix") + ' ' + InfinityAPI.signedInUser!!.Name.split(" ")[0])
    }

    private fun signOut() {
        var bd = BottomDialog.Builder(context!!)
                .setTitle("Signing Out")
                .setContent("Hold on Tight, This is going to be quite a ride")
                .setCustomView(CurrentProfileLoadingView(context!!))
                .autoDismiss(true)
                .setCancelable(false)
                .show()
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(context, AuthenticationActivity::class.java))
        mListener!!.finish()
        bd.dismiss()
    }

    private fun loadFriends(holder: CommonHolder?, m1: CommonRefer?, v: Context?) {

        m1!!.relReference.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                //EXPERIMENTAL var model = task.result.toObject
                val model = Relationship(task.result.getString("hostname"), task.result.getString("guestname"), task.result.getString("host"), task.result.getString("guest"), task.result.getString("hostpp"), task.result.getString("guestpp"), task.result.getString("ltm"))
                val iAmHost = (model.host == InfinityAPI.signedInUser!!.UserID)
                if (iAmHost) {
                    holder!!.lov.titletv!!.text = model.guestname
                    holder.lov.subtexttv!!.text = model.ltm
                    Glide.with(v!!).load(model.guestpp).into(holder.lov.lic!!)
                } else {
                    holder!!.lov.titletv!!.text = model.hostname
                    holder.lov.subtexttv!!.text = model.ltm
                    Glide.with(v!!).load(model.hostpp).into(holder.lov.lic!!)
                }
                InfinityAPI.NotificationsManager.subscribeToRelationship(m1, iAmHost)
                if (InfinityAPI.NotificationsManager.newMessageRelationship(m1.relId)) holder.setNewMessage(true)
                holder.setOnClickListener(View.OnClickListener {
                    //Start Chat
                    val c = InfinityAPI.RelationshipRefer(m1.relId, m1.relReference)
                    c.initialize(object : InfinityAPI.InitializedCallback {
                        override fun initialized() {
                            InfinityAPI.Relationship.initializeContext(c)
                            val i = Intent(activity, MessageListActivity::class.java)
                            i.putExtra("TYPE", TYPE.FRIENDS)
                            activity!!.startActivity(i)
                        }
                    })
                })
            } else {
                InfinityAPI.deinitializeCurrentContexts()
                FirebaseAuth.getInstance().signOut()
                activity!!.startActivity(Intent(activity, LaunchActivity::class.java))
                (activity as MainActivity).finish()
            }

        }
    }

    private fun loadGroups(holder: CommonHolder?, m1: CommonRefer?, v: Context?) {
        m1!!.groupReference.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                if (task.result.exists()) {
                    //EXPERIMENTAL var model = task.result.toObject
                    val model = Group(task.result.getString("createdBy"), task.result.getString("groupDescription"), task.result.getString("groupId"), task.result.getString("groupName"), task.result.getBoolean("groupOfficial")!!, task.result.getString("groupProfilePicture"))
                    holder!!.lov.titletv!!.text = model.groupName
                    holder.lov.subtexttv!!.text = model.groupDescription
                    Glide.with(v!!).load(model.groupProfilePicture).into(holder.lov.lic!!)
                    InfinityAPI.NotificationsManager.subscribeToGroup(m1)
                    if (model.groupOfficial) holder.lov.officialBadge!!.visibility = View.VISIBLE
                    if (InfinityAPI.NotificationsManager.newMessageGroup(m1.groupId)) holder.setNewMessage(true)
                    holder.setOnClickListener(View.OnClickListener {
                        //Start Chat
                        val c = InfinityAPI.GroupRefer(m1.groupId, m1.groupName, m1.groupReference)
                        c.initialize(object : InfinityAPI.InitializedCallback {
                            override fun initialized() {
                                InfinityAPI.Group.initializeContext(c)
                                val i = Intent(activity, MessageListActivity::class.java)
                                i.putExtra("TYPE", TYPE.GROUP)
                                InfinityAPI.ChatManager.initializeContext(m1)
                                activity!!.startActivity(i)
                            }

                        })
                    })
                } else {
                    InfinityAPI.deinitializeCurrentContexts()
                    FirebaseAuth.getInstance().signOut()
                    activity!!.startActivity(Intent(activity, LaunchActivity::class.java))
                    (activity as MainActivity).finish()
                }
            }
        }

    }

}

enum class TYPE : Serializable {
    GROUP, FRIENDS
}