package com.infinityapp.infinity.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.infinityapp.infinity.R
import com.infinityapp.infinity.activity.MainActivity

class VerificationFragment : Fragment() {

    private var rview: View? = null
    private var mActivity: MainActivity? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rview = inflater.inflate(R.layout.fragment_verification, RelativeLayout(context))
        FirebaseAuth.getInstance().currentUser!!.sendEmailVerification()
        rview!!.findViewById<Button>(R.id.verification_resend).setOnClickListener {
            FirebaseAuth.getInstance().currentUser!!.sendEmailVerification()
            Toast.makeText(mActivity, "Verification email sent! Check your inbox for instructions on how to Verify your MainApplication Account!", Toast.LENGTH_LONG).show()
        }
        rview!!.findViewById<TextView>(R.id.verification_headline).text = "A verification email was sent to ${FirebaseAuth.getInstance().currentUser!!.email}"
        rview!!.findViewById<Button>(R.id.verification_done).setOnClickListener {
            mActivity!!.signOut()
        }
        return rview
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivity) {
            mActivity = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mActivity = null
    }

}
