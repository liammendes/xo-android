package com.infinityapp.infinity.dataclasses;

/**
 * Created by liammendes on 03/01/2018.
 */

public class Relationship {

    String hostname;
    String guestname;
    String host;
    String guest;
    String hostpp;
    String guestpp;
    String ltm;

    public Relationship() {
    }

    public Relationship(String hostname, String guestname, String host, String guest, String hostpp, String guestpp, String ltm) {
        this.hostname = hostname;
        this.guestname = guestname;
        this.host = host;
        this.guest = guest;
        this.hostpp = hostpp;
        this.guestpp = guestpp;
        this.ltm = ltm;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getGuestname() {
        return guestname;
    }

    public void setGuestname(String guestname) {
        this.guestname = guestname;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getGuest() {
        return guest;
    }

    public void setGuest(String guest) {
        this.guest = guest;
    }

    public String getHostpp() {
        return hostpp;
    }

    public void setHostpp(String hostpp) {
        this.hostpp = hostpp;
    }

    public String getGuestpp() {
        return guestpp;
    }

    public void setGuestpp(String guestpp) {
        this.guestpp = guestpp;
    }

    public String getLtm() {
        return ltm;
    }

    public void setLtm(String ltm) {
        this.ltm = ltm;
    }
}
