package com.infinityapp.infinity.dataclasses;

import android.net.Uri;

import java.util.Date;

public final class SignUpDataGoogle {

    private String displayName;

    private Uri profilePicture;

    private String email;

    private Date dob;

    private String userID;

    private Boolean oSG;

    private String username;

    private Boolean useFirebaseAuthPP = true;


    public SignUpDataGoogle(String dn, String em, String un, Date dobv, Boolean officialSG, Uri pp, String userid) {
        this.displayName = dn;
        this.profilePicture = pp;
        this.email = em;
        this.dob = dobv;
        this.userID = userid;
        this.oSG = officialSG;
        this.username = un;
    }

    public final String getDisplayName() {
        return this.displayName;
    }

    public final void setDisplayName(String var1) {
        this.displayName = var1;
    }

    public final Uri getProfilePicture() {
        return this.profilePicture;
    }

    public final void setProfilePicture(Uri var1) {
        this.profilePicture = var1;
    }

    public final String getEmail() {
        return this.email;
    }

    public final void setEmail(String var1) {
        this.email = var1;
    }

    public final Date getDob() {
        return this.dob;
    }

    public final void setDob(Date var1) {
        this.dob = var1;
    }

    public final String getUserID() {
        return this.userID;
    }

    public final void setUserID(String var1) {
        this.userID = var1;
    }

    public final Boolean getOSG() {
        return this.oSG;
    }

    public final void setOSG(Boolean var1) {
        this.oSG = var1;
    }

    public final String getUsername() {
        return this.username;
    }

    public final void setUsername(String var1) {
        this.username = var1;
    }

    public Boolean getUseFirebaseAuthPP() {
        return useFirebaseAuthPP;
    }

    public void setUseFirebaseAuthPP(Boolean ufapp) {
        this.useFirebaseAuthPP = ufapp;
    }
}
