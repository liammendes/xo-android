package com.infinityapp.infinity.dataclasses;

import com.google.firebase.firestore.DocumentReference;

/**
 * Created by liammendes on 03/01/2018.
 */

public class CommonRefer {

    String relId;
    DocumentReference relReference;
    String groupName;
    DocumentReference groupReference;
    String groupId;
    String chatType;

    public CommonRefer() {
    }

    public CommonRefer(String groupId, String groupName, DocumentReference groupReference, String relId, DocumentReference relReference, String chatType) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupReference = groupReference;
        this.relId = relId;
        this.relReference = relReference;
        this.chatType = chatType;

    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public DocumentReference getGroupReference() {
        return groupReference;
    }

    public void setGroupReference(DocumentReference groupReference) {
        this.groupReference = groupReference;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getRelId() {
        return relId;
    }

    public void setRelId(String relId) {
        this.relId = relId;
    }

    public DocumentReference getRelReference() {
        return relReference;
    }

    public void setRelReference(DocumentReference relReference) {
        this.relReference = relReference;
    }
}
