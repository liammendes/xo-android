package com.infinityapp.infinity.dataclasses;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Chat {

    private String mGroupId;
    private String mGroupName;
    private String mMessage;
    private String mSender;
    private String mMessageType;
    private String mImageUrl;
    private Date mTimestamp;
    private int mMessageNumber;
    public Chat() {
    } // Needed for Firebase

    public Chat(String message, String sender, String groupName, String groupId, int messageNumber, String messageType, String imageUrl) {
        mMessage = message;
        mSender = sender;
        mGroupName = groupName;
        mGroupId = groupId;
        mMessageNumber = messageNumber;
        mMessageType = messageType;
        mImageUrl = imageUrl;


    }

    public String getmessageType() {
        return mMessageType;
    }

    public void setmessageType(String messageType) {
        this.mMessageType = messageType;
    }

    public String getimageUrl() {
        return mImageUrl;
    }

    public void setimageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
    }

    public int getmessageNumber() {
        return mMessageNumber;
    }

    public void setmessageNumber(int messageNumber) {
        this.mMessageNumber = messageNumber;
    }

    public String getmGroupId() {
        return mGroupId;
    }

    public void setmGroupId(String mGroupId) {
        this.mGroupId = mGroupId;
    }

    public String getmGroupName() {
        return mGroupName;
    }

    public void setmGroupName(String mGroupName) {
        this.mGroupName = mGroupName;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmSender() {
        return mSender;
    }

    public void setmSender(String mSender) {
        this.mSender = mSender;
    }

    @ServerTimestamp
    public Date getTimestamp() {
        if (mTimestamp == null) return new Date();
        else
            return mTimestamp;
    }

    public void setTimestamp(Date timestamp) {
        mTimestamp = timestamp;
    }
}