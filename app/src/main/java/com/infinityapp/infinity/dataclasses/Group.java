package com.infinityapp.infinity.dataclasses;

/**
 * Created by liammendes on 03/01/2018.
 */

public class Group {

    String groupName;
    String groupDescription;
    String groupId;
    Boolean groupOfficial;
    String groupProfilePicture;
    String createdBy;

    public Group() {
    }

    public Group(String createdBy, String groupDescription, String groupId, String groupName, boolean groupOfficial, String groupProfilePicture) {
        this.createdBy = createdBy;
        this.groupDescription = groupDescription;
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupOfficial = groupOfficial;
        this.groupProfilePicture = groupProfilePicture;

    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Boolean getGroupOfficial() {
        return groupOfficial;
    }

    public void setGroupOfficial(Boolean groupOfficial) {
        this.groupOfficial = groupOfficial;
    }

    public String getGroupProfilePicture() {
        return groupProfilePicture;
    }

    public void setGroupProfilePicture(String groupProfilePicture) {
        this.groupProfilePicture = groupProfilePicture;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}
