package com.infinityapp.infinity.dataclasses;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class RelationshipChat {

    private String mRelId;
    private String mSenderdn;
    private String mMessage;
    private String mSender;
    private String mProfilepicture;
    private String mMessageType;
    private String mImageUrl;
    private Date mTimestamp;
    private int mMessageNumber;

    public RelationshipChat() {
    } // Needed for Firebase

    public RelationshipChat(String senderdn, String message, String sender, String profilepicture, String relId, int messageNumber, String messageType, String imageUrl) {
        this.mRelId = relId;
        this.mSenderdn = senderdn;
        this.mMessage = message;
        this.mSender = sender;
        this.mProfilepicture = profilepicture;
        this.mMessage = message;
        this.mImageUrl = imageUrl;
        this.mMessageNumber = messageNumber;
        this.mMessageType = messageType;
    }

    public String getmessageType() {
        return mMessageType;
    }

    public void setmessageType(String messageType) {
        this.mMessageType = messageType;
    }

    public String getimageUrl() {
        return mImageUrl;
    }

    public void setimageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
    }

    public String getmRelId() {
        return mRelId;
    }

    public void setmRelId(String mRelId) {
        this.mRelId = mRelId;
    }

    public String getmSenderdn() {
        return mSenderdn;
    }

    public void setmSenderdn(String mSenderdn) {
        this.mSenderdn = mSenderdn;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmSender() {
        return mSender;
    }

    public void setmSender(String mSender) {
        this.mSender = mSender;
    }

    public String getmProfilepicture() {
        return mProfilepicture;
    }

    public void setmProfilepicture(String mProfilepicture) {
        this.mProfilepicture = mProfilepicture;
    }

    public int getmessageNumber() {
        return mMessageNumber;
    }

    public void setmessageNumber(int messageNumber) {
        this.mMessageNumber = messageNumber;
    }

    @ServerTimestamp
    public Date getTimestamp() {
        if (mTimestamp == null) return new Date();
        else
            return mTimestamp;
    }

    public void setTimestamp(Date timestamp) {
        mTimestamp = timestamp;
    }

}