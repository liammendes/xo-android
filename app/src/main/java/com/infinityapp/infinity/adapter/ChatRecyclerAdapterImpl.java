package com.infinityapp.infinity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.infinityapp.InfinityAPI;
import com.infinityapp.infinity.R;
import com.infinityapp.infinity.dataclasses.Chat;
import com.infinityapp.infinity.viewholders.ReceivedMessageHolder;
import com.infinityapp.infinity.viewholders.ReceivedPhotoHolder;
import com.infinityapp.infinity.viewholders.SentMessageHolder;
import com.infinityapp.infinity.viewholders.SentPhotoHolder;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by liammendes6 on 26/03/2018.
 */

public class ChatRecyclerAdapterImpl extends ChatRecyclerAdapter<Chat, RecyclerView.ViewHolder> {

    private static final int CHAT_IS_SENT = 2003;
    private static final int CHAT_IS_RECEIVED = 2004;
    private static final int PHOTO_IS_SENT = 2005;
    private static final int PHOTO_IS_RECEIVED = 2006;

    public int position;

    public ArrayList<Integer> documentsFromCache = new ArrayList<>();

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See
     * {@link FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public ChatRecyclerAdapterImpl(FirestoreRecyclerOptions<Chat> options) {
        super(options);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public void setActive(View rootView, boolean state) {

        //Use View.findViewById(int id) to look for your view in the rootView
        super.setActive(rootView, state);

        if (state) {
            //rootView.findViewById(R.id.tbml).setBackgroundColor(MainApplication.instance.getResources().getColor(R.color.md_white_1000));
        } else {
            //rootView.findViewById(R.id.tbml).setBackgroundColor(MainApplication.instance.getResources().getColor(R.color.md_white_1000));
        }
    }

    @Override
    public void onChildChanged(ChangeEventType type, DocumentSnapshot snapshot, int newIndex, int oldIndex) {
        super.onChildChanged(type, snapshot, newIndex, oldIndex);
        if (type == ChangeEventType.ADDED) {
            if (snapshot.getMetadata().isFromCache()) {
                documentsFromCache.add(newIndex);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        Chat c = getItem(position);
        if (c.getmessageType().equals("M")) {
            if (c.getmSender().equals(InfinityAPI.INSTANCE.getSignedInUser().getUserID())) {
                return CHAT_IS_SENT;
            } else {
                return CHAT_IS_RECEIVED;
            }
        } else if (c.getmessageType().equals("P")) {
            if (c.getmSender().equals(InfinityAPI.INSTANCE.getSignedInUser().getUserID())) {
                return PHOTO_IS_SENT;
            } else {
                return PHOTO_IS_RECEIVED;
            }
        }
        return 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, Chat model) {
        // Bind the Chat object to the ChatHolder
        // ...
        if (model.getmessageType().equals("M")) {
            if (model.getmSender().equals(InfinityAPI.INSTANCE.getSignedInUser().getUserID())) {
                ((SentMessageHolder) holder).bind(model);
                ((SentMessageHolder) holder).setBackground(InfinityAPI.Group.INSTANCE.retrieveContext().getGroupObject().getColorscheme_sent());
            } else {
                ((ReceivedMessageHolder) holder).bind(model);
                ((ReceivedMessageHolder) holder).setBackground(InfinityAPI.Group.INSTANCE.retrieveContext().getGroupObject().getColorscheme_received());
            }
        } else if (model.getmessageType().equals("P")) {
            if (model.getmSender().equals(InfinityAPI.INSTANCE.getSignedInUser().getUserID())) {
                ((SentPhotoHolder) holder).bind(model);
            } else {
                ((ReceivedPhotoHolder) holder).bind(model);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup group, int i) {
        if (i == CHAT_IS_SENT) {
            return new SentMessageHolder(LayoutInflater.from(group.getContext())
                    .inflate(R.layout.item_message_sent, group, false));
        }
        if (i == CHAT_IS_RECEIVED) {
            return new ReceivedMessageHolder(LayoutInflater.from(group.getContext())
                    .inflate(R.layout.item_message_received, group, false));
        }
        if (i == PHOTO_IS_SENT) {
            return new SentPhotoHolder(LayoutInflater.from(group.getContext())
                    .inflate(R.layout.item_photo_sent, group, false));
        }
        if (i == PHOTO_IS_RECEIVED) {
            return new ReceivedPhotoHolder(LayoutInflater.from(group.getContext())
                    .inflate(R.layout.item_photo_received, group, false));
        }

        //Return new sent message holder if no vh exists

        return null;
    }
}
