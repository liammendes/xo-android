package com.infinityapp.infinity.adapter

import androidx.annotation.NonNull
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.*
import com.infinityapp.infinity.dataclasses.Chat

class ChatRecyclerLiveData(var query: Query) : LiveData<MutableList<Chat>>() {

    lateinit var instance: ListenerRegistration
    val listener = EventListener<QuerySnapshot> { p0, p1 ->
        if (p1 == null) {
            if (this@ChatRecyclerLiveData.value == null) {
                this@ChatRecyclerLiveData.postValue(kotlin.collections.mutableListOf())
            }
            this@ChatRecyclerLiveData.value!!.clear()
            for (cdf in p0!!.documents.iterator()) {
                this@ChatRecyclerLiveData.value!!.add(Chat(cdf.getString("mMessage"), cdf.getString("mSender"), "", cdf.getString("mGroupId"), cdf.get("messageNumber") as Int, cdf.getString("messageType"), ""))
            }
        } else {
            throw p1
        }
    }

    class ChatRecyclerViewModel(var groupId: String) : ViewModel() {

        private val liveData = ChatRecyclerLiveData(FirebaseFirestore.getInstance().collection("groups").document(groupId).collection("chats"))

        val dataSnapshotLiveData: LiveData<MutableList<Chat>>
            @NonNull
            get() = liveData
    }

    override fun onActive() {
        super.onActive()
        instance = query.addSnapshotListener(listener)
    }

    override fun onInactive() {
        super.onInactive()
        instance.remove()
    }

}