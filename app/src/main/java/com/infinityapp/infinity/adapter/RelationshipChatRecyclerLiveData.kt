package com.infinityapp.infinity.adapter

import androidx.annotation.NonNull
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.*
import com.infinityapp.infinity.dataclasses.RelationshipChat

class RelationshipChatRecyclerLiveData(var query: Query) : LiveData<MutableList<RelationshipChat>>() {

    lateinit var instance: ListenerRegistration
    val listener = EventListener<QuerySnapshot> { p0, p1 ->
        if (p1 == null) {
            if (this@RelationshipChatRecyclerLiveData.value == null) {
                this@RelationshipChatRecyclerLiveData.postValue(kotlin.collections.mutableListOf())
            }
            this@RelationshipChatRecyclerLiveData.value!!.clear()
            for (cdf in p0!!.documents.iterator()) {
                this@RelationshipChatRecyclerLiveData.value!!.add(RelationshipChat("", cdf.getString("mMessage"), cdf.getString("mSender"), "", cdf.getString("mRelId"), cdf.get("messageNumber") as Int, cdf.getString("messageType"), ""))
            }
        } else {
            throw p1
        }
    }

    class RelationshipChatRecyclerViewModel(var groupId: String) : ViewModel() {

        private val liveData = RelationshipChatRecyclerLiveData(FirebaseFirestore.getInstance().collection("groups").document(groupId).collection("chats"))

        val dataSnapshotLiveData: LiveData<MutableList<RelationshipChat>>
            @NonNull
            get() = liveData
    }

    override fun onActive() {
        super.onActive()
        instance = query.addSnapshotListener(listener)
    }

    override fun onInactive() {
        super.onInactive()
        instance.remove()
    }

}