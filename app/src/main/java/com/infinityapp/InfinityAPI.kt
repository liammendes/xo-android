package com.infinityapp

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.HttpsCallableResult
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.infinityapp.infinity.InfinityMessagingService
import com.infinityapp.infinity.MainApplication
import com.infinityapp.infinity.R
import com.infinityapp.infinity.callback.NewMessageListener
import com.infinityapp.infinity.dataclasses.CommonRefer
import com.infinityapp.infinity.fragment.TYPE
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


object InfinityAPI {

    var signedInUser: User? = null
    var groups: ArrayList<GroupRefer> = ArrayList()
    private var endTime: Long = 0L
    private var startTime: Long = 0L
    private var fname: String = ""

    @Override
    fun firebaseUidToSignIn(uid: String, sic: SignedInCallback) {
        FirebaseFirestore.getInstance().collection("users").document(uid).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                if (task.result.exists()) {
                    var friends: ArrayList<String> = arrayListOf()
                    if (task.result.get("Friends") != null) friends = task.result.get("Friends") as ArrayList<String>

                    var relationships: ArrayList<String> = arrayListOf()
                    if (task.result.get("relationships") != null) relationships = task.result.get("relationships") as ArrayList<String>

                    var topics: ArrayList<String> = arrayListOf()
                    if (task.result.get("topics") != null) topics = task.result.get("topics") as ArrayList<String>

                    var stat: Status? = when {
                        task.result.getString("Status") == "U" -> Status.USER
                        task.result.getString("Status") == "A" -> Status.ADMIN
                        else -> Status.USER
                    }
                    signIn(
                            DOB = task.result.getDate("DOB")!!,
                            EH = task.result.getString("EH")!!,
                            Name = task.result.getString("Name")!!,
                            username = task.result.getString("username")!!,
                            Status = stat!!,
                            UserID = uid,
                            Verified = task.result.getBoolean("Verified")!!,
                            Friends = friends,
                            Online = task.result.getBoolean("Online")!!,
                            profileImage = task.result.getString("profileImage")!!,
                            relationships = relationships,
                            setupcompleted = task.result.getBoolean("setupcompleted")!!,
                            topics = topics,
                            sic = sic
                    )
                } else {
                    sic.signUp()
                }
            } else {
                Log.e(MainApplication.instance.createGlobalTag("InfinityAPI"), ("Error in InfinityAPI @l7-" + this::class.java.canonicalName))
                return@addOnCompleteListener
            }
        }
    }

    fun signIn(DOB: Date, EH: String, Name: String, username: String, Status: Status, UserID: String, Verified: Boolean, Friends: ArrayList<String>, Online: Boolean, profileImage: String, relationships: ArrayList<String>, setupcompleted: Boolean, topics: ArrayList<String>, sic: SignedInCallback) {

        signedInUser = User(DOB = DOB, EH = EH, Name = Name, username = username, Status = Status, UserID = UserID, Verified = Verified, Friends = Friends, Online = Online, profileImage = profileImage, relationships = relationships, setupcompleted = setupcompleted, topics = topics)
        signedInUser!!.initialize(object : InitializedCallback {
            override fun initialized() {
                FirebaseFirestore.getInstance().collection("users").document(UserID).get().addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        task.result.reference.collection("groups").get().addOnCompleteListener { task2 ->
                            if (task2.isSuccessful) {
                                for (group in task2.result.documents) {
                                    groups.add(GroupRefer(groupId = group.getString("groupId")!!, groupReference = group.getDocumentReference("groupReference")!!, groupName = group.getString("groupId")!!))
                                }
                                sic.signedIn()
                            } else {
                                Log.e(MainApplication.instance.createGlobalTag("InfinityAPI"), ("Error in InfinityAPI @l31-" + this::class.java.canonicalName))
                                return@addOnCompleteListener
                            }
                        }
                    } else {

                    }
                }
            }

        })
    }

    data class User(var DOB: Date, var EH: String, var Name: String, var username: String, var Status: Status, val UserID: String, var Verified: Boolean, var Friends: ArrayList<String>, var Online: Boolean, var profileImage: String, var relationships: ArrayList<String>, var setupcompleted: Boolean, var topics: ArrayList<String>) {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as User

            if (DOB != other.DOB) return false
            if (EH != other.EH) return false
            if (Name != other.Name) return false
            if (username != other.username) return false
            if (Status != other.Status) return false
            if (UserID != other.UserID) return false
            if (Verified != other.Verified) return false
            if (!Arrays.equals(Friends.toArray(), other.Friends.toArray())) return false
            if (Online != other.Online) return false
            if (profileImage != other.profileImage) return false
            if (!Arrays.equals(relationships.toArray(), other.relationships.toArray())) return false
            if (setupcompleted != other.setupcompleted) return false
            if (!Arrays.equals(topics.toArray(), other.topics.toArray())) return false

            return true
        }

        override fun hashCode(): Int {
            var result = DOB.hashCode()
            result = 31 * result + EH.hashCode()
            result = 31 * result + Name.hashCode()
            result = 31 * result + username.hashCode()
            result = 31 * result + Status.hashCode()
            result = 31 * result + UserID.hashCode()
            result = 31 * result + Verified.hashCode()
            result = 31 * result + Arrays.hashCode(Friends.toArray())
            result = 31 * result + Online.hashCode()
            result = 31 * result + profileImage.hashCode()
            result = 31 * result + Arrays.hashCode(relationships.toArray())
            result = 31 * result + setupcompleted.hashCode()
            result = 31 * result + Arrays.hashCode(topics.toArray())
            return result
        }

        fun drawableToBitmap(drawable: Drawable): Bitmap? {
            var bitmap: Bitmap? = null

            if (drawable is BitmapDrawable) {
                if (drawable.bitmap != null) {
                    return drawable.bitmap
                }
            }

            bitmap = if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
                Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888) // Single color bitmap will be created of 1x1 pixel
            } else {
                Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
            }

            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            return bitmap
        }

        fun initialize(callback: InitializedCallback) {
            callback.initialized()
        }
    }

    data class GroupFields(var groupDescription: String, var groupId: String, var groupName: String, var groupOfficial: Boolean, var groupProfilePicture: String, var createdBy: String, var colorscheme_sent: Long = 19, var colorscheme_received: Long = 7)

    data class RelationshipFields(var hostname: String, var guestname: String, var host: String, var guest: String, var hostpp: String, var guestpp: String, var ltm: String, var colorscheme_sent: Long = 19, var colorscheme_received: Long = 7) {
        fun getOtherName(userId: String): String {
            if(userId == host) return guestname
            return hostname
        }
    }

    data class GroupRefer(val groupId: String, val groupName: String, val groupReference: DocumentReference) {

        private lateinit var groupObject: GroupFields
        private var initialized: Boolean = false

        fun initialize(param: InitializedCallback) {
            FirebaseFirestore.getInstance().collection("groups").document(groupId).get().addOnCompleteListener { task ->
                if (task.isSuccessful && task.result.exists()) {
                    initialized = true
                    groupObject = if (task.result.get("colorscheme_sent") != null || task.result.get("colorscheme_received") != null)
                        GroupFields(task.result.getString("groupDescription")!!, task.result.getString("groupId")!!, task.result.getString("groupName")!!, task.result.getBoolean("groupOfficial")!!, task.result.getString("groupProfilePicture")!!, task.result.getString("createdBy")!!, task.result.get("colorscheme_sent") as Long, task.result.get("colorscheme_received") as Long)
                    else
                        GroupFields(task.result.getString("groupDescription")!!, task.result.getString("groupId")!!, task.result.getString("groupName")!!, task.result.getBoolean("groupOfficial")!!, task.result.getString("groupProfilePicture")!!, task.result.getString("createdBy")!!, 19, 7)
                    param.initialized()
                } else {
                    initialized = false
                    param.initialized()
                    return@addOnCompleteListener
                }
            }
        }

        fun getGroupObject(): GroupFields = groupObject
    }

    data class RelationshipRefer(val relID: String, val relReference: DocumentReference) {

        private lateinit var relObject: RelationshipFields
        private var initialized: Boolean = false

        fun initialize(param: InitializedCallback) {
            FirebaseFirestore.getInstance().collection("relationships").document(relID).get().addOnCompleteListener { task ->
                if (task.isSuccessful && task.result.exists()) {
                    initialized = true
                    relObject = if (task.result.get("colorscheme_sent") != null || task.result.get("colorscheme_received") != null)
                        RelationshipFields(task.result.getString("hostname")!!, task.result.getString("guestname")!!, task.result.getString("host")!!, task.result.getString("guest")!!, task.result.getString("hostpp")!!, task.result.getString("guestpp")!!, task.result.getString("ltm")!!, task.result.get("colorscheme_sent") as Long, task.result.get("colorscheme_received") as Long)
                    else
                        RelationshipFields(task.result.getString("hostname")!!, task.result.getString("guestname")!!, task.result.getString("host")!!, task.result.getString("guest")!!, task.result.getString("hostpp")!!, task.result.getString("guestpp")!!, task.result.getString("ltm")!!, 19, 7)
                    param.initialized()
                } else {
                    initialized = false
                    param.initialized()
                    return@addOnCompleteListener
                }
            }
        }

        fun getRelationshipObject(): RelationshipFields = relObject
    }

    interface InitializedCallback {
        fun initialized()
    }

    interface SignedInCallback {
        fun signedIn()
        fun signUp()
    }

    enum class Status(s: String) { USER("user"), ADMIN("admin") }

    fun makeCustomCall(method: String, vararg data: Pair<String, Any>): Task<HttpsCallableResult>? {

        var alist = mutableMapOf<String, Any>()
        for (pair in data) {
            alist[pair.first] = pair.second
        }

        var function = FirebaseFunctions.getInstance()
        var calledFunction = function.getHttpsCallable(method).call(alist)
        calledFunction.addOnCompleteListener { task ->
            if (task.isSuccessful) {
//                Log.e(MainApplication.instance.createGlobalTag("InfinityFunctionsTesting - IFT"), task.result.data.toString())
            } else task.exception!!.printStackTrace(System.err)
        }
        return calledFunction
    }

    fun makeCustomCall(method: String, alist: Map<String, Any>): Task<HttpsCallableResult>? {
        var function = FirebaseFunctions.getInstance()
        return function.getHttpsCallable(method).call(alist)
    }

    private fun boolToString(uf: Boolean): String {
        if (uf) return "true" else if (!uf) return "false"
        return "false"
    }

    interface InvokeFinishedCallback {
        fun responseReceived(response: String?)
    }

    object Group {

        private var context: GroupRefer? = null

        fun initializeContext(a: GroupRefer) {
            context = a
        }

        fun retrieveContext(): GroupRefer? = context

        fun nullContext() {
            context = null
        }

    }

    object Relationship {

        private var context: RelationshipRefer? = null

        fun initializeContext(a: RelationshipRefer) {
            context = a
        }

        fun retrieveContext(): RelationshipRefer? = context

        fun nullContext() {
            context = null
        }

    }

    fun isConencted(): Boolean {
        val cm = MainApplication.instance.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return cm.activeNetworkInfo != null
    }

    object Utils {
        @JvmStatic
        fun colorSchemeToResource(i: Long, b: Boolean): Int {
            var black = FirebaseRemoteConfig.getInstance().getString("black")
            var white = FirebaseRemoteConfig.getInstance().getString("white")
            var red = FirebaseRemoteConfig.getInstance().getString("red")
            var pink = FirebaseRemoteConfig.getInstance().getString("pink")
            var purple = FirebaseRemoteConfig.getInstance().getString("purple")
            var deep_purple = FirebaseRemoteConfig.getInstance().getString("deep_purple")
            var indigo = FirebaseRemoteConfig.getInstance().getString("indigo")
            var blue = FirebaseRemoteConfig.getInstance().getString("blue")
            var light_blue = FirebaseRemoteConfig.getInstance().getString("light_blue")
            var cyan = FirebaseRemoteConfig.getInstance().getString("cyan")
            var teal = FirebaseRemoteConfig.getInstance().getString("teal")
            var green = FirebaseRemoteConfig.getInstance().getString("green")
            var light_green = FirebaseRemoteConfig.getInstance().getString("light_green")
            var lime = FirebaseRemoteConfig.getInstance().getString("lime")
            var yellow = FirebaseRemoteConfig.getInstance().getString("yellow")
            var amber = FirebaseRemoteConfig.getInstance().getString("amber")
            var orange = FirebaseRemoteConfig.getInstance().getString("orange")
            var deep_orange = FirebaseRemoteConfig.getInstance().getString("deep_orange")
            var brown = FirebaseRemoteConfig.getInstance().getString("brown")
            var grey = FirebaseRemoteConfig.getInstance().getString("grey")
            var blue_grey = FirebaseRemoteConfig.getInstance().getString("blue_grey")
            if (black == "" ||
                    white == "" ||
                    red == "" ||
                    pink == "" ||
                    purple == "" ||
                    deep_purple == "" ||
                    indigo == "" ||
                    blue == "" ||
                    light_blue == "" ||
                    cyan == "" ||
                    teal == "" ||
                    green == "" ||
                    light_green == "" ||
                    lime == "" ||
                    yellow == "" ||
                    amber == "" ||
                    orange == "" ||
                    deep_orange == "" ||
                    brown == "" ||
                    grey == "" ||
                    blue_grey == "") return if (b) R.drawable.rounded_rectangle_grey_selector else R.drawable.rounded_rectangle_blue_selector

            when (i) {
                black.toLong() -> return R.drawable.rounded_rectangle_black_selector                 //0
                white.toLong() -> return R.drawable.rounded_rectangle_white_selector                 //1
                red.toLong() -> return R.drawable.rounded_rectangle_red_selector                     //2
                pink.toLong() -> return R.drawable.rounded_rectangle_pink_selector                   //3
                purple.toLong() -> return R.drawable.rounded_rectangle_purple_selector               //4
                deep_purple.toLong() -> return R.drawable.rounded_rectangle_deep_purple_selector     //5
                indigo.toLong() -> return R.drawable.rounded_rectangle_indigo_selector               //6
                blue.toLong() -> return R.drawable.rounded_rectangle_blue_selector                   //7
                light_blue.toLong() -> return R.drawable.rounded_rectangle_light_blue_selector       //8
                cyan.toLong() -> return R.drawable.rounded_rectangle_cyan_selector                   //9
                teal.toLong() -> return R.drawable.rounded_rectangle_teal_selector                   //10
                green.toLong() -> return R.drawable.rounded_rectangle_green_selector                 //11
                light_green.toLong() -> return R.drawable.rounded_rectangle_light_green_selector     //12
                lime.toLong() -> return R.drawable.rounded_rectangle_lime_selector                   //13
                yellow.toLong() -> return R.drawable.rounded_rectangle_yellow_selector               //14
                amber.toLong() -> return R.drawable.rounded_rectangle_amber_selector                 //15
                orange.toLong() -> return R.drawable.rounded_rectangle_orange_selector               //16
                deep_orange.toLong() -> return R.drawable.rounded_rectangle_deep_orange_selector     //17
                brown.toLong() -> return R.drawable.rounded_rectangle_brown_selector                 //18
                grey.toLong() -> return R.drawable.rounded_rectangle_grey_selector                   //19
                blue_grey.toLong() -> return R.drawable.rounded_rectangle_blue_grey_selector         //20
            }
            return if (b) R.drawable.rounded_rectangle_grey_selector else R.drawable.rounded_rectangle_blue_selector
        }
    }

    object Messaging {

        var type: TYPE? = null

        fun initializeContext(t: TYPE) {
            type = t
        }

        fun nullContext() {
            type = null
        }

        fun retrieveContext(): TYPE = type!!

    }

    fun nullReportValues() {
        fname = ""
        startTime = 0L
        endTime = 0L
    }

    fun deinitializeCurrentContexts() {
        Messaging.nullContext()
        Relationship.nullContext()
        Group.nullContext()
        ChatManager.nullContext()
        InfinityMessagingService.NotificationMessagingManager.nullContexts()
        NotificationsManager.unSubscribeFromAll()
        nullReportValues()
        groups = ArrayList()
        signedInUser = null
    }

    object ChatManager {

        var cr: CommonRefer? = null

        fun initializeContext(t: CommonRefer) {
            cr = t
        }

        fun nullContext() {
            cr = null
        }

        fun retrieveContext(): CommonRefer = cr!!

    }

    object NotificationsManager {
        var newMessageGroupsListener: MutableMap<String, ArrayList<NewMessageListener>> = mutableMapOf()
        var newMessageGroups: MutableMap<String, String> = mutableMapOf()
        var newMessageRelationshipsListener: MutableMap<String, ArrayList<NewMessageListener>> = mutableMapOf()
        var newMessageRelationships: MutableMap<String, String> = mutableMapOf()
        var notificationEnabled: MutableMap<CommonRepresentative, Boolean> = mutableMapOf()
        fun subscribeToRelationship(model: CommonRefer, iAmHost: Boolean) {
            if (CommonRepresentative(model.relId, TYPE.FRIENDS) in notificationEnabled) {
                notificationEnabled[CommonRepresentative(model.relId, TYPE.FRIENDS)] = true
            } else notificationEnabled.put(CommonRepresentative(model.relId, TYPE.FRIENDS), true)
            FirebaseMessaging.getInstance().subscribeToTopic(model.relId)
        }

        fun unSubscribeFromRelationship(model: CommonRefer, iAmHost: Boolean) {
            notificationEnabled[CommonRepresentative(model.relId, TYPE.FRIENDS)] = false
            FirebaseMessaging.getInstance().unsubscribeFromTopic(model.relId)
        }

        fun unSubscribeFromGroup(model: CommonRefer) {
            notificationEnabled[CommonRepresentative(model.groupId, TYPE.GROUP)] = false
            FirebaseMessaging.getInstance().unsubscribeFromTopic(model.groupId)
        }

        fun subscribeToGroup(model: CommonRefer) {
            if (CommonRepresentative(model.groupId, TYPE.GROUP) in notificationEnabled) {
                notificationEnabled[CommonRepresentative(model.groupId, TYPE.GROUP)] = true
            } else notificationEnabled[CommonRepresentative(model.groupId, TYPE.GROUP)] = true
            FirebaseMessaging.getInstance().subscribeToTopic(model.groupId)
        }

        fun isSubscribedGroup(model: CommonRefer): Boolean {
            return notificationEnabled.keys.contains(CommonRepresentative(model.groupId, TYPE.GROUP))
        }

        fun isSubscribedRelationship(model: CommonRefer): Boolean {
            return notificationEnabled.keys.contains(CommonRepresentative(model.relId, TYPE.FRIENDS))
        }

        fun sendRegistrationToServer(refreshedToken: String) {
            /**var ms: Map<String, Any> = mapOf(Pair<String, Any>("refreshedToken", refreshedToken))
            FirebaseFirestore.getInstance().collection("instanceId").document(FirebaseAuth.getInstance().uid!!).set(ms)**/
        }

        fun unSubscribeFromAll() {
            for (cr in notificationEnabled) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(cr.key.id)
            }
            notificationEnabled = mutableMapOf()
            newMessageRelationships = mutableMapOf()
            newMessageRelationshipsListener = mutableMapOf()
            newMessageGroups = mutableMapOf()
            newMessageGroupsListener = mutableMapOf()
        }

        fun setNewMessageGroup(groupId: String, messageNumber: String) {
            if (newMessageGroups[groupId] == null) return
            newMessageGroups.put(groupId, messageNumber)
            newMessageGroupsListener[groupId]!!.forEach { nml ->
                nml.newMessage(messageNumber)
            }
        }

        fun setNewMessageRelationship(relId: String, messageNumber: String) {
            if (newMessageGroups[relId] == null) return
            newMessageRelationships.put(relId, messageNumber)
            newMessageRelationshipsListener[relId]!!.forEach { nml ->
                nml.newMessage(messageNumber)
            }
        }

        fun setReadMessagesGroups(groupId: String) {
            if (newMessageGroups[groupId] == null) return
            newMessageGroups.remove(groupId)
            newMessageGroupsListener[groupId]!!.forEach { nml ->
                nml.readMessages(groupId)
            }
        }

        fun setReadMessagesRelationship(relId: String) {
            if (newMessageRelationships[relId] == null) return
            newMessageRelationships.remove(relId)
            newMessageRelationshipsListener[relId]!!.forEach { nml ->
                nml.readMessages(relId)
            }
        }

        fun addNMLGroups(groupId: String, nml: NewMessageListener) {
            if (newMessageGroupsListener[groupId] == null) {
                newMessageGroupsListener.put(groupId, arrayListOf(nml))
            } else {
                newMessageGroupsListener[groupId]!!.add(nml)
            }
        }

        fun addNMLRelationships(relId: String, nml: NewMessageListener) {
            if (newMessageRelationshipsListener[relId] == null) {
                newMessageRelationshipsListener.put(relId, arrayListOf(nml))
            } else {
                newMessageRelationshipsListener[relId]!!.add(nml)
            }
        }

        fun newMessageGroup(groupId: String?): Boolean {
            return newMessageGroups.contains(groupId)
        }

        fun newMessageRelationship(relID: String?): Boolean {
            return newMessageRelationships.contains(relID)
        }

    }

    data class CommonRepresentative(val id: String, val type: TYPE)

    fun getOtherNameRelationship(relId: String, invokeFinishedCallback: InfinityAPI.InvokeFinishedCallback) {
        makeCustomCall("getOtherNameRelationship", Pair("relId", relId))!!.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                invokeFinishedCallback.responseReceived(oldMethodBridge(task.result.data))
            } else {
                invokeFinishedCallback.responseReceived(null)
            }
        }
    }

    fun getProfilePicture(sender: String, invokeFinishedCallback: InfinityAPI.InvokeFinishedCallback) {
        makeCustomCall("userIdToAvatar", Pair("userId", sender))!!.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                invokeFinishedCallback.responseReceived(oldMethodBridge(task.result.data))
            } else {
                invokeFinishedCallback.responseReceived(null)
            }
        }
    }

    fun getUsersDisplayName(sender: String, invokeFinishedCallback: InfinityAPI.InvokeFinishedCallback) {
        makeCustomCall("userIdToName", Pair("userId", sender))!!.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                invokeFinishedCallback.responseReceived(oldMethodBridge(task.result.data))
            } else {
                invokeFinishedCallback.responseReceived(null)
            }
        }
    }

    fun getUsersEmail(sender: String, invokeFinishedCallback: InfinityAPI.InvokeFinishedCallback) {
        makeCustomCall("userIdToEmail", Pair("userId", sender))!!.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                invokeFinishedCallback.responseReceived(oldMethodBridge(task.result.data))
            } else {
                invokeFinishedCallback.responseReceived(null)
            }
        }
    }

    fun oldMethodBridge(data: Any?): String {
        return when (data) {
            is String -> JSONObject(data).toString(4)
            is HashMap<*, *> -> JSONObject(data).toString(4)
            else -> data.toString()
        }
    }

}